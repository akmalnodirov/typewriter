
$(function(){

    var myPlacemark;
    var address;

    var lat = parseFloat($('#object_location').attr('lat'));
    var lng = parseFloat($('#object_location').attr('lng'));
    var contact = $('#object_location').attr('contact');

    ymaps.ready(function () {

        var resort_map = new ymaps.Map('object_map', {
            center: [lat, lng],
            zoom: 14
        });


        myPlacemark = new ymaps.Placemark([lat, lng], {}, {
            //iconLayout: 'default#image',
            //iconImageHref: '/rasulov_assets/img/marker.png',
            //iconImageSize: [50, 70],
            draggable: true
        });

        resort_map.geoObjects.add(myPlacemark);

        myPlacemark.events.add('dragend', function(e) {

            var coords = myPlacemark.geometry.getCoordinates();
            setAddressLocation(e, coords);

        });

        resort_map.events.add('click', function (e) {

            var coords = e.get('coords');
            myPlacemark.geometry.setCoordinates(coords);
            var coords = myPlacemark.geometry.getCoordinates();
            setAddressLocation(e, coords);

        });

        if(contact.length <=0){
            ajaxRequestToPoints(resort_map);
        }

    });


})

function setAddressLocation(e, coords){


    var myReverseGeocoder = ymaps.geocode(coords);

    myReverseGeocoder.then(

        function (res) {

            var nearest = res.geoObjects.get(0);
            address = nearest.properties._data.text;
            $('#tradepoints-lat').val(coords[0]);
            $('#tradepoints-lng').val(coords[1]);
            $('#tradepoints-address').val(address);
        },

        function (err) {
            console.log(error);
        }
    );

}

function ajaxRequestToPoints(map) {
    $.ajax({
        url: '/kraska/trade-points',
        success: function (response) {

            handlePoints(response, map);

        }
    });
}

function handlePoints(points, map) {

    let myGeoObjects = [];
    let coordinates = [];
    let locations = [];
    let trade_points;
    let myClusterer;

    trade_points = JSON.parse(points);


    var placemarks = [];
    for (x in trade_points) {

        locations.push(trade_points[x]['lat']);
        locations.push(trade_points[x]['lng']);
        coordinates.push(locations);
        locations = [];

        var my_placemark = new ymaps.Placemark([trade_points[x]['lat'], trade_points[x]['lng']], {}, {
            iconLayout: 'default#image',
            iconImageHref: '/adminty/images/shop_icon.png',
            iconImageSize: [60, 60],
            //draggable: true,
            properties: {
                iconContent: 'something',
            }
        });
        placemarks.push(my_placemark);

    }

    myClusterer = new ymaps.Clusterer();

    myClusterer.add(placemarks);
    map.geoObjects.add(myClusterer);
    map.setBounds(myClusterer.getBounds());

}

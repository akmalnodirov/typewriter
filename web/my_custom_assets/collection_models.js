$(function () {
    let limit = 16;
    let symbols = '';
    let search = null;
    let collection_id = $('#collection_models_area').attr('collection_id');

    $('.collection_model_symbol').click(function () {
        $('#collection_show_all_button_div').css('display', 'block');
        symbols = symbols + $(this).attr('symbol');
        $('#collection_model_search').val(symbols);

        if(symbols.length >= 3){
            search = symbols.toLowerCase();
            getModels(collection_id, limit, search)
        }
    });

    $('#collection_model_search').keyup(function (e) {

        $('#collection_show_all_button_div').css('display', 'block');
        let valueLength = $(this).val().length;
        if (valueLength >= 3) {
            search = $(this).val();
            getModels(collection_id, limit, search)
        } else {
            getModels(collection_id, limit)
        }

        if(e.keyCode === 8){

            if(valueLength < 3){
                let string = Array.from(search);
                string.splice(valueLength, 1);
                search = string.join('');
                symbols = search;
                console.log(search, valueLength, '');
            }

        }

    });

    $('#collection_show_all_button').click(function (e) {
        e.preventDefault();
        search = null;
        limit = 0;
        getModels(collection_id, limit, search);
        $('#collection_show_all_button_div').css('display', 'none');
    });

    function getModels(collection_id, limit, search = null) {
        $.ajax({
            url: '/type/collection-models-partial',
            data: {collection_id: collection_id, limit: limit, search: search},
            dataType: 'json',
            success: function (response) {
                $('#collection_models_area').html(response.content);
                if(response.collection && response.collection.title)
                $('#collection_title').html(response.collection.title);
            }
        })
    };

    $('body').on('click', '.collection_name_with_count', function(){

        if($(this).attr('count') > 0){
            collection_id = $(this).attr('collection_id');
            limit = null;
            getModels(collection_id, limit)
        }
    });

    getModels(collection_id, limit);

});
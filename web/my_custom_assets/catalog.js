$(function () {
    let limit = 6;
    let search = null;
    let symmbols = '';

    $('.catalog_symbol').click(function () {
        $('#show_all_button_div').css('display', 'block');
        symmbols = symmbols + '' + $(this).attr('symbol');
        $('#catalog_search').val(symmbols);

        if(symmbols.length >= 3){
            search = symmbols.toLowerCase();
            getCatalogs(limit, search)
        }

    });

    $('#catalog_search').keyup(function (e) {

        let valueLength = $(this).val().length;

        $('#show_all_button_div').css('display', 'block');

        if (valueLength >= 3) {
            search = $(this).val();
            getCatalogs(limit, search);
            $('#show_all_button_div').css('display', 'block');
        } else {
            getCatalogs(limit)
        }

        if(e.keyCode == 8){

            if(valueLength < 3){
                let string = Array.from(search);
                string.splice(valueLength, 1);
                search = string.join('');
                symmbols = search;
                console.log(search, valueLength, '');
            } else {

            }

        }

    });



    $('#show_all_button').click(function (e) {
        e.preventDefault();
        console.log('button clicked');
        search = null;
        limit = 0;
        getCatalogs(limit, search, true);
        $('#show_all_button_div').css('display', 'none');
    });

    function getCatalogs(limit, search = null) {
        $.ajax({
            url: '/type/catalog-partial',
            data: {limit: limit, search: search},
            dataType: 'json',
            success: function (response) {
                $('#catalogs_area').html(response);
            }
        })
    };

    getCatalogs(limit);

});
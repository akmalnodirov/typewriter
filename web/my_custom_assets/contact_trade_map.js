$(function () {

    let myPlacemark;
    let myClusterer;
    let address;
    let contact_map;

    let hash = window.location.hash;

    let lat = parseFloat($('.contact_map').attr('lat'));
    let lng = parseFloat($('.contact_map').attr('lng'));


    ymaps.ready(function () {

        contact_map = new ymaps.Map('contact_map', {
            center: [lat, lng],
            zoom: 16
        });

        addContactMarker(contact_map, lat, lng);


        if(hash === '#trades') {
            removeContactPlaceMark(contact_map);
            ajaxRequestToPoints(contact_map);
            $('.where_to_buy_button').addClass('active');
        }

        if(hash === '#footerMap') {
            $('.contacts_button').addClass('active');
        }

        if(hash === '#news-lk') {
            $('.news_button').addClass('active');
        }

        if(hash === '#products_area') {
            $('.products_button').addClass('active');
        }

    });







    $('.where_to_buy_button > a').on('click', function(){
        removeContactPlaceMark(contact_map);
        ajaxRequestToPoints(contact_map);
    });

    $('.contacts_button > a').on('click', function(){
        contact_map.geoObjects.removeAll();
        removeContactPlaceMark(contact_map);
        addContactMarker(contact_map, lat, lng);
    });



})

function removeContactPlaceMark(map){
    map.geoObjects.remove(myPlacemark);
}

function addContactMarker(map ,lat, lng) {

    myPlacemark = new ymaps.Placemark([lat, lng], {
        iconCaption: 'Наш Офис'
    }, {
        iconLayout: 'default#imageWithContent',
        iconImageHref: '/adminty/images/map_flag.png',
        iconImageSize: [100, 100],
    });

    map.geoObjects.add(myPlacemark);
    map.setCenter([lat, lng], 16, {
        checkZoomRange: true
    });
}

function ajaxRequestToPoints(map) {
    $.ajax({
        url: '/kraska/trade-points',
        success: function (response) {

            handlePoints(response, map);

        }
    });
}

function handlePoints(points, map) {

    let myGeoObjects = [];
    let coordinates = [];
    let locations = [];
    let trade_points;
    let myClusterer;

    trade_points = JSON.parse(points);


    var placemarks = [];
    for (x in trade_points) {

        locations.push(trade_points[x]['lat']);
        locations.push(trade_points[x]['lng']);
        coordinates.push(locations);
        locations = [];

        var my_placemark = new ymaps.Placemark([trade_points[x]['lat'], trade_points[x]['lng']], {}, {
            iconLayout: 'default#image',
            iconImageHref: '/adminty/images/shop_icon.png',
            iconImageSize: [60, 60],
            //draggable: true,
            properties: {
                iconContent: 'something',
            }
        });
        placemarks.push(my_placemark);

    }

    myClusterer = new ymaps.Clusterer();

    myClusterer.add(placemarks);
    map.geoObjects.add(myClusterer);
    map.setBounds(myClusterer.getBounds());
}



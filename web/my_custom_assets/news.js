$(function () {


    console.log('im here');

    $('#show_all_news_button').click(function (e) {
        console.log('im here');
        e.preventDefault();
        getModels(true);
        $('#show_all_news_button_div').css('display', 'none');
    });

    function getModels(all=null) {
        $.ajax({
            url: '/type/news-partial',
            data: {all: all},
            dataType: 'json',
            success: function (response) {
                $('#news_area').html(response);
            }
        })
    };

    getModels();

});
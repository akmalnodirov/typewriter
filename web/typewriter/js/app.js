$(document).ready(function () {

    //////////
    // Global variables
    //////////

    var _window = $(window);
    var _document = $(document);
    var scrollPos = 0;

    if ($('body').hasClass('homepage')) {
        var elPosition = [
            $('#section-js-slide-1').position().top,
            $('#section-js-slide-2').position().top,
            $('#section-js-slide-3').position().top,
            $('#section-js-slide-4').position().top
        ]
    }

    // READY - triggered when PJAX DONE
    function pageReady() {
        legacySupport();
        updateHeaderActiveClass();
        initHeaderScroll();
        initPopups();
        initSliders();
        initMasks();
        initSelectric();
        // custom
        searchToggle();
        uiTransformMobile();
        tabsNavigation();
        setActive();
        accordion();
        customTabs();
        checkBox();
    }

    // this is a master function which should have all functionality
    pageReady();

    // COMMON

    function legacySupport() {
        // svg support for laggy browsers
        svg4everybody();

        // Viewport units buggyfill
        window.viewportUnitsBuggyfill.init({
            force: false,
            refreshDebounceWait: 150,
            appendToBody: true
        });
    }

    function scrollPosition(section) {
        var position = 0;

        switch (section) {
            case '#section-js-slide-1':
                position = elPosition[0];
                break;
            case '#section-js-slide-2':
                position = elPosition[1];
                break;
            case '#section-js-slide-3':
                position = elPosition[2];
                break;
            case '#section-js-slide-4':
                position = elPosition[3];
                break;
        }
        return position;
    }

    // Prevent # behavior
    _document
        .on('click', '[href="#"]', function (e) {
            e.preventDefault();
        })
        .on('click', 'a[href^="#section"]', function () {
            var el = $(this).attr('href');

            $('#scroll').animate(
                {
                    scrollTop: scrollPosition(el)
                }, 1000);
            return false;
        });


    // HEADER SCROLL
    // add .header-static for .page or body
    // to disable sticky header
    function initHeaderScroll() {
        _window.on('scroll', throttle(function (e) {
            var vScroll = _window.scrollTop();
            var header = $('.header').not('.header--static');
            var headerHeight = header.height();
            var firstSection = _document.find('.page__content div:first-child()').height() - headerHeight;
            var visibleWhen = Math.round(_document.height() / _window.height()) > 2.5

            if (visibleWhen) {
                if (vScroll > headerHeight) {
                    header.addClass('is-fixed');
                } else {
                    header.removeClass('is-fixed');
                }
                if (vScroll > firstSection) {
                    header.addClass('is-fixed-visible');
                } else {
                    header.removeClass('is-fixed-visible');
                }
            }
        }, 10));
    }


    // HAMBURGER TOGGLER
    _document.on('click', '.js-hamburger', function () {
        $(this).toggleClass('is-active');
        $('.mobile-navi').toggleClass('is-active');
        $('.mobile-menu').css({'top': '200px', 'bottom': 'auto'})
    });

    _document.on('click', '.js-hamburger-footer', function () {
        $(this).toggleClass('is-active');
        $('.mobile-menu-footer').toggleClass('is-active');
        $('.mobile-navi-footer').toggleClass('is-active');
        $('.mobile-menu-footer').css({'bottom': '345px', 'top': 'auto'})
    });


    // SET ACTIVE CLASS IN HEADER
    // * could be removed in production and server side rendering when header is inside barba-container
    function updateHeaderActiveClass() {
        $('.header__menu li').each(function (i, val) {
            if ($(val).find('a').attr('href') == window.location.pathname.split('/').pop()) {
                $(val).addClass('is-active');
            } else {
                $(val).removeClass('is-active')
            }
        });
    }

    //////////
    // SLIDERS
    //////////

    function sliderCreator(parent, mobileSwiperWprapper, mobileSwiperItem, swiperConfig) {
        var sliderParent = $(parent).html();
        $(mobileSwiperWprapper).html(sliderParent);
        $(mobileSwiperItem).addClass('swiper-slide');
        new Swiper(swiperConfig.class, {
            slidesPerView: 1,
            loop: true,
            navigation: {
                nextEl: swiperConfig.next,
                prevEl: swiperConfig.prev,
            },
        });
    }

    function uiTransformMobile() {
        if (_window.width() <= 568) {
            var search = $('.collection-top-info .search');

            $('.forum-list__item').each(function () {
                var info = $(this).find('.forum-list-info');
                $(this).find('.js-mobile-info').append(info);
            });

            $('.js-append-search-in-mobile').append(search);
        }

        if (_window.width() <= 1170) {

            sliderCreator(
                '.homepage .company-news-content',
                '.company-news-swiper-mobile .swiper-wrapper',
                '.company-news-swiper-mobile .company-news-content__item',
                {
                    class: '.company-news-swiper-mobile',
                    next: '.news-btn-next',
                    prev: '.news-btn-prev',
                }
            );

            sliderCreator(
                '.partners-content',
                '.partners-mobile-swiper .swiper-wrapper',
                '.partners-mobile-swiper .partners-logo',
                {
                    class: '.partners-swiper-mobile',
                    next: '.partners-btn-next',
                    prev: '.partners-btn-prev',
                },
            );
        }

        if (_window.width() <= 768) {
            $('.mobile-btns').append($('.search'));
        }
    }

    function searchToggle() {
        $('.js-search-toggle').click(function () {
            $('.search').fadeToggle();
        });
    }

    function initSliders() {

        var swiperCongig = {
            speed: 1000,
            loop: true,
        };

        var galleryTop = new Swiper('.gallery-top', {
            thumbs: galleryThumbs,
            loop: swiperCongig.loop,
            speed: swiperCongig.speed,
            navigation: {
                nextEl: '.gallery-next',
                prevEl: '.gallery-prev',
            },
        });

        var galleryThumbs = new Swiper('.gallery-thumbs', {
            slidesPerView: 3,
            slidesPerColumn: 3,
            spaceBetween: 20,
            slideToClickedSlide: true,
            loop: swiperCongig.loop,
            speed: swiperCongig.speed,
        });

        if ($('body').hasClass('card-model-page')) {
            // galleryTop.controller.control = galleryThumbs;
            // galleryThumbs.controller.control = galleryTop;
        }

        new Swiper('.new-product-swiper', {
            slidesPerView: 4,
            // loop: swiperCongig.loop,
            speed: swiperCongig.speed,
            // slideToClickedSlide: true,
            pagination: {
                el: '.new-swiper-pagination',
                type: 'fraction',
            },
            navigation: {
                nextEl: '.new-button-next',
                prevEl: '.new-button-prev',
            },
            breakpoints: {
                1170: {
                    slidesPerView: 3,
                },
                815: {
                    slidesPerView: 2,
                },
                568: {
                    slidesPerView: 1,
                },
            },
        });

        new Swiper('.homepage-swiper', {
            loop: swiperCongig.loop,
            speed: swiperCongig.speed,
            pagination: {
                el: '.homepage-pagination',
                type: 'fraction',
            },
            noSwiping: true,
            navigation: {
                nextEl: '.homepage-button-next',
                prevEl: '.homepage-button-prev',
            },
        });

        new Swiper('.swiper-collection-carousel', {
            loop: swiperCongig.loop,
            speed: swiperCongig.speed,
            slidesPerView: 4,
            spaceBetween: 20,
            breakpoints: {
                992: {
                    slidesPerView: 3,
                },
                568: {
                    slidesPerView: 2,
                },
                425: {
                    slidesPerView: 1,
                },
            }
        });

        new Swiper('.events-swiper-container', {
            loop: swiperCongig.loop,
            speed: swiperCongig.speed,
            slidesPerView: 1,
            slidesPerColumn: 3,
            spaceBetween: 10,
            navigation: {
                nextEl: '.events-button-next',
                prevEl: '.events-button-prev',
            },
        });

        // .swiper-container-download
        new Swiper('.swiper-container-download', {
            speed: swiperCongig.speed,
            slidesPerView: 2,
            slidesPerColumn: 2,
            spaceBetween: 20,
            navigation: {
                nextEl: '.download-button-next',
                prevEl: '.download-button-prev',
            },
            breakpoints: {
                568: {
                    slidesPerView: 1,
                    slidesPerColumn: 1,
                }
            },
        });

        var materialsTop = new Swiper('.materials-top', {
            spaceBetween: 10,
            loop: swiperCongig.loop,
            speed: swiperCongig.speed,
            thumbs: materialsThumbs,
            navigation: {
                nextEl: '.materials-button-next',
                prevEl: '.materials-button-prev',
            },
        });

        var materialsThumbs = new Swiper('.materials-thumbs', {
            spaceBetween: 20,
            slidesPerView: 4,
            slideToClickedSlide: true,
            loop: swiperCongig.loop,
            speed: swiperCongig.speed,
            breakpoints: {
                568: {
                    slidesPerView: 3,
                },
            },
        });

        new Swiper('.news-more-container', {
            loop: swiperCongig.loop,
            speed: swiperCongig.speed,
            slidesPerView: 4,
            spaceBetween: 20,
            navigation: {
                nextEl: '.news-button-next',
                prevEl: '.news-button-prev',
            },
            breakpoints: {
                1170: {
                    slidesPerView: 3,
                },
                768: {
                    slidesPerView: 2,
                },
                425: {
                    slidesPerView: 1,
                },
            }
        });


    }

    //////////
    // MODALS
    //////////

    function initPopups() {
        // Magnific Popup
        var startWindowScroll = 0;
        $('.js-popup').magnificPopup({
            type: 'inline',
            fixedContentPos: true,
            fixedBgPos: true,
            overflowY: 'auto',
            closeBtnInside: true,
            preloader: false,
            midClick: true,
            removalDelay: 300,
            mainClass: 'popup-buble',
            callbacks: {
                beforeOpen: function () {
                    startWindowScroll = _window.scrollTop();
                },
                close: function () {
                    _window.scrollTop(startWindowScroll);
                }
            }
        });

        $('.js-popup-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Загрузка #%curr%...',
            mainClass: 'popup-buble',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1]
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
            }
        });
    }

    function closeMfp() {
        $.magnificPopup.close();
    }


    ////////////
    // UI
    ////////////

    // textarea autoExpand
    _document
        .one('focus.autoExpand', '.ui-group textarea', function () {
            var savedValue = this.value;
            this.value = '';
            this.baseScrollHeight = this.scrollHeight;
            this.value = savedValue;
        })
        .on('input.autoExpand', '.ui-group textarea', function () {
            var minRows = this.getAttribute('data-min-rows') | 0, rows;
            this.rows = minRows;
            rows = Math.ceil((this.scrollHeight - this.baseScrollHeight) / 17);
            this.rows = minRows + rows;
        });

    // Masked input
    function initMasks() {
        $(".js-dateMask").mask("99.99.99", {placeholder: "ДД.ММ.ГГ"});
        $("input[type='tel']").mask("+7 (000) 000-0000");
    }

    // selectric
    function initSelectric() {
        $('select').selectric({
            maxHeight: 300,
            arrowButtonMarkup: '<b class="button"></b>',

            onInit: function (element, data) {
                var $elm = $(element),
                    $wrapper = $elm.closest('.' + data.classes.wrapper);

                $wrapper.find('.label').html($elm.attr('placeholder'));
            },
            onBeforeOpen: function (element, data) {
                var $elm = $(element),
                    $wrapper = $elm.closest('.' + data.classes.wrapper);

                $wrapper.find('.label').data('value', $wrapper.find('.label').html()).html($elm.attr('placeholder'));
            },
            onBeforeClose: function (element, data) {
                var $elm = $(element),
                    $wrapper = $elm.closest('.' + data.classes.wrapper);

                $wrapper.find('.label').html($wrapper.find('.label').data('value'));
            }
        });
    }

    // CUSTOM SCRIPTS

    // tabsNavigation
    function tabsNavigation() {

        var tabsTotalCount = $('.tabs__for .tab').length - 1;

        $('.js-btn-next').on('click', function nexTabFunc() {
            var thisTabIndex = $('.tabs__for .active').index();
            var nextTab = ++thisTabIndex;
            if (nextTab <= tabsTotalCount) {
                $('.tabs').find('.tabs__nav .tab, .tabs__for .tab').removeClass('active');
                $('.tabs').find('.tabs__nav .tab').eq(nextTab).addClass('active');
                $('.tabs').find('.tabs__for .tab').eq(nextTab).addClass('active');
            }
        });

        $('.js-btn-prev').on('click', function prevTabFunc() {
            var thisTabIndex = $('.tabs__for .active').index();
            var prevTab = --thisTabIndex;
            if (prevTab > tabsTotalCount || prevTab >= 0) {
                $('.tabs').find('.tabs__nav .tab, .tabs__for .tab').removeClass('active');
                $('.tabs').find('.tabs__nav .tab').eq(prevTab).addClass('active');
                $('.tabs').find('.tabs__for .tab').eq(prevTab).addClass('active');
            }
        });
    }

    function setActive() {
        $('.card-model-gallery-for .tab').first().addClass('active');
        $('.card-model-gallery-nav .tab').first().addClass('active');
    }

    function accordion() {
        $('.model-filter__item .model-filter-title').click(function () {
            $(this).siblings('.model-filter-content').fadeToggle();
        });
    }

    $('.scroll-navigation').click(function () {
        setTimeout(function () {
            var pos = document.getElementById("scroll").scrollTop;
            visibleFooter(pos);
        }, 1600)
    })

    function visibleFooter(count) {
        count > 2000 ? $('footer').fadeIn() : $('footer').hide()
    }

    function highlightScrollNav(position) {
        $('.scroll-navigation a').each(function () {
            if ($(this).data().position == position) {
                $(this).addClass('current-section');
            } else {
                $(this).removeClass('current-section');
            }
        });
    }

    function ScrollHandler(pageId, navId) {

        var page = document.getElementById(pageId);
        var pageStart = page.offsetTop;
        var pageJump = false;
        var viewStart;
        var duration = 800;
        var scrolled = document.getElementById("scroll");

        var navElement = '<a data-position="' + navId + '" href="#' + pageId + '" > - </a>';
        $('.scroll-navigation').append(navElement);

        function scrollToPage() {
            pageJump = true;

            var startLocation = viewStart;
            var endLocation = pageStart;
            var distance = endLocation - startLocation;

            var runAnimation;

            var timeLapsed = 0;
            var percentage, position;

            var easing = function (progress) {
                return progress < 0.5
                    ? 4 * progress * progress * progress
                    : (progress - 1) * (2 * progress - 2) * (2 * progress - 2) + 1;
            };

            function stopAnimationIfRequired(pos) {
                if (pos == endLocation) {
                    cancelAnimationFrame(runAnimation);
                    setTimeout(function () {
                        pageJump = false;
                    }, 500);
                }
            }

            var animate = function () {
                timeLapsed += 16;
                percentage = timeLapsed / duration;
                if (percentage > 1) {
                    percentage = 1;
                    position = endLocation;
                } else {
                    position = startLocation + distance * easing(percentage);
                }
                scrolled.scrollTop = position;
                scrollPos = scrolled.scrollTop;
                runAnimation = requestAnimationFrame(animate);
                stopAnimationIfRequired(position);
                visibleFooter(scrolled.scrollTop);

            };
            runAnimation = requestAnimationFrame(animate);
        }

        window.addEventListener("wheel", function (event) {

            viewStart = scrolled.scrollTop;

            if (!pageJump) {
                var pageHeight = page.scrollHeight;
                var pageStopPortion = pageHeight / 2;
                var viewHeight = window.innerHeight;

                var viewEnd = viewStart + viewHeight;
                var pageStartPart = viewEnd - pageStart;
                var pageEndPart = pageStart + pageHeight - viewStart;

                var canJumpDown = pageStartPart >= 0;
                var stopJumpDown = pageStartPart > pageStopPortion;

                var canJumpUp = pageEndPart >= 0;
                var stopJumpUp = pageEndPart > pageStopPortion;

                var scrollingForward = event.deltaY > 0;
                if (
                    (scrollingForward && canJumpDown && !stopJumpDown) ||
                    (!scrollingForward && canJumpUp && !stopJumpUp)
                ) {
                    event.preventDefault();
                    scrollToPage();
                }
                false;

            } else {
                event.preventDefault();
            }
        }, {passive: false});

    }

    if ($('body').hasClass('homepage') && _window.width() > 1170) {
        new ScrollHandler("section-js-slide-1", elPosition[0]);
        new ScrollHandler("section-js-slide-2", elPosition[1]);
        new ScrollHandler("section-js-slide-3", elPosition[2]);
        new ScrollHandler("section-js-slide-4", elPosition[3]);
        highlightScrollNav(0);
    }

    $('.scroll-navigation a').click(function () {
        $(this).addClass('current-section');
        $('.scroll-navigation a').not(this).removeClass('current-section');
    });

    _window.on('wheel', function () {
        setTimeout(function () {
            highlightScrollNav(scrollPos);
        }, 1600);
    });

    // tabs logic
    function customTabs() {
        $('.tabs .tabs__nav .tab').on('click', function () {
            if ($(this).hasClass('active')) {
            } else {
                var currentTab = $(this).index();
                $('.tabs .tabs__nav .tab, .tabs .tabs__for .tab').removeClass('active');
                $(this).addClass('active');
                $('.tabs .tabs__for .tab').eq(currentTab).addClass('active');
            }
        });
    }

    function checkBox() {
        $('.ui-checkbox input').on('click', function () {
            $(this).closest('.ui-checkbox').toggleClass('active');
        });
    }

});

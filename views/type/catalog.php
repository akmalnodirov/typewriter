<section class="collection-info decor-lines">
    <div class="container">
        <!-- top-->
        <div class="collection-top-info row">
            <!-- section breadcrumbs-->
            <div class="breadcrumbs">
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#"><?= Yii::t('type', 'Главная') ?></a></li>
                        <li class="breadcrumb-item active"><?= Yii::t('type', 'Коллекции') ?></li>
                    </ol>
                </nav>
            </div>
            <!-- search-->
            <div class="search">
                <form>
                    <input type="search" placeholder="<?= Yii::t('type', 'Поиск по коллекциям') ?>" id="catalog_search">
                    <button class="search-submit">
                        <svg class="ico ico-search">
                            <use xlink:href="/typewriter/img/sprite.svg#ico-search"></use>
                        </svg>
                    </button>
                </form>
            </div>
        </div>
        <!-- title-->
        <div class="collection-title-info row">
            <div class="section-title">
                <h1><?= Yii::t('type', 'Коллекции') ?></h1>
            </div>
            <div class="js-append-search-in-mobile"></div>
            <div class="alphabet row">
                <button class="catalog_symbol alphabet__item row" symbol="A">
                    <img src="/typewriter/img/1.png" alt="">
                </button>
                <button class="catalog_symbol alphabet__item row" symbol="B">
                    <img src="/typewriter/img/2.png" alt="">
                </button>
                <button class="catalog_symbol alphabet__item row" symbol="C">
                    <img src="/typewriter/img/3.png" alt="">
                </button>
                <button class="catalog_symbol alphabet__item row" symbol="D">
                    <img src="/typewriter/img/4.png" alt="">
                </button>
                <button class="catalog_symbol alphabet__item row" symbol="E">
                    <img src="/typewriter/img/5.png" alt="">
                </button>
                <button class="catalog_symbol alphabet__item row" symbol="F">
                    <img src="/typewriter/img/6.png" alt="">
                </button>
                <button class="catalog_symbol alphabet__item row" symbol="G">
                    <img src="/typewriter/img/7.png" alt="">
                </button>
                <button class="catalog_symbol alphabet__item row" symbol="H">
                    <img src="/typewriter/img/8.png" alt="">
                </button>
                <button class="catalog_symbol alphabet__item row" symbol="I">
                    <img src="/typewriter/img/9.png" alt="">
                </button>
                <button class="catalog_symbol alphabet__item row" symbol="J">
                    <img src="/typewriter/img/10.png" alt="">
                </button>
                <button class="catalog_symbol alphabet__item row" symbol="K">
                    <img src="/typewriter/img/11.png" alt="">
                </button>
                <button class="catalog_symbol alphabet__item row" symbol="L">
                    <img src="/typewriter/img/12.png" alt="">
                </button>
                <button class="catalog_symbol alphabet__item row" symbol="M">
                    <img src="/typewriter/img/13.png" alt="">
                </button>
                <button class="catalog_symbol alphabet__item row" symbol="N">
                    <img src="/typewriter/img/14.png" alt="">
                </button>
                <button class="catalog_symbol alphabet__item row" symbol="O">
                    <img src="/typewriter/img/15.png" alt="">
                </button>
                <button class="catalog_symbol alphabet__item row" symbol="P">
                    <img src="/typewriter/img/16.png" alt="">
                </button>
                <button class="catalog_symbol alphabet__item row" symbol="Q">
                    <img src="/typewriter/img/17.png" alt="">
                </button>
                <button class="catalog_symbol alphabet__item row" symbol="R">
                    <img src="/typewriter/img/18.png" alt="">
                </button>
                <button class="catalog_symbol alphabet__item row" symbol="S">
                    <img src="/typewriter/img/19.png" alt="">
                </button>
                <button class="catalog_symbol alphabet__item row" symbol="T">
                    <img src="/typewriter/img/20.png" alt="">
                </button>
                <button class="catalog_symbol alphabet__item row" symbol="U">
                    <img src="/typewriter/img/21.png" alt="">
                </button>
                <button class="catalog_symbol alphabet__item row" symbol="V">
                    <img src="/typewriter/img/22.png" alt="">
                </button>
                <button class="catalog_symbol alphabet__item row" symbol="W">
                    <img src="/typewriter/img/23.png" alt="">
                </button>

                <button class="catalog_symbol alphabet__item row" symbol="X">
                    <img src="/typewriter/img/24.png" alt="">
                </button>
                <button class="catalog_symbol alphabet__item row" symbol="Y">
                    <img src="/typewriter/img/25.png" alt="">
                </button>
                <button class="catalog_symbol alphabet__item row" symbol="Z">
                    <img src="/typewriter/img/26.png" alt="">
                </button>
            </div>
        </div>
    </div>
</section>
<main class="collection row">
    <div class="container" >
        <div class="collection-content row" id="catalogs_area">
        </div>
        <div class="collection-content-btn row" id="show_all_button_div">
            <a class="btn btn-transparent" href="" id="show_all_button">
                <span class="arrow-txt"><?=Yii::t('type','Показать все')?></span>
                <span class="arrow-icon">
                  <svg class="ico ico-arrow-angle">
                    <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                  </svg></span>
            </a>
        </div>

    </div>
</main>


<?php

use yii\helpers\Url;

?>
<section class="breadcrumbs">
    <div class="container">
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?=Url::to(['/type/index'])?>"><?=Yii::t('type','Главная')?></a></li>
                <li class="breadcrumb-item"><a href="<?=Url::to(['/type/news'])?>"><?=Yii::t('type','Новости')?></a></li>
                <li class="breadcrumb-item active"><?=$model->title?></li>
            </ol>
        </nav>
    </div>
</section>
<article class="news-article">
    <div class="container">
        <div class="news-article-img"> <img src="/uploads<?=$model->image?>" alt=""></div>
        <div class="section-title">
            <h1><?=Yii::t('type','Политика конфидециальности')?></h1>
        </div>
        <div class="news-article-wrapper">
            <h2><?=$model->title?></h2>
            <?=htmlspecialchars_decode($model->text)?>
            <div class="article-date row">
                <p><?=Yii::t('type','Добавлено')?> : <?=Yii::$app->formatter->asDate($model->created_at, 'dd.mm.yyyy')?></p>
            </div>
        </div>
    </div>
</article>
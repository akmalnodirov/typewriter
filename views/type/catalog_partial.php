<?php

use yii\helpers\Url;

?>
    <?php foreach ($catalogs as $key => $catalog): ?>
        <div class="collection-content__item row">
            <!-- main-->
            <div class="collection-main-wrapper row">
                <div class="collection-main-img row"><img src="/uploads<?= $catalog->logo ?>" alt=""/></div>
                <div class="collection-main-title">
                    <h3><?= $catalog->name ?></h3>
                    <a class="hover-link" href="<?= Url::to(['/type/one-collection', 'slug' => $catalog->slug]) ?>">
                        <span class="arrow-txt"><?=Yii::t('type','Показать все')?></span>
                        <span class="arrow-icon">
                        <svg class="ico ico-arrow-angle">
                          <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                        </svg>
                    </span>
                    </a>
                </div>
            </div>
            <!-- img-->
            <div class="collection-img-wrapper">
                <ul class="row">
                    <?php if ($catalog->collectionImages): ?>
                        <?php foreach ($catalog->collectionImages as $imageKey => $image): ?>
                            <?php if ($imageKey > 3) {
                                break;
                            } ?>
                            <li class="row">
                                <img src="/uploads<?= $image->image ?>" alt=""/>
                            </li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </div>
            <!-- description-->
            <div class="collection-description"><span><?= $catalog->title ?></span></div>
        </div>
    <?php endforeach; ?>


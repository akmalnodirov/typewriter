<?php

use yii\helpers\Url;

?>
<div class="goods-response row">
    <?php foreach ($models as $modelKey => $modelItem): ?>
        <div class="catalog-goods-item hover-link"><a class="all-goods-link" href="#"></a>
            <div class="goods-stratus row">
                <p><?= $modelItem->old ?></p>
                <svg class="ico ico-goods-star">
                    <use xlink:href="/typewriter/img/sprite.svg#ico-goods-star"></use>
                </svg>
            </div>
            <div class="catalog-goods-wrapper">
                <div class="catalog-goods-top"><img src="/uploads<?= $modelItem->image ?>" alt=""/>
                    <p><?= $modelItem->name ?></p>
                </div>
                <div class="catalog-goods-btn">
                    <a href="<?=Url::to(['/type/one-model', 'slug' => $modelItem->slug])?>">
                        <span class="arrow-txt"><?=Yii::t('type','Подробнее')?></span>
                        <span class="arrow-icon">
                            <svg class="ico ico-arrow-angle">
                              <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                            </svg>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>

</div>

<?php if (count($models) >= 16): ?>
    <div class="goods-btn row" id="collection_show_all_button_div">
        <a class="btn-transparent hover-link" href="#" id="collection_show_all_button">
            <span class="arrow-txt"><?=Yii::t('type','Все варианты')?></span><span class="arrow-icon">
                      <svg class="ico ico-arrow-angle">
                        <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                      </svg></span>
        </a>
    </div>
<?php endif ?>

<?php

use yii\helpers\Url;

$settings = $this->params['settings'];

?>


<section class="card-model-gallery decor-lines" style="background-image:  url('/uploads<?=\app\components\Helpers::convertPathToUrl($settings->card_model_bg)?>')">
    <div class="container">
        <!-- section breadcrumbs-->
        <div class="breadcrumbs">
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a
                                href="<?= Url::to(['/type/index']) ?>"><?= Yii::t('type', 'Главная') ?></a></li>
                    <li class="breadcrumb-item"><a
                                href="<?= Url::to(['/type/catalog']) ?>"><?= Yii::t('type', 'Коллекции') ?></a></li>
                    <li class="breadcrumb-item"><a
                                href="<?= Url::to(['/type/one-collectin', 'slug' => $model->collection->slug]) ?>"><?= $model->collection->name ?></a>
                    </li>
                    <li class="breadcrumb-item active"><?= $model->name ?></li>
                </ol>
            </nav>
        </div>
        <!-- section-title -->
        <div class="section-title">
            <h1><?= $model->name ?></h1>
        </div>
        <!-- card-model-gallery-wrapper-->
        <div class="tabs card-model-gallery-wrapper row">
            <div class="tabs__for row card-model-gallery-for">
                <?php foreach ($model->collectionModelImages as $modelKey => $modelValue): ?>
                    <div class="tab"><img src="/uploads<?= $modelValue->image ?>" alt=""></div>
                <?php endforeach; ?>

                <div class="card-model-btn-next nav-btn-prev js-btn-next">
                    <svg class="ico ico-arrow-angle">
                        <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                    </svg>
                </div>
                <div class="card-model-btn-prev nav-btn-next js-btn-prev">
                    <svg class="ico ico-arrow-angle">
                        <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                    </svg>
                </div>
            </div>
            <div class="tabs__nav card-model-gallery-nav">
                <?php foreach ($model->collectionModelImages as $modelKey2 => $modelValue2): ?>
                    <div class="tab"><img src="/uploads<?= $modelValue2->image ?>" alt=""></div>
                <?php endforeach; ?>

            </div>
        </div>
        <!-- card-model-gallery-btn-->
        <div class="card-model-gallery-btn row"></div>
    </div>
</section>
<!-- card-model-info-->
<section class="card-model-info decor-lines">
    <div class="container">
        <!-- info-wrapper-->
        <div class="card-model-info-wrapper row">
            <div class="card-model-info-wrapper__item">
                <div class="decor-line-title">
                    <h2><?= Yii::t('type', 'Описание') ?> </h2>
                </div>
                <!-- card-model-description-->
                <div class="card-model-description-wrapper">
                    <article class="card-model-description">
                        <?= $model->description ?>
                    </article>
                </div>
            </div>
            <div class="card-model-info-wrapper__item characteristic">
                <div class="decor-line-title">
                    <h2><?= Yii::t('type', 'Характеристики') ?></h2>
                </div>
                <!-- card-model-characteristic-->
                <div class="card-model-characteristic">
                    <ul class="characteristic-list">
                        <?php if ($model->collectionModelCharacteristics): ?>
                            <?php foreach ($model->collectionModelCharacteristics as $characterKey => $characterValue): ?>
                                <li class="row">
                                    <p><?= $characterValue->key ?></p><span><?= $characterValue->value ?></span>
                                </li>
                            <?php endforeach ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
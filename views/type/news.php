<?php

use yii\helpers\Url;

?>
<section class="breadcrumbs">
    <div class="container">
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?=Url::to(['/type/index'])?>"><?=Yii::t('type','Главная')?></a></li>
                <li class="breadcrumb-item active"><?=Yii::t('type','Новости')?></li>
            </ol>
        </nav>
    </div>
</section>
<!-- all-news-->
<main class="all-news-section">
    <div class="container">
        <!-- title-->
        <div class="section-title">
            <h1><?=Yii::t('type','Новости')?></h1>
        </div>
        <!-- all-news-->
        <div class="company-news-content row" id="news_area">

        </div>
        <div class="company-news-read-more row" id="show_all_news_button_div">
            <a class="btn-transparent hover-link" href="#" id="show_all_news_button">
                <span class="arrow-txt"><?=Yii::t('type','Все новости')?></span>
                <span class="arrow-icon">
                  <svg class="ico ico-arrow-angle">
                    <use xlink:href="img/sprite.svg#ico-arrow-angle"></use>
                  </svg></span>
            </a>
        </div>
    </div>
</main>
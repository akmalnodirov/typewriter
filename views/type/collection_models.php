<?php

use app\models\Collection;
use yii\helpers\Url;

?>

<section class="breadcrumbs">
    <div class="container">
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?=Url::to(['/type/index'])?>"><?=Yii::t('type','Главная')?></a></li>
                <li class="breadcrumb-item"><a href="<?=Url::to(['/type/catalog'])?>"><?=Yii::t('type','Коллекции')?></a></li>
                <li class="breadcrumb-item"><a href="#" id="collection_title"><?=$collection->name?></a></li>
                <li class="breadcrumb-item active"><?=Yii::t('type','Каталог моделей')?></li>
            </ol>
        </nav>
    </div>
</section>
<main class="model-catalog">
    <div class="container">
        <!-- title-->
        <div class="section-title">
            <h1><?=Yii::t('type','Каталог моделей')?></h1>
        </div>
        <!-- model-catalog-goods-->
        <div class="model-catalog-goods row">
            <!-- filter-->
            <aside class="model-catalog-goods__filter">
                <div class="model-filter js-accordion">
                    <!-- i-->
                    <div class="model-filter__item">
                        <div class="model-filter-title">
                            <p><?=Yii::t('type','Поиск')?></p>
                        </div>
                        <div class="model-filter-content">
                            <!-- search-->
                            <div class="search">
                                <form>
                                    <input type="search" placeholder="<?=Yii::t('type','Поиск по названию')?>" id="collection_model_search">
                                    <button class="search-submit">
                                        <svg class="ico ico-search">
                                            <use xlink:href="/typewriter/img/sprite.svg#ico-search"></use>
                                        </svg>
                                    </button>
                                </form>
                            </div>
                            <!-- alphabet-->
                            <div class="alphabet row">
                                <button class="collection_model_symbol alphabet__item row" symbol="A">
                                    <img src="/typewriter/img/1.png" alt="">
                                </button>
                                <button class="collection_model_symbol alphabet__item row" symbol="B">
                                    <img src="/typewriter/img/2.png" alt="">
                                </button>
                                <button class="collection_model_symbol alphabet__item row" symbol="C">
                                    <img src="/typewriter/img/3.png" alt="">
                                </button>
                                <button class="collection_model_symbol alphabet__item row" symbol="D">
                                    <img src="/typewriter/img/4.png" alt="">
                                </button>
                                <button class="collection_model_symbol alphabet__item row" symbol="E">
                                    <img src="/typewriter/img/5.png" alt="">
                                </button>
                                <button class="collection_model_symbol alphabet__item row" symbol="F">
                                    <img src="/typewriter/img/6.png" alt="">
                                </button>
                                <button class="collection_model_symbol alphabet__item row" symbol="G">
                                    <img src="/typewriter/img/7.png" alt="">
                                </button>
                                <button class="collection_model_symbol alphabet__item row" symbol="H">
                                    <img src="/typewriter/img/8.png" alt="">
                                </button>
                                <button class="collection_model_symbol alphabet__item row" symbol="I">
                                    <img src="/typewriter/img/9.png" alt="">
                                </button>
                                <button class="collection_model_symbol alphabet__item row" symbol="J">
                                    <img src="/typewriter/img/10.png" alt="">
                                </button>
                                <button class="collection_model_symbol alphabet__item row" symbol="K">
                                    <img src="/typewriter/img/11.png" alt="">
                                </button>
                                <button class="collection_model_symbol alphabet__item row" symbol="L">
                                    <img src="/typewriter/img/12.png" alt="">
                                </button>
                                <button class="collection_model_symbol alphabet__item row" symbol="M">
                                    <img src="/typewriter/img/13.png" alt="">
                                </button>
                                <button class="collection_model_symbol alphabet__item row" symbol="N">
                                    <img src="/typewriter/img/14.png" alt="">
                                </button>
                                <button class="collection_model_symbol alphabet__item row" symbol="O">
                                    <img src="/typewriter/img/15.png" alt="">
                                </button>
                                <button class="collection_model_symbol alphabet__item row" symbol="P">
                                    <img src="/typewriter/img/16.png" alt="">
                                </button>
                                <button class="collection_model_symbol alphabet__item row" symbol="Q">
                                    <img src="/typewriter/img/17.png" alt="">
                                </button>
                                <button class="collection_model_symbol alphabet__item row" symbol="R">
                                    <img src="/typewriter/img/18.png" alt="">
                                </button>
                                <button class="collection_model_symbol alphabet__item row" symbol="S">
                                    <img src="/typewriter/img/19.png" alt="">
                                </button>
                                <button class="collection_model_symbol alphabet__item row" symbol="T">
                                    <img src="/typewriter/img/20.png" alt="">
                                </button>
                                <button class="collection_model_symbol alphabet__item row" symbol="U">
                                    <img src="/typewriter/img/21.png" alt="">
                                </button>
                                <button class="collection_model_symbol alphabet__item row" symbol="V">
                                    <img src="/typewriter/img/22.png" alt="">
                                </button>
                                <button class="collection_model_symbol alphabet__item row" symbol="W">
                                    <img src="/typewriter/img/23.png" alt="">
                                </button>

                                <button class="collection_model_symbol alphabet__item row" symbol="X">
                                    <img src="/typewriter/img/24.png" alt="">
                                </button>
                                <button class="collection_model_symbol alphabet__item row" symbol="Y">
                                    <img src="/typewriter/img/25.png" alt="">
                                </button>
                                <button class="collection_model_symbol alphabet__item row" symbol="Z">
                                    <img src="/typewriter/img/26.png" alt="">
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- i-->
                    <div class="model-filter__item">
                        <div class="model-filter-title">
                            <p><?=Yii::t('type','Производитель')?></p>
                        </div>
                        <div class="model-filter-content">
                            <div class="manufacturer row">

                                <?php foreach (Collection::getModelsCountWithName() as $countNameKey => $countNameValue): ?>
                                    <div class="manufacturer__item">
                                        <input type="checkbox" id="checkbox-<?=$countNameKey?>">
                                        <label for="checkbox-<?=$countNameKey?>" class="collection_name_with_count" collection_id="<?=$countNameValue['collection_id']?>" count="<?=$countNameValue['modelsCount']?>" style="cursor:pointer;"><?= mb_substr($countNameValue['collection_name'], 0, 9) ?></label>
                                        <sup>(<?= $countNameValue['modelsCount'] ?>)</sup>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="manufacturer-more row">
                                <svg class="ico ico-arrow-angle">
                                    <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>
            <div class="model-catalog-goods__result" id="collection_models_area" collection_id="<?=$collection->id?>">


            </div>
        </div>
    </div>
</main>
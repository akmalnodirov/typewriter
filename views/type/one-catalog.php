<?php

use yii\helpers\Url;

?>

<section class="breadcrumbs">
    <div class="container">
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= Url::to(['/type/index']) ?>"><?=Yii::t('type','Главная')?></a></li>
                <li class="breadcrumb-item"><a href="<?= Url::to(['/type/catalog']) ?>"><?=Yii::t('type','Коллекции')?></a></li>
                <li class="breadcrumb-item active"><?= $collection ? $collection->title : 'Нет такого' ?></li>
            </ol>
        </nav>
    </div>
</section>
<!-- .card-collection-info-->
<main class="card-collection-info decor-lines">
    <div class="container">
        <!-- top-info-->
        <div class="card-collection-top-info row">
            <div class="section-title">
                <h1><?= $collection->title ?></h1>
            </div>
            <div class="card-logo"><img src="/uploads<?= $collection->logo ?>" alt=""></div>
        </div>
        <!-- collection-carousel-->
        <div class="card-collection-carousel">
            <div class="swiper-container swiper-collection-carousel">
                <div class="swiper-wrapper">
                    <?php if ($collection->collectionImages): ?>
                        <?php foreach ($collection->collectionImages as $image): ?>
                            <div class="swiper-slide row"><img src="/uploads<?= $image->image ?>" alt=""></div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!-- card-model-info-wrapper-->
        <div class="card-model-info-wrapper row">
            <!-- item-->
            <div class="card-model-info-wrapper__item collection-txt">
                <h2><?=Yii::t('type','История')?></h2>
                <div class="card-model-description-wrapper">
                    <article class="card-model-description">
                        <?= $collection->history ?>
                    </article>
                </div>
                <div class="card-model-btns row">
                    <a class="btn" href="<?=Url::to(['type/collection-models', 'slug' => $collection->slug])?>">
                        <span class="arrow-txt"><?=Yii::t('type','Модели')?></span>
                        <span class="arrow-icon">
                      <svg class="ico ico-arrow-angle">
                        <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                      </svg></span></a>
                    <a class="hover-link btn-transparent" href="<?=Url::to(['type/collection-forum', 'slug' => $collection->slug])?>">
                        <span class="arrow-txt"><?=Yii::t('type','Форум')?></span>
                        <span class="arrow-icon"><svg class="ico ico-arrow-angle">
                        <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                        </svg>
                        </span>
                    </a>
                </div>
            </div>
            <!-- item-->
            <div class="card-model-info-wrapper__item collection-carousel">
                <h2>События</h2>
                <div class="swiper-container events-swiper-container">
                    <div class="swiper-wrapper">
                        <?php if ($collection->collectionEvents): ?>
                            <?php foreach ($collection->collectionEvents as $event): ?>
                                <div class="swiper-slide collection-carousel-item">
                                    <time><?= $event->event_date ?></time>
                                    <p><?= $event->event_description ?></p>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                    <div class="nav-btn row">
                        <div class="nav-btn-next nav-border swiper-button-prev events-button-prev">
                            <svg class="ico ico-arrow-angle">
                                <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                            </svg>
                        </div>
                        <div class="nav-btn-prev nav-border swiper-button-next events-button-next">
                            <svg class="ico ico-arrow-angle">
                                <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!-- additional-materials-->
<section class="additional-materials-section">
    <div class="container">
        <div class="section-title">
            <h3><?=Yii::t('type','Дополнительные материалы')?></h3>
        </div>
        <!-- additional-materials-->
        <div class="additional-materials row">
            <!-- carousel-->
            <div class="additional-materials-carousel additional-materials__item">
                <!-- materials-top-->
                <?php if ($collection->collectionDetails): ?>
                <div class="materials-top swiper-container">
                    <div class="swiper-wrapper">

                            <?php foreach ($collection->collectionDetails as $detail): ?>
                                <div class="swiper-slide row"><img src="/uploads<?= $detail->image ?>" alt=""></div>
                            <?php endforeach; ?>

                    </div>
                    <div class="materials-button-next swiper-button-next nav-btn-next gallery-next">
                        <svg class="ico ico-arrow-angle">
                            <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                        </svg>
                    </div>
                    <div class="materials-button-prev swiper-button-prev nav-btn-prev gallery-prev">
                        <svg class="ico ico-arrow-angle">
                            <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                        </svg>
                    </div>
                </div>
                <?php endif ?>
                <!-- materials-thumbs-->
                <?php if ($collection->collectionDetails): ?>
                <div class="materials-thumbs swiper-container">
                    <div class="swiper-wrapper">


                            <?php foreach ($collection->collectionDetails as $detail): ?>
                                <div class="swiper-slide row"><img src="/uploads<?= $detail->image ?>" alt=""></div>
                            <?php endforeach; ?>

                    </div>
                </div>
                <?php endif ?>
            </div>
            <!-- download-->
            <?php if ($collection->collectionFiles): ?>
            <div class="additional-materials-download additional-materials__item">
                <div class="swiper-container swiper-container-download">
                    <div class="swiper-wrapper">

                            <?php foreach ($collection->collectionFiles as $document): ?>
                                <div class="swiper-slide">
                                    <div class="additional-materials-download__item">
                                        <!-- title-->
                                        <div class="materials-download-info row">
                                            <div class="materials-download-info__icon">
                                                <svg class="ico ico-download">
                                                    <use xlink:href="/typewriter/img/sprite.svg#ico-download"></use>
                                                </svg>
                                            </div>
                                            <div class="materials-download-info__title">
                                                <p><?= $document->name ?></p>
                                                <!-- btn-->
                                                <div class="materials-download-btn"><a class="hover-link"
                                                                                       href="/uploads<?= $document->file ?>"
                                                                                       download=>
                                                        <span class="arrow-txt"><?=Yii::t('type','Скачать')?></span>
                                                        <span class="arrow-icon">
                                                            <svg class="ico ico-arrow-angle">
                                                                <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                                                            </svg>
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>

                    </div>
                </div>
                <div class="download-btns row">
                    <div class="download-button-prev nav-border download-btn nav-btn-next">
                        <svg class="ico ico-arrow-angle">
                            <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                        </svg>
                    </div>
                    <div class="download-button-next nav-border download-btn nav-btn-prev">
                        <svg class="ico ico-arrow-angle">
                            <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                        </svg>
                    </div>
                </div>
            </div>
            <?php endif ?>
        </div>
    </div>
</section>
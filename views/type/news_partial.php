<div class="company-news-content-col row">
    <?php use yii\helpers\Url;

    foreach($news as $newsKey=> $newsItem):?>
    <?php if($newsKey >= 3) { break;}?>
    <?php $class = 'full-item'; if($newsKey > 0) { $class = 'half-item';}?>
    <div class="company-news-content__item row <?=$class?>">
        <!-- img -->
        <div class="company-news-img"><img src="/uploads<?=$newsItem->image?>" alt=""/></div>
        <!-- txt-->
        <div class="company-news-txt">
            <h3><?=$newsItem->title?></h3>
            <?php if($newsKey == 0):?>
            <p><?=mb_substr(strip_tags(htmlspecialchars_decode($newsItem->text)), 0, 287)?></p>
            <?php endif;?>
            <?php if($newsKey == 1 || $newsKey == 2):?>
                <p><?=mb_substr(strip_tags(htmlspecialchars_decode($newsItem->text)), 0, 287)?></p>
            <?php endif;?>
            <!-- info-->
            <div class="company-news-info row">
                <div class="news-link">
                    <a class="hover-link" href="<?=Url::to(['/type/one-news', 'slug' => $newsItem->slug])?>">
                        <span class="arrow-txt"><?=Yii::t('type','Подробнее')?></span><span class="arrow-icon">
                            <svg class="ico ico-arrow-angle">
                              <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                            </svg>
                        </span>
                    </a>
                </div>
                <div class="news-date">
                    <date><?=$newsItem->posted_date?></date>
                </div>
            </div>
        </div>
    </div>
    <?php endforeach;?>

</div>
<div class="company-news-content-col news-negative-margin row">
    <?php foreach($news as $newsKey=> $newsItem):?>
        <?php if($newsKey <= 2) { continue;}?>
        <?php $class = 'one-third-item'; if($newsKey == 6) { $class = 'full-item';}?>
    <div class="company-news-content__item row <?=$class?>">
        <!-- img -->
        <?php if($newsKey == 6):?>
        <div class="company-news-img"><img src="/uploads<?=$newsItem->image?>" alt=""/></div>
        <?php endif;?>
        <?php if($newsKey != 6):?>
            <div class="company-news-img"><img src="" alt=""/></div>
        <?php endif;?>
        <!-- txt-->
        <div class="company-news-txt">
            <h3><?=$newsItem->title?></h3>
            <?php if($newsKey >=3):?>
                <p><?=mb_substr(strip_tags(htmlspecialchars_decode($newsItem->text)), 0, 130)?></p>
            <?php endif;?>
            <!-- info-->
            <div class="company-news-info row">
                <div class="news-link">
                    <a class="hover-link" href="<?=Url::to(['/type/one-news', 'slug' => $newsItem->slug])?>"><span class="arrow-txt"><?=Yii::t('type','Подробнее')?></span><span class="arrow-icon">
                            <svg class="ico ico-arrow-angle">
                              <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                            </svg></span></a></div>
                <div class="news-date">
                    <date><?=$newsItem->posted_date?></date>
                </div>
            </div>
        </div>
    </div>
    <?php endforeach;?>
</div>
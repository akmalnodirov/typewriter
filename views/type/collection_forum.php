<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;

$settings = $this->params['settings'];

?>

    <section class="breadcrumbs">
        <div class="container">
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?=Url::to(['type/index'])?>"><?= Yii::t('type', 'Главная') ?></a></li>
                    <li class="breadcrumb-item active"><?= Yii::t('type', 'Форум') ?></li>
                </ol>
            </nav>
        </div>
    </section>
    <!-- forum-->
    <section class="forum">
        <div class="container">
            <!-- title-->
            <div class="section-title">
                <h1><?= Yii::t('type', 'Форум') ?></h1>
            </div>
            <!-- forum-title-type-->
            <?php if ($forumTheme): ?>
                <div class="forum-title row">
                    <div class="forum-title__type">
                        <p><?= Yii::t('type', 'Темы раздела') ?> : <b><?= $forumTheme->theme_name ?></b></p>
                    </div>
                    <div class="forum-title__time">
                        <p>
                            <span><?= Yii::t('type', 'Опубликовано') ?> :</span><?= Yii::$app->formatter->asDate($forumTheme->created_at, 'dd.MM.yyyy') ?>
                        </p>
                    </div>

                </div>
            <?php endif; ?>

            <?php if (!$forumTheme): ?>
                <div class="forum-title row">
                    <div class="forum-title__type">
                        <p><?= Yii::t('type', 'Для этого коллекции не еще не создано темы') ?></b></p>
                    </div>
                    <div class="forum-title__time">
                    </div>

                </div>
            <?php endif; ?>


            <?php if ($forumTheme): ?>
                <!-- forum-list-->
                <ul class="forum-list" id="all_comment_li">

                    <?php if (count($forumComments) <= 0) {
                        echo 'Нет комментариев';
                    } ?>

                    <?php foreach ($forumComments as $commentKey => $commentValue): ?>


                        <li class="row forum-list__item">
                            <div class="forum-list-img"><img src="/uploads<?= $settings->forum_avatar ?>" alt=""></div>
                            <div class="forum-list-txt">
                                <div class="forum-list-txt__title row">
                                    <div class="forum-list-name">
                                        <h3><?= $commentValue->user ? $commentValue->user->username : '' ?></h3>
                                    </div>
                                    <div class="forum-list-info">
                                        <?php if (!Yii::$app->user->isGuest): ?>
                                            <a href="#" class="reply_button" clicked="0">Цитировать</a>
                                        <?php endif; ?>
                                        <?php if (Yii::$app->user->isGuest): ?>
                                            <a href="#" class="reply_button" clicked="0">Для комментирования войдите в
                                                систему</a>
                                        <?php endif; ?>
                                        <time><?= Yii::$app->formatter->asDate($commentValue->created_at, 'dd.mm.yyyy') ?></time>
                                    </div>
                                </div>
                                <div class="forum-list-txt__content">
                                    <?php if ($commentValue->child): ?>

                                        <?php foreach ($commentValue->child as $childKey => $childValue): ?>
                                            <blockquote><?= $childValue->body ?></blockquote>
                                        <?php endforeach ?>

                                    <?php endif; ?>
                                    <p><?= $commentValue->body ?></p>
                                    <div class="js-mobile-info"></div>
                                </div>
                            </div>
                        </li>

                        <?php if (!Yii::$app->user->isGuest): ?>
                            <div style="margin-top: 10px; display: none;" class="reply_comment_leaving_textarea">
                                <?php $form = ActiveForm::begin(['options' => ['id' => 'rent_comments_form']]);
                                ?>
                                <div class="row">

                                    <?php echo $form->field($new_comment, 'body', [
                                        'template' => '{input}{error}',
                                        'options' => [
                                            'tag' => false,
                                        ]])->textarea([
                                        'maxlength' => true,
                                        'cols' => 30,
                                        'rows' => 13,
                                        'class' => 'comment_textarea',
                                        'placeholder' => 'Leave a reply'
                                    ])->label(false) ?>

                                    <?php $user_id = Yii::$app->user->identity->id ?>

                                    <?php echo $form->field($new_comment, 'forum_theme_id')->hiddenInput(['maxlength' => true, 'value' => $forumTheme->id,])->label(false) ?>

                                    <?php echo $form->field($new_comment, 'parent_id')->hiddenInput(['maxlength' => true, 'value' => $commentValue->id,])->label(false) ?>

                                    <?php echo $form->field($new_comment, 'user_id')->hiddenInput(['maxlength' => true, 'value' => $user_id,])->label(false) ?>

                                </div>
                                <div class="row comment_button_coverd" style="margin-top: 20px; margin-bottom: 20px">
                                    <button type="submit" class="comment_button">Leave a reply</button>
                                </div>
                                <?php ActiveForm::end(); ?>

                            </div>
                        <?php endif; ?>

                    <?php endforeach; ?>
                </ul>
                <?php if (!Yii::$app->user->isGuest): ?>
                    <div style="margin-top: 10px;" id="main_comment_leaving_textarea">
                        <?php $form = ActiveForm::begin(['options' => ['id' => 'rent_comments_form']]);
                        ?>
                        <div class="row">

                            <?php echo $form->field($new_comment, 'body', [
                                'template' => '{input}{error}',
                                'options' => [
                                    'tag' => false,
                                ]])->textarea([
                                'maxlength' => true,
                                'cols' => 30,
                                'rows' => 13,
                                'class' => 'comment_textarea',
                                'placeholder' => 'Leave a comment'
                            ])->label(false) ?>

                            <?php $user_id = Yii::$app->user->identity->id ?>

                            <?php echo $form->field($new_comment, 'forum_theme_id')->hiddenInput(['maxlength' => true, 'value' => $forumTheme->id,])->label(false) ?>

                            <?php echo $form->field($new_comment, 'parent_id')->hiddenInput(['maxlength' => true, 'value' => null,])->label(false) ?>

                            <?php echo $form->field($new_comment, 'user_id')->hiddenInput(['maxlength' => true, 'value' => $user_id,])->label(false) ?>

                        </div>
                        <div class="row comment_button_coverd" style="margin-top: 20px;">
                            <button type="submit" class="comment_button">Leave a comment</button>
                        </div>
                        <?php ActiveForm::end(); ?>

                    </div>
                <?php endif; ?>

                <?php if (Yii::$app->user->isGuest): ?>
                    <div>
                        <?= Yii::t('type', 'Пожалуйста, войдите или зарегистрируйтесь, чтобы оставить комментарий') ?>
                    </div>
                <?php endif; ?>

                <div>
                    <?php echo \yii\widgets\LinkPager::widget([
                        'pagination' => $provider->pagination,
                    ]); ?>
                </div>

            <?php endif; ?>
        </div>
    </section>

    <style>
        .comment_textarea {
            width: 70%;
            background-color: transparent;
            color: antiquewhite;
            font-size: 14px;
            height: 150px
        }

        .pagination {
            display: flex;
            padding-left: 0;
            list-style: none;
            border-radius: .25rem;
        }
    </style>

<?php $this->registerJs("

    $(function(){
    
           $('body').on('click', '.reply_button', function(){
           
           console.log('helloo');
           
                if(parseInt($(this).attr('clicked')) == 0){
                    $(this).attr('clicked', parseInt(1));
                    $(this).parent().parent().parent().parent().next('.reply_comment_leaving_textarea:first').css('display', 'block');
                } else {
                    $(this).attr('clicked', parseInt(0));
                    $(this).parent().parent().parent().parent().next('.reply_comment_leaving_textarea:first').css('display', 'none');
                }
           })
           
           $('#comment_again_button').click(function(){
                
                let theme_id = $(this).attr('theme_id');
                let offset = $(this).attr('offset');
                let element = $(this);
                 
                $.ajax({
                    url: '/type/forum-partial',
                    data: {theme_id: theme_id, offset: offset},
                    dataType: 'json',
                    success: function (response) {
                        $('#all_comment_li').append(response.content);
                        $('#comment_again_button').attr('offset', response.offset)
                    }
                })
               
           })
    
    })


") ?>
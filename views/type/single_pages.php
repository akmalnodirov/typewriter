<?php

use yii\helpers\Url;

?>
<section class="breadcrumbs">
    <div class="container">
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?=Url::to(['/type/index'])?>"><?=Yii::t('type','Главная')?></a></li>
                <li class="breadcrumb-item"><a href="<?=Yii::$app->request->referrer?>"><?= $page->pageType ? $page->pageType->name : 'Стандартная страница'?></a></li>
                <li class="breadcrumb-item active"><?=$page->title?></li>
            </ol>
        </nav>
    </div>
</section>
<article class="news-article">
    <div class="container">
        <div class="news-article-img"> <img src="/uploads<?=$page->image?>" alt=""></div>
        <div class="section-title">
            <h1><?=$page->title?></h1>
        </div>
        <div class="news-article-wrapper">
            <h2><?=$page->title?></h2>
            <?=htmlspecialchars_decode($page->text)?>
            <div class="article-date row">
                <p><?=Yii::t('type','Добавлено')?> : <?=Yii::$app->formatter->asDate($page->created_at, 'dd.mm.yyyy')?></p>
            </div>
        </div>
    </div>
</article>
<?php

use yii\helpers\Url;

?>
<section class="breadcrumbs">
    <div class="container">
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?=Url::to(['type/index'])?>">Главная</a></li>
                <li class="breadcrumb-item active"><?=$pageType->name?></li>
            </ol>
        </nav>
    </div>
</section>
<!-- all-news-->
<main class="all-news-section">
    <div class="container">
        <!-- title-->
        <div class="section-title">
            <h1><?=$pageType->name?></h1>
        </div>
        <!-- all-news-->
        <div class="company-news-content row">
            <div class="company-news-content-col row">
                <?php

                foreach($pages as $pagesKey=> $pagesItem):?>
                    <?php if($pagesKey >= 3) { break;}?>
                    <?php $class = 'full-item'; if($pagesKey > 0) { $class = 'half-item';}?>
                    <div class="company-news-content__item row <?=$class?>">
                        <!-- img -->
                        <div class="company-news-img"><img src="/uploads<?=$pagesItem->image?>" alt=""/></div>
                        <!-- txt-->
                        <div class="company-news-txt">
                            <h3><?=$pagesItem->title?></h3>
                            <?php if($pagesKey == 0):?>
                                <p><?=mb_substr(strip_tags(htmlspecialchars_decode($pagesItem->text)), 0, 287)?></p>
                            <?php endif;?>
                            <?php if($pagesKey == 1 || $pagesKey == 2):?>
                                <p><?=mb_substr(strip_tags(htmlspecialchars_decode($pagesItem->text)), 0, 287)?></p>
                            <?php endif;?>
                            <!-- info-->
                            <div class="company-news-info row">
                                <div class="news-link">
                                    <a class="hover-link" href="<?=Url::to(['/type/single-page', 'slug' => $pagesItem->slug])?>">
                                        <span class="arrow-txt"><?=Yii::t('type','Подробнее')?></span><span class="arrow-icon">
                            <svg class="ico ico-arrow-angle">
                              <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                            </svg>
                        </span>
                                    </a>
                                </div>
                                <div class="news-date">
                                    <date><?=Yii::$app->formatter->asDate($pagesItem->created_at)?></date>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>

            </div>
            <div class="company-news-content-col news-negative-margin row">
                <?php foreach($pages as $pagesKey=> $pagesItem):?>
                    <?php if($pagesKey <= 2) { continue;}?>
                    <?php $class = 'one-third-item'; if($pagesKey == 6) { $class = 'full-item';}?>
                    <div class="company-news-content__item row <?=$class?>">
                        <!-- img -->
                        <?php if($pagesKey == 6):?>
                            <div class="company-news-img"><img src="/uploads<?=$pagesItem->image?>" alt=""/></div>
                        <?php endif;?>
                        <?php if($pagesKey != 6):?>
                            <div class="company-news-img"><img src="" alt=""/></div>
                        <?php endif;?>
                        <!-- txt-->
                        <div class="company-news-txt">
                            <h3><?=$pagesItem->title?></h3>
                            <?php if($pagesKey >=3):?>
                                <p><?=mb_substr(strip_tags(htmlspecialchars_decode($pagesItem->text)), 0, 130)?></p>
                            <?php endif;?>
                            <!-- info-->
                            <div class="company-news-info row">
                                <div class="news-link">
                                    <a class="hover-link" href="<?=Url::to(['/type/one-news', 'slug' => $pagesItem->slug])?>"><span class="arrow-txt"><?=Yii::t('type','Подробнее')?></span><span class="arrow-icon">
                            <svg class="ico ico-arrow-angle">
                              <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                            </svg></span></a></div>
                                <div class="news-date">
                                    <date><?=Yii::$app->formatter->asDate($pagesItem->created_at, 'dd.mm.yyyy')?></date>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        </div>

    </div>
</main>
<?php

use yii\widgets\ActiveForm;

?>

<?php
$avatars = [
    '/typewriter/avatars/boy.png',
    '/typewriter/avatars/boy1.png',
    '/typewriter/avatars/boy2.png',
    '/typewriter/avatars/girl.png',
    '/typewriter/avatars/girl (1).png',
    '/typewriter/avatars/man.png',
    '/typewriter/avatars/man (1).png',
    '/typewriter/avatars/man (2).png',
    '/typewriter/avatars/man (3).png',
]
?>

<?php if ($forumComments): ?>
    <?php foreach ($forumComments as $commentKey => $commentValue): ?>

        <?php $avatar = array_rand($avatars); ?>

        <li class="row forum-list__item">
            <div class="forum-list-img"><img src="<?= $avatars[$avatar] ?>" alt=""></div>
            <div class="forum-list-txt">
                <div class="forum-list-txt__title row">
                    <div class="forum-list-name">
                        <h3><?= $commentValue->user ? $commentValue->user->username : '' ?></h3>
                    </div>
                    <div class="forum-list-info">
                        <?php if (!Yii::$app->user->isGuest): ?>
                            <a href="#" class="reply_button" clicked="0"><?=Yii::t('type','Цитировать')?></a>
                        <?php endif; ?>
                        <?php if (Yii::$app->user->isGuest): ?>
                            <a href="#" class="reply_button" clicked="0"><?=Yii::t('type','Для комментирования войдите в систему')?></a>
                        <?php endif; ?>
                        <time><?= Yii::$app->formatter->asDate($childValue->created_at, 'dd.mm.yyyy') ?></time>
                    </div>
                </div>
                <div class="forum-list-txt__content">
                    <?php if ($commentValue->child): ?>

                        <?php foreach ($commentValue->child as $childKey => $childValue): ?>
                            <blockquote><?=$childValue->body?></blockquote>
                        <?php endforeach ?>

                    <?php endif; ?>
                    <p><?= $commentValue->body ?></p>
                    <div class="js-mobile-info"></div>
                </div>
            </div>
        </li>

        <?php if (!Yii::$app->user->isGuest): ?>
            <div style="margin-top: 10px; display: none;" class="reply_comment_leaving_textarea">
                <?php $form = ActiveForm::begin(['action' => ['type/collection-forum'],'options' => ['method' => 'post']]);?>
                ?>
                <div class="row">

                    <?php echo $form->field($new_comment, 'body', [
                        'template' => '{input}{error}',
                        'options' => [
                            'tag' => false,
                        ]])->textarea([
                        'maxlength' => true,
                        'cols' => 30,
                        'rows' => 13,
                        'class' => 'comment_textarea',
                        'placeholder' => 'Leave a reply'
                    ])->label(false) ?>

                    <?php $user_id = Yii::$app->user->identity->id ?>

                    <?php echo $form->field($new_comment, 'forum_theme_id')->hiddenInput(['maxlength' => true, 'value' => $forumTheme->id,])->label(false) ?>

                    <?php echo $form->field($new_comment, 'parent_id')->hiddenInput(['maxlength' => true, 'value' => $commentValue->id,])->label(false) ?>

                    <?php echo $form->field($new_comment, 'user_id')->hiddenInput(['maxlength' => true, 'value' => $user_id,])->label(false) ?>

                </div>
                <div class="row comment_button_coverd" style="margin-top: 20px; margin-bottom: 20px">
                    <button type="submit" class="comment_button"><?=Yii::t('type','Leave a reply')?></button>
                </div>
                <?php ActiveForm::end(); ?>

            </div>
        <?php endif; ?>

    <?php endforeach; ?>
<?php endif; ?>

<?php if (!$forumComments): ?>

    <h2><?=Yii::t('type','Болше комментариев нет')?></h2>

<?php endif; ?>



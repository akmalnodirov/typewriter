<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;

$settings = $this->params['settings'];

?>

    <section class="breadcrumbs">
        <div class="container">
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?=Url::to(['type/index'])?>"><?= Yii::t('type', 'Главная') ?></a></li>
                    <li class="breadcrumb-item active"><?= Yii::t('type', 'Результаты поиска') ?></li>
                </ol>
            </nav>
        </div>
    </section>
    <!-- forum-->
    <section class="forum">
        <div class="container">
            <!-- forum-title-type-->

            <?php if ($collections): ?>
                <!-- forum-list-->
                <ul class="forum-list" id="all_comment_li">
                        <li class="row forum-list__item">
                            <div class="forum-list-txt">
                                <div class="forum-list-name"><h3>Коллекции</h3></div>
                                <div class="forum-list-txt__content">
                                    <p>
                                        <?php foreach ($collections as $collection): ?>
                                            <a href="<?=Url::to(['type/one-collection', 'slug' => $collection->slug])?>">
                                                <?= $collection->title ?>,
                                                <?=Yii::t('type',' Теги: ')?>
                                                <?= $collection->hashtag ?> ,
                                            </a>
                                        <?php endforeach ?>
                                    </p>
                                    <div class="js-mobile-info"></div>
                                </div>
                            </div>
                        </li>
                </ul>
            <?php endif; ?>

            <?php if ($collectionEvents): ?>
                <!-- forum-list-->
                <ul class="forum-list" id="all_comment_li">
                    <li class="row forum-list__item">
                        <div class="forum-list-txt">
                            <div class="forum-list-name"><h3>Событии коллекции</h3></div>
                            <div class="forum-list-txt__content">
                                <p>
                                    <?php foreach ($collectionEvents as $collectionEvent): ?>
                                        <a href="">
                                            <?= $collectionEvent->event_date ?>,
                                            <?=Yii::t('type',' Теги: ')?>
                                            <?= $collectionEvent->hashtag ?> ,
                                        </span>
                                    <?php endforeach ?>
                                </p>
                                <div class="js-mobile-info"></div>
                            </div>
                        </div>
                    </li>
                </ul>
            <?php endif; ?>

            <?php if ($collectionModelCharacteristics): ?>
                <!-- forum-list-->
                <ul class="forum-list" id="all_comment_li">
                    <li class="row forum-list__item">
                        <div class="forum-list-txt">
                            <div class="forum-list-name"><h3>Событии коллекции</h3></div>
                            <div class="forum-list-txt__content">
                                <p>
                                    <?php foreach ($collectionModelCharacteristics as $characteristic): ?>
                                        <a href="">
                                            <?= $characteristic->value ?>,
                                            <?=Yii::t('type',' Теги: ')?>
                                            <?= $characteristic->hashtag ?> ,
                                        </a>
                                    <?php endforeach ?>
                                </p>
                                <div class="js-mobile-info"></div>
                            </div>
                        </div>
                    </li>
                </ul>
            <?php endif; ?>

            <?php if ($colectionModels): ?>
                <!-- forum-list-->
                <ul class="forum-list" id="all_comment_li">
                    <li class="row forum-list__item">
                        <div class="forum-list-txt">
                            <div class="forum-list-name"><h3>Модели коллекции</h3></div>
                            <div class="forum-list-txt__content">
                                <p>
                                    <?php foreach ($colectionModels as $collectionModel): ?>
                                        <a href="">
                                            <?= $collectionModel->name ?>,
                                            <?=Yii::t('type',' Теги: ')?>
                                            <?= $collectionModel->hashtag ?> ,
                                        </a>
                                    <?php endforeach ?>
                                </p>
                                <div class="js-mobile-info"></div>
                            </div>
                        </div>
                    </li>
                </ul>
            <?php endif; ?>

            <?php if ($forumThemes): ?>
                <!-- forum-list-->
                <ul class="forum-list" id="all_comment_li">
                    <li class="row forum-list__item">
                        <div class="forum-list-txt">
                            <div class="forum-list-name"><h3>Темы форума</h3></div>
                            <div class="forum-list-txt__content">
                                <p>
                                    <?php foreach ($forumThemes as $forumTheme): ?>
                                        <a href="">
                                            <?= $forumTheme->theme_name ?>,
                                            <?=Yii::t('type',' Теги: ')?>
                                            <?= $forumTheme->hashtag ?> ,
                                        </a>
                                    <?php endforeach ?>
                                </p>
                                <div class="js-mobile-info"></div>
                            </div>
                        </div>
                    </li>
                </ul>
            <?php endif; ?>

            <?php if ($news): ?>
                <!-- forum-list-->
                <ul class="forum-list" id="all_comment_li">
                    <li class="row forum-list__item">
                        <div class="forum-list-txt">
                            <div class="forum-list-name"><h3>Новости</h3></div>
                            <div class="forum-list-txt__content">
                                <p>
                                    <?php foreach ($news as $item): ?>
                                        <a href="">
                                            <?= $item->title ?>,
                                            <?=Yii::t('type',' Теги: ')?>
                                            <?= $item->hashtag ?> ,
                                        </a>
                                    <?php endforeach ?>
                                </p>
                                <div class="js-mobile-info"></div>
                            </div>
                        </div>
                    </li>
                </ul>
            <?php endif; ?>

            <?php if ($standardPages): ?>
                <!-- forum-list-->
                <ul class="forum-list" id="all_comment_li">
                    <li class="row forum-list__item">
                        <div class="forum-list-txt">
                            <div class="forum-list-name"><h3>Динамисексие страницы</h3></div>
                            <div class="forum-list-txt__content">
                                <p>
                                    <?php foreach ($standardPages as $standardPage): ?>
                                        <a href="">
                                            <?= $standardPage->title ?>,
                                            <?=Yii::t('type',' Теги: ')?>
                                            <?= $standardPage->hashtag ?> ,
                                        </a>
                                    <?php endforeach ?>
                                </p>
                                <div class="js-mobile-info"></div>
                            </div>
                        </div>
                    </li>
                </ul>
            <?php endif; ?>

        </div>
    </section>
<style>
    .forum-list-txt__content .a:hover {
        color: white;
    }
</style>
<?php

use yii\helpers\Url;

?>
<section class="breadcrumbs">
    <div class="container">
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?=Url::to(['/type/index'])?>"><?=Yii::t('type','Главная')?></a></li>
                <li class="breadcrumb-item"><a href="<?=Url::to(['/type/news'])?>"><?=Yii::t('type','Новости')?></a></li>
                <li class="breadcrumb-item active"><?=$model->title?></li>
            </ol>
        </nav>
    </div>
</section>
<article class="news-article">
    <div class="container">
        <div class="news-article-img"> <img src="/uploads<?=$model->image?>" alt=""></div>
        <div class="section-title">
            <h1><?=Yii::t('type','Страница новости')?></h1>
        </div>
        <div class="news-article-wrapper">
            <h2><?=$model->title?></h2>
           <?=htmlspecialchars_decode($model->text)?>
            <div class="article-date row">
                <p><?=Yii::t('type','Добавлено')?> : <?=$model->posted_date?></p>
            </div>
        </div>
    </div>
</article>
<section class="news-more">
    <div class="container">
        <div class="section-title">
            <h2><?=Yii::t('type','Читайте также')?></h2>
        </div>
        <div class="swiper-container news-more-container">
            <div class="swiper-wrapper">
                <?php foreach($news as $newsKey=>$newsItem):?>
                <div class="swiper-slide">
                    <div class="company-news-content__item row news-slide-more">
                        <!-- img -->
                        <div class="company-news-img"><img src="/uploads<?=$newsItem->image?>" alt=""/></div>
                        <!-- txt-->
                        <div class="company-news-txt">
                            <h3><?=$newsItem->title?></h3>
                            <!-- info-->
                            <div class="company-news-info row">
                                <div class="news-link"><a class="hover-link" href="<?=Url::to(['/type/one-news', 'slug' => $newsItem->slug])?>"><span class="arrow-txt"><?=Yii::t('type','Подробнее')?></span><span class="arrow-icon">
                              <svg class="ico ico-arrow-angle">
                                <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                              </svg></span></a></div>
                                <div class="news-date">
                                    <date><?=$newsItem->posted_date?></date>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach;?>
            </div>
        </div>
        <div class="swiper-button-prev news-button-prev nav-btn-next nav-border">
            <svg class="ico ico-arrow-angle">
                <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
            </svg>
        </div>
        <div class="swiper-button-next news-button-next nav-btn-prev nav-border">
            <svg class="ico ico-arrow-angle">
                <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
            </svg>
        </div>
    </div>
</section>
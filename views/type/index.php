<?php

use yii\helpers\Url;
$settings = $this->params['settings'];

?>

<div id="scroll">
    <div class="scroll-navigation"></div>
    <!-- homepage-main-slider-->
    <section class="homepage-main-slider full-page-slide decor-lines" id="section-js-slide-1" style="background-image: url(/uploads<?=\app\components\Helpers::convertPathToUrl($settings->brands_index_bg)?>);">
        <div class="container">
            <div class="swiper-container homepage-swiper">
                <div class="swiper-wrapper">

                    <?php foreach($brandSlider as $sliderKey=>$sliderItem):?>
                    <div class="swiper-slide swiper-no-swiping">
                        <!-- title-->
                        <div class="section-title"><span><?=$sliderItem->first_title?></span>
                            <!-- .js-typeit-->
                            <h2><?=$sliderItem->second_title?></h2>
                        </div>
                        <!-- message-->
                        <div class="slide-message">
                            <div class="txt-wrapper">
                                <p><?=$sliderItem->description?></p>
                            </div>
                            <?php if($sliderItem->link):?>
                                <button class="btn index_brands_button" url="<?=$sliderItem->link?>">
                                    <span class="arrow-txt"><?=Yii::t('type','Подробнее')?></span>
                                    <span class="arrow-icon">
                                        <svg class="ico ico-arrow-angle">
                                            <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                                        </svg>
                                    </span>
                                </button>
                            <?php endif;?>

                        </div>
                    </div>
                    <?php endforeach;?>

                </div>
            </div>
            <div class="swiper-pagination homepage-pagination"></div>
            <div class="swiper-button-prev homepage-button-next nav-btn-prev nav-border">
                <svg class="ico ico-arrow-angle">
                    <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                </svg>
            </div>
            <div class="swiper-button-next homepage-button-prev nav-btn-next nav-border">
                <svg class="ico ico-arrow-angle">
                    <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                </svg>
            </div>
        </div>
    </section>
    <!-- new-product-->
    <section class="new-product full-page-slide decor-lines" id="section-js-slide-2" style="background-image: url(/uploads<?=\app\components\Helpers::convertPathToUrl($settings->catalog_museum_index_bg)?>);">
        <div class="container">
            <div class="section-title"> <span><?=Yii::t('type','каталог-музей')?></span>
                <h2><?=Yii::t('type','Новинки')?></h2>
            </div>
            <div class="swiper-container new-product-swiper">
                <div class="swiper-wrapper">
                    <?php foreach($collectionModels as $collectionModelKey=>$collectionModelItem):?>
                    <div class="swiper-slide">
                        <div class="new-product-item row">
                            <div class="new-product-img"> <img src="/uploads<?=$collectionModelItem->image?>" alt=""/></div>
                            <div class="new-product-txt">
                                <h3><?=$collectionModelItem->name?></h3>
                                <a class="hover-link" href="<?=Url::to(['/type/one-model', 'slug' => $collectionModelItem->slug])?>">
                                    <span class="arrow-txt"><?=Yii::t('type','Подробнее')?></span>
                                    <span class="arrow-icon">
                                    <svg class="ico ico-arrow-angle">
                                      <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                                    </svg></span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
            <div class="swiper-pagination new-swiper-pagination"></div>
            <div class="new-button-prev nav-border nav-btn-next">
                <svg class="ico ico-arrow-angle">
                    <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                </svg>
            </div>
            <div class="new-button-next nav-border nav-btn-prev">
                <svg class="ico ico-arrow-angle">
                    <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                </svg>
            </div>
        </div>
    </section>
    <!-- company-news-->
    <section class="company-news full-page-slide decor-lines" id="section-js-slide-3" style="background-image: url(/uploads<?=\app\components\Helpers::convertPathToUrl($settings->news_index_bg)?>);">
        <div class="container">
            <div class="section-title"> <span><?=Yii::t('type','каталог-музей')?></span>
                <h2><?=Yii::t('type','Новости')?></h2>
            </div>
            <div class="swiper-container company-news-swiper-mobile">
                <div class="swiper-wrapper"> </div>
                <div class="swiper-pagination"></div>
                <div class="nav-btn-next nav-border news-btn-next">
                    <svg class="ico ico-arrow-angle">
                        <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                    </svg>
                </div>
                <div class="nav-btn-prev nav-border news-btn-prev">
                    <svg class="ico ico-arrow-angle">
                        <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                    </svg>
                </div>
            </div>
            <div class="company-news-content">
                <?php $class = 'default-news';?>
                <?php foreach($news as $newsKey=>$newsItem):?>
                <?php if($newsKey == 2) {$class = 'full-news';}?>
                <?php if($newsKey > 2) {$class = 'min-news';}?>
                <div class="company-news-content__item row <?=$class?>">
                    <!-- img -->
                    <?php if($newsKey <=2):?>
                    <div class="company-news-img"><img src="/uploads<?=$newsItem->image?>" alt=""/></div>
                    <?php endif;?>

                    <?php if($newsKey > 2):?>
                        <div class="company-news-img"><img src="" alt=""/></div>
                    <?php endif?>
                    <!-- txt-->
                    <div class="company-news-txt">
                        <h3><?=$newsItem->title?></h3>
                        <?php if($newsKey === 2):?>
                        <p><?=mb_substr(strip_tags(htmlspecialchars_decode($newsItem->text)), 0, 287)?></p>
                        <?php endif;?>
                        <?php if($newsKey > 2):?>
                            <p><?=mb_substr(strip_tags(htmlspecialchars_decode($newsItem->text)), 0, 130)?></p>
                        <?php endif;?>
                        <!-- info-->
                        <div class="company-news-info row">
                            <div class="news-link">
                                <a class="hover-link" href="<?=Url::to(['/type/one-news', 'slug' => $newsItem->slug])?>">
                                    <span class="arrow-txt"><?=Yii::t('type','Подробнее')?></span><span class="arrow-icon">
                                        <svg class="ico ico-arrow-angle">
                                          <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                                        </svg>
                                    </span>
                                </a>
                            </div>
                            <div class="news-date">
                                <date><?=$newsItem->posted_date?></date>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach?>
            </div>
            <div class="company-news-read-more row"><a class="btn-transparent hover-link" href="<?=Url::to(['type/news'])?>"><span class="arrow-txt"><?=Yii::t('type','Все новости')?></span><span class="arrow-icon">
                    <svg class="ico ico-arrow-angle">
                      <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                    </svg></span></a></div>
        </div>
    </section>
    <!-- partners-->
    <section class="partners full-page-slide decor-lines" id="section-js-slide-4" style="background-image: url(/uploads<?=\app\components\Helpers::convertPathToUrl($settings->partners_index_bg)?>);">
        <div class="container">
            <div class="section-title"> <span><?=Yii::t('type','каталог-музей')?></span>
                <h2><?=Yii::t('type','Партнеры')?></h2>
            </div>
            <div class="partners-wrapper row">
                <div class="swiper-container partners-mobile-swiper">
                    <div class="swiper-wrapper"> </div>
                </div>
                <div class="partners-content row">
                    <?php foreach($partners as $partner):?>
                    <div class="partners-logo row"><img src="/uploads<?=$partner->image?>" alt=""></div>
                    <?php endforeach?>
                </div>
                <!-- message-->
                <div class="slide-message">
                    <div class="txt-wrapper">
                        <p><?=$this->params['settings']->partners_text?></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php $this->registerJs("

    $('body').on('click', '.index_brands_button', function(){
        window.location.href=$(this).attr('url');
    })

")?>
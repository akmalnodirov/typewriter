<?php

use yii\widgets\ActiveForm;

?>
<section class="breadcrumbs">
    <div class="container">
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Главная</a></li>
                <li class="breadcrumb-item active">Личный кабинет</li>
            </ol>
        </nav>
    </div>
</section>
<!-- section-title-->
<div class="section-title">
    <div class="container">
        <h1>Авторизация</h1>
    </div>
</div>
<!-- login-->
<section class="form-section">
    <div class="container">
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
            <div class="login-form row">
                <div class="ui-group login-field">
                    <?php echo $form->field($model, 'username', [
                        'template' => '{input}{error}',
                        'options' => [
                            'tag' => false,
                        ]])->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Введите ваш логин'
                    ])->label(false) ?>

                </div>
                <div class="ui-group login-field">
                    <?php echo $form->field($model, 'password', [
                        'template' => '{input}{error}',
                        'options' => [
                            'tag' => false,
                        ]])->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Введите пароль',
                        'type' => 'password'
                    ])->label(false) ?>

                </div>
                <div class="ui-group login-send">
                    <button class="btn"><span class="arrow-txt">Войти</span><span class="arrow-icon">
                      <svg class="ico ico-arrow-angle">
                        <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                      </svg></span></button>
                </div>
                <div class="login-form-info row">
                    <div class="ui-checkbox">
                        <label for="remember">Запомнить меня</label>
                        <input id="remember" type="checkbox" name="">
                    </div>
                    <div class="login-link row"><a href="#">Я уже зарегестрирован</a><a href="#">Забыли пароль?</a>
                    </div>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</section>
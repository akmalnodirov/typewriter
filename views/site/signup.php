<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>
<section class="breadcrumbs">
    <div class="container">
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Главная</a></li>
                <li class="breadcrumb-item active">Личный кабинет</li>
            </ol>
        </nav>
    </div>
</section>
<!-- section-title-->
<div class="section-title">
    <div class="container">
        <h1>Регистрация</h1>
    </div>
</div>
<!-- login-->
<section class="form-section">
    <div class="container">
        <?php
        $form = ActiveForm::begin(['id' => 'form-signup']); ?>
        <!-- registration-->
        <div class="registration-wrapper row">
            <!-- ui-->
            <div class="ui-group">
                <?php echo $form->field($model, 'username', [
                    'template' => '{input}{error}',
                   ])->textInput([
                    'maxlength' => true,
                    'placeholder' => 'Ваше имя'
                ])->label(false) ?>

            </div>


            <!-- ui-->
            <div class="ui-group">
                <?php echo $form->field($model, 'email', [
                    'template' => '{input}{error}',
                    ])->textInput([
                    'maxlength' => true,
                    'placeholder' => 'Ваш e-mail'
                ])->label(false) ?>
            </div>
            <!-- ui-->
            <div class="ui-group">
                <?php echo $form->field($model, 'password', [
                    'template' => '{input}{error}',
                   ])->passwordInput([
                    'maxlength' => true,
                    'placeholder' => 'Введите пароль',
                ])->label(false) ?>
            </div>
            <!-- ui-->
            <div class="ui-group submit-btn">
                <button class="btn" type="submit"><span class="arrow-txt">Зарегестрироваться</span><span
                            class="arrow-icon">
                      <svg class="ico ico-arrow-angle">
                        <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                      </svg></span></button>
            </div>
            <!-- ui-->
            <div class="ui-checkbox">
                <label for="remember">Согласие на обработку <a href="<?=Url::to(['type/personal-agreement'])?>">персональных данных</a></label>

                        <?= $form->field($model, 'agreement', [
                            'template' => '{input}{error}',
                            ])->checkbox([
                            'label' => false,
                            'id' => 'remember',
                            'hidden' => false,
                            //'class' => ''
                        ])->label(false)?>

            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</section>
<?php

use yii\helpers\Url;

$user = Yii::$app->user->identity;

?>
<section class="breadcrumbs">
    <div class="container">
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?=Url::to(['type/index'])?>">Главная</a></li>
                <li class="breadcrumb-item active"> Личный кабинет</li>
            </ol>
        </nav>
    </div>
</section>
<!-- lk-section-->
<section class="lk-section decor-lines">
    <div class="container">
        <!-- section-title-->
        <div class="section-title">
            <h1>Личный кабинет</h1>
        </div>
        <div class="lk row">
            <!-- img-->
            <div class="lk__img linear-gradient-right"><img src="/typewriter/img/lk/lk-img.png" alt=""></div>
            <!-- info-->
            <div class="lk__info">
                <!-- settings-->
                <button class="settings-btn">
                    <svg class="ico ico-settings">
                        <use xlink:href="/typewriter/img/sprite.svg#ico-settings"></use>
                    </svg>
                </button>
                <!-- line-->
                <div class="lk-info-line row">
                    <!-- i-->
                    <div class="lk-info-line__item row"><span>Имя:</span>
                        <p><?=$user->username?></p>
                    </div>
                    <!-- i-->
<!--                    <div class="lk-info-line__item row"><span>Номер телефона:</span><a href="tel:--><?//=$user->phone?><!--">--><?//=$user->phone?><!--</a></div>-->
                    <!-- i-->
                    <div class="lk-info-line__item row"><span>E-mail:</span><a href="mailto:<?=$user->email?>"><?=$user->email?></a></div>
                    <!-- i-->
                    <div class="lk-info-line__item row"><span>Адрес:</span>
                        <p><?=$user->address?></p>
                    </div>
                </div>
                <!-- line-->
                <div class="lk-info-line row">
                    <!-- i-->
                    <div class="lk-info-line__item row"><span>Дата регистрации:</span>
                        <p><?=Yii::$app->formatter->asDate($user->created_at, 'dd.mm.yyyy')?></p>
                    </div>
                    <!-- i-->
                    <div class="lk-info-line__item row"><span>Дата изменений:</span>
                        <p><?=Yii::$app->formatter->asDate($user->updated_at, 'dd.mm.yyyy')?></p>
                    </div>
                    <!-- i-->
                    <div class="lk-info-line__item row"><span>Комментарии на форуме:</span>
                        <p><?=$user->commentCount?></p>
                    </div>
                    <!-- i-->
                    <div class="lk-info-line__item row"><span>Статус:</span>
                        <p><?=$user->userStatus?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="lk-btn">
            <button class="btn-transparent hover-link js-popup" data-mfp-src="#modal-form-logout"><span class="arrow-txt">Выйти</span><span class="arrow-icon">
                  <svg class="ico ico-arrow-angle">
                    <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                  </svg></span></button>
        </div>
        <div class="modal-content mfp-hide white-popup-block my-mfp-slide-bottom modal-logout" id="modal-form-logout">
            <div class="popup-buble">
                <div class="popup-dialog row">
                    <div class="logout-wrapper row">
                        <h3>Вы уверены, что хотите выйти?</h3>
                        <div class="logout-btn row">
                            <a class="btn logout-btn__item" href="<?=Url::to(['site/logout'])?>">Да</a>
                            <a class="btn-transparent logout-btn__item " id="profile_modal_close_button">Нет</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php

$this->registerJs("

$('body').on('click', '#profile_modal_close_button', function(){

    console.log('closed');
    
    $('.mfp-close').trigger('click');

})

")

?>
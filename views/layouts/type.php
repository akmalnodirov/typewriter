<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use app\assets\TypeAsset;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

TypeAsset::register($this);
$comment = $this->params['comment'];
$settings = $this->params['settings'];
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#fff">
    <meta name="format-detection" content="telephone=no">
    <link rel="apple-touch-icon" sizes="180x180" href="/typewriter/files/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/typewriter/files/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/typewriter/files/favicon-16x16.png">
    <link rel="manifest" href="/typewriter/files/site.webmanifest">
    <link rel="mask-icon" href="/typewriter/files/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#ffffff">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<?php $bodyClass = [
    'login' => 'login-page',
    'signup' => 'registration-page',
    'index' => 'homepage',
    'profile' => 'lk-page',
    'collection-models' => 'model-catalog-page',
    'collection-forum' => 'forum-page',
    'one-model' => 'card-model-page',
    'news' => 'all-news-page',
    'multipage' => 'all-news-page',
    'one-news' => 'news-page',
    'single_page' => 'news-page',
    'political-confident' => 'news-page',
    'personal-agreement' => 'news-page',
] ?>

<body class="<?= $bodyClass[Yii::$app->controller->action->id] ?>">
<?php $this->beginBody() ?>
<?= \app\widgets\HeaderWidget::widget(); ?>
<div class="page">
    <div class="page__content">
        <?= $content ?>
    </div>
    <?= \app\widgets\MobileFooterWidget::widget(); ?>
    <?= \app\widgets\FooterWidget::widget(); ?>
</div>

<?php if (Yii::$app->session->hasFlash('success')): ?>
    <?php $this->registerJs("
        $(function(){
            $('#thanks_button').trigger('click');
        })
    ") ?>
    <?php Yii::$app->session->remove('success') ?>
<?php endif; ?>

<?php if (Yii::$app->session->hasFlash('register')): ?>
    <?php $this->registerJs("
        $(function(){
            $('#register_button').trigger('click');
        })
    ") ?>
    <?php Yii::$app->session->remove('register') ?>
<?php endif; ?>

<style>
    .model-catalog-page {
        background-image:  url(/uploads<?=\app\components\Helpers::convertPathToUrl($settings->catalog_model_bg)?>);
    }
    .all-news-page {
        background-image: url(/uploads<?=\app\components\Helpers::convertPathToUrl($settings->all_news_bg)?>);
    }
    .news-page {
        background-image: url(/uploads<?=\app\components\Helpers::convertPathToUrl($settings->news_item_bg)?>);
    }
    .forum-page {
        background-image: url(/uploads<?=\app\components\Helpers::convertPathToUrl($settings->forum_bg)?>);
    }
</style>

<div class="modal-content mfp-hide white-popup-block my-mfp-slide-bottom" id="modal-form-1">
    <div class="popup-buble">
        <div class="popup-dialog">
            <h2><?=Yii::t('layouts','Свяжитесь с нами')?></h2>
            <?php $form = ActiveForm::begin(['action' => ['type/comment-save'], 'options' => ['method' => 'post']]); ?>
            <div class="ui-group">
                <?php echo $form->field($comment, 'full_name')->textInput([
                    'placeholder' => 'Ваше имя',
                    'type' => 'text',
                ])->label(false) ?>
            </div>
            <div class="ui-group">
                <?php echo $form->field($comment, 'phone', [
                    'template' => '{input}{error}',
                    'options' => [
                        'tag' => false,
                    ]])->textInput([
                    'placeholder' => 'Ваш номер телефона',
                    'type' => 'tel',
                ])->label(false) ?>
            </div>
            <div class="ui-group">
                <?php echo $form->field($comment, 'email')->textInput([
                    'placeholder' => 'Ваш e-mail',
                    'type' => 'email',
                ])->label(false) ?>
            </div>
            <div class="ui-checkbox">
                <label for="checkbox"><?=Yii::t('layouts','Согласие на обработку')?> <a href=""><?=Yii::t('layouts','персональных данных')?></a></label>
                <input type="checkbox" id="checkbox">
            </div>
            <div class="ui-submit">
                <input class="btn" type="submit" value="<?=Yii::t('layouts','Отправить')?>">
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<div class="modal-content mfp-hide white-popup-block my-mfp-slide-bottom modal-form-tnx" id="modal-form-tnx">
    <div class="popup-buble">
        <div class="popup-dialog">
            <h2><?=Yii::t('layouts','Спасибо за заявку')?></h2>
            <p class="subtitle"><?=Yii::t('layouts','Наши менеджеры свяжутся с Вами, как только обработают Ваш запрос')?></p>
            <a class="btn" href="<?=Url::to(['/type/index'])?>">
                <span class="arrow-txt"><?=Yii::t('layouts','Вернутся на главную')?></span>
                <span class="arrow-icon">
                <svg class="ico ico-arrow-angle">
                  <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                </svg>
                </span>
            </a>
        </div>
    </div>
</div>

<div class="modal-content mfp-hide white-popup-block my-mfp-slide-bottom modal-form-tnx" id="modal-form-register">
    <div class="popup-buble">
        <div class="popup-dialog">
            <h2><?=Yii::t('layouts','Успешно')?></h2>
            <p class="subtitle"><?=Yii::t('layouts','Вы были успешно зарегистрированы в нашей системе')?></p>
            <a class="btn" href="<?=Url::to(['/type/index'])?>">
                <span class="arrow-txt"><?=Yii::t('layouts','Вернутся на главную')?></span>
                <span class="arrow-icon">
                <svg class="ico ico-arrow-angle">
                  <use xlink:href="/typewriter/img/sprite.svg#ico-arrow-angle"></use>
                </svg>
                </span>
            </a>
        </div>
    </div>
</div>

<div class="" style="display: none">
    <button id="thanks_button" class="js-popup" data-mfp-src="#modal-form-tnx">
        <svg class="ico ico-tel">
            <use xlink:href="/typewriter/img/sprite.svg#ico-tel"></use>
        </svg>
    </button>
    <button id="register_button" class="js-popup" data-mfp-src="#modal-form-register">
        <svg class="ico ico-tel">
            <use xlink:href="/typewriter/img/sprite.svg#ico-tel"></use>
        </svg>
    </button>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>


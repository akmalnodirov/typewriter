<?php

namespace app\widgets;
use yii\base\Widget;


/**
 * Class HeaderWidget
 * @package app\widgets
 */
class MobileFooterWidget extends Widget
{

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('mobileFooter');
    }
}

?>
<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;

$menus = $this->params['menu'];
$settings = $this->params['settings'];
$comment = $this->params['comment'];

?>
<footer class="footer nav-hover-link">
    <div class="container">
        <!-- top-->
        <div class="footer-top row">
            <div class="footer-m-menu">
                <div class="header__hamburger">
                    <div class="hamburger hamburger--elastic js-hamburger-footer">
                        <div class="hamburger-box">
                            <div class="hamburger-inner"></div>
                        </div>
                    </div>
                </div>
            </div>
            <nav class="footer-nav">
                <ul>
                    <?php foreach ($menus as $menuKey => $menuItem): ?>
                        <li><a href="<?= Url::to([$menuItem->link]) ?>"><?= $menuItem->name ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </nav>
            <!-- address-link-->
            <address></address>
            <div class="address-link row list-style">
                <div class="address-link__item social-icon"><a href="#">
                        <svg class="ico ico-fb">
                            <use xlink:href="/typewriter/img/sprite.svg#ico-fb"></use>
                        </svg>
                    </a></div>
                <div class="address-link-group address-link__item row">
                    <div class="address-link__item"><a href="mailto:info@typewriter.ru"><?= $settings->email ?></a>
                    </div>
                    <div class="address-link__item"><a href="tel:+74953455678"><?= $settings->phone ?></a></div>
                </div>
                <div class="address-link__item">
                    <button class="btn-dashed js-popup" data-mfp-src="#modal-form-1">
                        <svg class="ico ico-tel">
                            <use xlink:href="/typewriter/img/sprite.svg#ico-tel"></use>
                        </svg>
                    </button>
                </div>
            </div>
        </div>
        <!-- info-->
        <div class="footer-info row">
            <div class="footer-info__txt">
                <a class="politica" href="<?= Url::to(['type/political-confident']) ?>">
                    <?= Yii::t('footer.php', 'Политика конфинденциальности') ?>
                </a>
                <a class="pixwhite" href=""><?= $settings->footer_text ?></a></div>
            <div class="footer-info__mail">
                <?php $form = ActiveForm::begin(['action' => ['type/comment-save'], 'options' => ['method' => 'post']]); ?>
                <div class="mail-send-wrapper row">
                    <div class="ui-group mail-send-field">
                        <?php echo $form->field($comment, 'email')->textInput([
                            'placeholder' => Yii::t('footer.php', 'Ваша почта')
                        ])->label(false) ?>
                    </div>
                    <div class="ui-send">
                        <input class="btn btn-transparent" type="submit"
                               value="<?= Yii::t('footer.php', 'Подписаться на рассылку') ?>">
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</footer>
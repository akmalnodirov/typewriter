<?php

use yii\helpers\Url;

$menus = $this->params['menu'];
$languages = $this->params['languages'];
$settings = $this->params['settings'];

?>
<header class="header nav-hover-link">
    <div class="container">
        <div class="header-wrapper row">
            <!-- logo -->
            <div class="header-logo"><a href="<?= Url::to(['/type/index']) ?>"><img src="/uploads<?= $settings->logo ?>"
                                                                                    alt=""></a></div>
            <!-- navigation-wrapper-->
            <div class="navigation-wrapper">
                <!-- navigation-->
                <div class="navigation row">
                    <!-- main-nav-->
                    <nav class="main-nav">
                        <ul class="row list-style">
                            <?php foreach ($menus as $menu): ?>


                                <?php
                                $url = null;
                                if ($menu->page_id && $menu->page_type == \app\models\Menu::DYNAMIC_PAGE) {
                                    $url = Url::to(['type/standard-pages', 'page_id' => $menu->page_id]);
                                } else if (!$menu->page_id && $menu->link && $menu->page_type == \app\models\Menu::STANDARD_PAGE) {
                                    $url = Url::to([$menu->link]);
                                }
                                ?>

                                <li><a href="<?= $url ?>"><?= $menu->name ?></a></li>
                            <?php endforeach ?>
                        </ul>
                    </nav>
                    <!-- login-->
                    <nav class="login">
                        <ul class="row list-style">
                            <?php foreach ($languages as $langKey => $langItem): ?>
                                <?php $style = 'style = ""' ?>
                                <?php if ($langItem->code == Yii::$app->language): ?>
                                    <?php $style = 'style = "color:red"' ?>
                                <?php endif; ?>

                                <li>
                                    <a <?= $style ?>
                                            href="<?= Url::to(['/base/set-language', 'language' => $langItem->code]) ?>"><?= $langItem->name ?></a>
                                </li>
                            <?php endforeach; ?>

                            <?php if (Yii::$app->user->isGuest): ?>
                                <li><a href="<?= Url::to(['/site/login']) ?>"><?= Yii::t('header.php', 'Войти') ?></a>
                                </li>
                                <li>
                                    <a href="<?= Url::to(['/site/signup']) ?>"><?= Yii::t('header.php', 'Зарегистрироваться') ?></a>
                                </li>
                            <?php endif ?>
                            <?php if (!Yii::$app->user->isGuest): ?>
                                <li><a href="#"><?= Yii::$app->user->identity->username; ?></a></li>
                                <li>
                                    <a href="<?= Url::to(['/site/profile']) ?>"><?= Yii::t('header.php', 'Мой профиль') ?></a>
                                </li>
                                <li><a href="<?= Url::to(['/site/logout']) ?>"><?= Yii::t('header.php', 'Выход') ?></a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </nav>
                </div>
                <!-- header-user-area-->
                <div class="header-user-area row">
                    <!-- search-->
                    <div class="search">
                        <form action="<?=Url::to(['type/hashtag-search'])?>" method="POST">
                            <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                            <input name="search_value" type="search" placeholder="<?= Yii::t('header.php', 'Поиск по хештегу') ?>">
                            <button class="search-submit">
                                <svg class="ico ico-search">
                                    <use xlink:href="/typewriter/img/sprite.svg#ico-search"></use>
                                </svg>
                            </button>
                        </form>
                    </div>
                    <!-- header-address-->
                    <address class="header-address">
                        <div class="address-link row list-style">
                            <div class="address-link__item social-icon"><a href="#">
                                    <svg class="ico ico-fb">
                                        <use xlink:href="/typewriter/img/sprite.svg#ico-fb"></use>
                                    </svg>
                                </a></div>
                            <div class="address-link-group address-link__item row">
                                <div class="address-link__item"><a
                                            href="mailto:<?= $settings->email ?>"><?= $settings->email ?></a></div>
                                <div class="address-link__item"><a
                                            href="tel:<?= $settings->phone ?>"><?= $settings->phone ?></a></div>
                            </div>
                            <div class="address-link__item">
                                <button class="btn-dashed js-popup" data-mfp-src="#modal-form-1">
                                    <svg class="ico ico-tel">
                                        <use xlink:href="/typewriter/img/sprite.svg#ico-tel"></use>
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </address>
                </div>
            </div>
            <!-- mobile-btns-->
            <div class="mobile-btns row">
                <div class="header__hamburger">
                    <div class="hamburger hamburger--elastic js-hamburger">
                        <div class="hamburger-box">
                            <div class="hamburger-inner"></div>
                        </div>
                    </div>
                </div>
                <div class="mobile-btns-aside row">
                    <button class="search-submit js-search-toggle">
                        <svg class="ico ico-search">
                            <use xlink:href="/typewriter/img/sprite.svg#ico-search"></use>
                        </svg>
                    </button>
                    <div class="address-link__item social-icon"><a href="#">
                            <svg class="ico ico-fb">
                                <use xlink:href="/typewriter/img/sprite.svg#ico-fb"></use>
                            </svg>
                        </a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="mobile-menu">
        <div class="mobile-menu__wrapper">
            <nav class="mobile-menu__nav mobile-navi">
                <ul>
                    <?php foreach ($menus as $menu): ?>
                        <li><a href="#"><?= $menu->name ?></a></li>
                    <?php endforeach ?>
                </ul>
            </nav>
        </div>
    </div>
</header>
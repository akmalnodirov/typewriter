<div class="mobile-menu-footer">
    <div class="mobile-menu__wrapper">
        <nav class="mobile-menu__nav mobile-navi-footer">
            <ul>
                <li> <a href="#">Главная</a></li>
                <li> <a href="#">Каталог</a></li>
                <li> <a href="#">Новости</a></li>
                <li> <a href="#">Блог</a></li>
                <li> <a href="#">Сервис</a></li>
                <li> <a href="#">Партнеры</a></li>
                <li> <a href="#">Дополнительно</a></li>
                <li> <a href="#">Контакты</a></li>
            </ul>
        </nav>
    </div>
</div>
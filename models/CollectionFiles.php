<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "collection_files".
 *
 * @property int $id
 * @property int $collection_id
 * @property string $file
 * @property string $name
 *
 * @property Collection $collection
 */
class CollectionFiles extends \yii\db\ActiveRecord
{
    public $file_dynamic;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'collection_files';
    }

    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'file_dynamic',
                'pathAttribute' => 'file',
                'baseUrlAttribute' => false
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['collection_id'], 'default', 'value' => null],
            [['collection_id'], 'integer'],
            [['file_dynamic'], 'safe'],
            [['file', 'name', 'hashtag'], 'string', 'max' => 255],
            [['collection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Collection::className(), 'targetAttribute' => ['collection_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'collection_id' => Yii::t('app', 'Collection ID'),
            'file' => Yii::t('app', 'File'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCollection()
    {
        return $this->hasOne(Collection::className(), ['id' => 'collection_id']);
    }
}

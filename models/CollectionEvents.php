<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "collection_events".
 *
 * @property int $id
 * @property int $collection_id
 * @property string $event_date
 * @property string $event_description
 * @property string $update_at
 * @property string $create_at
 *
 * @property Collection $collection
 */
class CollectionEvents extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'collection_events';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['collection_id'], 'default', 'value' => null],
            [['collection_id'], 'integer'],
            [['event_description', 'hashtag'], 'string'],
            [['update_at', 'create_at'], 'required'],
            [['update_at', 'create_at'], 'safe'],
            [['event_date'], 'string', 'max' => 255],
            [['collection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Collection::className(), 'targetAttribute' => ['collection_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'collection_id' => Yii::t('app', 'Collection ID'),
            'event_date' => Yii::t('app', 'Event Date'),
            'event_description' => Yii::t('app', 'Event Description'),
            'update_at' => Yii::t('app', 'Update At'),
            'create_at' => Yii::t('app', 'Create At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCollection()
    {
        return $this->hasOne(Collection::className(), ['id' => 'collection_id']);
    }
}

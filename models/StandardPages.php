<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "standard_pages".
 *
 * @property int $id
 * @property int $page_type_id
 * @property string $title
 * @property string $text
 * @property string $image
 * @property string $slug
 * @property int $order
 * @property string $author
 * @property int $created_at
 * @property int $updated_at
 */
class StandardPages extends \yii\db\ActiveRecord
{
    public $image_dynamic;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'standard_pages';
    }

    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'image_dynamic',
                'pathAttribute' => 'image',
                'baseUrlAttribute' => false
            ],
            [
                'class' => \nsept\behaviors\CyrillicSlugBehavior::className(),
                'attribute' => 'title',
                //'slugAttribute' => 'slug',
            ],
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_type_id', 'order', 'created_at', 'updated_at'], 'default', 'value' => null],
            [['page_type_id', 'order', 'created_at', 'updated_at'], 'integer'],
            [['page_type_id', 'text', 'title'], 'required'],
            [['text'], 'string'],
            [['image_dynamic'], 'safe'],
            [['title', 'image', 'slug', 'author', 'hashtag'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'page_type_id' => Yii::t('app', 'Page Type ID'),
            'title' => Yii::t('app', 'Title'),
            'text' => Yii::t('app', 'Text'),
            'image' => Yii::t('app', 'Image'),
            'slug' => Yii::t('app', 'Slug'),
            'order' => Yii::t('app', 'Order'),
            'author' => Yii::t('app', 'Author'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function getPageType()
    {
        return $this->hasOne(PageTypes::className(), ['id' => 'page_type_id']);
    }
}

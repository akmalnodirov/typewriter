<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "brand_slider".
 *
 * @property int $id
 * @property string $first_title
 * @property string $second_title
 * @property string $description
 */
class BrandSlider extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'brand_slider';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['first_title', 'second_title', 'link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'first_title' => Yii::t('app', 'First Title'),
            'second_title' => Yii::t('app', 'Second Title'),
            'description' => Yii::t('app', 'Description'),
        ];
    }
}

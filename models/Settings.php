<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property int $id
 * @property string $brands_index_bg
 * @property string $catalog_museum_index_bg
 * @property string $news_index_bg
 * @property string $partners_index_bg
 * @property string $collection_bg
 * @property string $card_collection_bg
 * @property string $card_model_bg
 * @property string $catalog_model_bg
 * @property string $all_news_bg
 * @property string $forum_bg
 * @property string $news_item_bg
 * @property string $login_bg
 * @property string $profile_bg
 * @property string $registration_bg
 * @property string $logo
 * @property string $email
 * @property string $phone
 * @property string $confidential_link
 * @property string $patent_text
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings';
    }

    public $logo_dynamic;
    public $brands_index_bg_dynamic;
    public $catalog_museum_index_bg_dynamic;
    public $news_index_bg_dynamic;
    public $partners_index_bg_dynamic;
    public $collection_bg_dynamic;
    public $card_collection_bg_dynamic;
    public $card_model_bg_dynamic;
    public $catalog_model_bg_dynamic;
    public $all_news_bg_dynamic;
    public $forum_bg_dynamic;
    public $news_item_bg_dynamic;
    public $login_bg_dynamic;
    public $profile_bg_dynamic;
    public $registration_bg_dynamic;
    public $avatar_dynamic;

    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'avatar_dynamic',
                'pathAttribute' => 'forum_avatar',
                'baseUrlAttribute' => false
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'logo_dynamic',
                'pathAttribute' => 'logo',
                'baseUrlAttribute' => false
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'brands_index_bg_dynamic',
                'pathAttribute' => 'brands_index_bg',
                'baseUrlAttribute' => false
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'catalog_museum_index_bg_dynamic',
                'pathAttribute' => 'catalog_museum_index_bg',
                'baseUrlAttribute' => false
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'news_index_bg_dynamic',
                'pathAttribute' => 'news_index_bg',
                'baseUrlAttribute' => false
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'partners_index_bg_dynamic',
                'pathAttribute' => 'partners_index_bg',
                'baseUrlAttribute' => false
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'collection_bg_dynamic',
                'pathAttribute' => 'collection_bg',
                'baseUrlAttribute' => false
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'card_collection_bg_dynamic',
                'pathAttribute' => 'card_collection_bg',
                'baseUrlAttribute' => false,
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'card_model_bg_dynamic',
                'pathAttribute' => 'card_model_bg',
                'baseUrlAttribute' => false,
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'catalog_model_bg_dynamic',
                'pathAttribute' => 'catalog_model_bg',
                'baseUrlAttribute' => false,
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'all_news_bg_dynamic',
                'pathAttribute' => 'all_news_bg',
                'baseUrlAttribute' => false,
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'forum_bg_dynamic',
                'pathAttribute' => 'forum_bg',
                'baseUrlAttribute' => false,
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'news_item_bg_dynamic',
                'pathAttribute' => 'news_item_bg',
                'baseUrlAttribute' => false,
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'login_bg_dynamic',
                'pathAttribute' => 'login_bg',
                'baseUrlAttribute' => false,
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'profile_bg_dynamic',
                'pathAttribute' => 'profile_bg',
                'baseUrlAttribute' => false,
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'registration_bg_dynamic',
                'pathAttribute' => 'registration_bg',
                'baseUrlAttribute' => false,
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'phone'], 'required'],
            [['facebook', 'instagram'], 'required'],
            [[
                'logo_dynamic',
                'brands_index_bg_dynamic',
                'catalog_museum_index_bg_dynamic',
                'news_index_bg_dynamic',
                'partners_index_bg_dynamic',
                'collection_bg_dynamic',
                'card_collection_bg_dynamic',
                'card_model_bg_dynamic',
                'catalog_model_bg_dynamic',
                'all_news_bg_dynamic',
                'forum_bg_dynamic',
                'news_item_bg_dynamic',
                'login_bg_dynamic',
                'profile_bg_dynamic',
                'registration_bg_dynamic',
                'avatar_dynamic'
            ], 'safe'],
            [['partners_text','brands_index_bg', 'catalog_museum_index_bg', 'news_index_bg', 'partners_index_bg', 'collection_bg', 'card_collection_bg', 'card_model_bg', 'catalog_model_bg', 'all_news_bg', 'forum_bg', 'news_item_bg', 'login_bg', 'profile_bg', 'registration_bg', 'logo', 'email', 'phone', 'confidential_link', 'footer_text'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'brands_index_bg' => Yii::t('app', 'Brands Index Bg'),
            'catalog_museum_index_bg' => Yii::t('app', 'Catalog Museum Index Bg'),
            'news_index_bg' => Yii::t('app', 'News Index Bg'),
            'partners_index_bg' => Yii::t('app', 'Partners Index Bg'),
            'collection_bg' => Yii::t('app', 'Collection Bg'),
            'card_collection_bg' => Yii::t('app', 'Card Collection Bg'),
            'card_model_bg' => Yii::t('app', 'Card Model Bg'),
            'catalog_model_bg' => Yii::t('app', 'Catalog Model Bg'),
            'all_news_bg' => Yii::t('app', 'All News Bg'),
            'forum_bg' => Yii::t('app', 'Forum Bg'),
            'news_item_bg' => Yii::t('app', 'News Item Bg'),
            'login_bg' => Yii::t('app', 'Login Bg'),
            'profile_bg' => Yii::t('app', 'Profile Bg'),
            'registration_bg' => Yii::t('app', 'Registration Bg'),
            'logo' => Yii::t('app', 'Logo'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'confidential_link' => Yii::t('app', 'Confidential Link'),
        ];
    }
}

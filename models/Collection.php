<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "collection".
 *
 * @property int $id
 * @property string $title
 * @property string $name
 * @property string $slug
 * @property string $hashtag
 * @property string $logo
 * @property string $history
 *
 * @property CollectionDetails[] $collectionDetails
 * @property CollectionDocuments[] $collectionDocuments
 * @property CollectionEvents[] $collectionEvents
 * @property CollectionForum[] $collectionForums
 * @property CollectionImages[] $collectionImages
 * @property CollectionModels[] $collectionModels
 */
class Collection extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'collection';
    }

    /**
     * @var
     */
    public $logo_dynamic;
    /**
     * @var
     */
    public $collection_images;
    /**
     * @var
     */
    public $collection_details;
    /**
     * @var array
     */
    public $events = [];

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'logo_dynamic',
                'pathAttribute' => 'logo',
                'baseUrlAttribute' => false
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'collection_images',
                'multiple' => true,
                'uploadRelation' => 'collectionImages',
                'pathAttribute' => 'image',
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'collection_details',
                'multiple' => true,
                'uploadRelation' => 'collectionDetails',
                'pathAttribute' => 'image',
            ],
            [
                'class' => \nsept\behaviors\CyrillicSlugBehavior::className(),
                'attribute' => 'title',
                //'slugAttribute' => 'slug',
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['history', 'start_symbol'], 'string'],
            [['events'], 'safe'],
            [['title', 'name', 'history', 'start_symbol'], 'required'],
            [['title', 'name', 'slug', 'hashtag', 'logo'], 'string', 'max' => 255],
            [['logo_dynamic', 'collection_images','collection_details', 'collection_documents'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'name' => Yii::t('app', 'Name'),
            'slug' => Yii::t('app', 'Slug'),
            'hashtag' => Yii::t('app', 'Hashtag'),
            'logo' => Yii::t('app', 'Logo'),
            'history' => Yii::t('app', 'History'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCollectionDetails()
    {
        return $this->hasMany(CollectionDetails::className(), ['collection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCollectionFiles()
    {
        return $this->hasMany(CollectionFiles::className(), ['collection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCollectionDocuments()
    {
        return $this->hasMany(CollectionDocuments::className(), ['collection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCollectionEvents()
    {
        return $this->hasMany(CollectionEvents::className(), ['collection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCollectionForums()
    {
        return $this->hasMany(CollectionForum::className(), ['collection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCollectionImages()
    {
        return $this->hasMany(CollectionImages::className(), ['collection_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCollectionModels()
    {
        return $this->hasMany(CollectionModels::className(), ['collection_id' => 'id']);
    }

    /**
     *
     */
    public static function getModelsCountWithName(){
        $collections = Collection::find()->all();
        $result = [];
        foreach($collections as $key=>$item){
            $result[$key]['collection_name'] = $item->name;
            $result[$key]['collection_id'] = $item->id;
            if($item->collectionModels){
                $result[$key]['modelsCount'] = count($item->collectionModels);
            } else {
                $result[$key]['modelsCount'] = 0;
            }
        }
        return $result;
    }
}
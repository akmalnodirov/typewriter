<?php
namespace app\models\Authorization;

use app\models\User;
use Yii;
use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $agreement;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['agreement', 'required', 'requiredValue' => 1, 'message' => 'Вы должны установить этот флажок, чтобы согласиться на обработку ваших личных данных'],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return array
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->status = 10;
        $user->type = 0;
        $user->agreement = $this->agreement;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        $user->created_at = time();
        $user->updated_at = time();

        if($user->save()){
            $result = ['signup' => true, 'user' => $user];
        } else {
            $result = ['signup' => false, 'user' => false];
        }

        return $result; //$user->save(); //$user->save() && $this->sendEmail($user);

    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        $trim_ = trim($this->email);
        $filter_var = filter_var($trim_, FILTER_SANITIZE_EMAIL);
        $iconv = iconv('ISO-8859-1','UTF-8//IGNORE', $filter_var);

        return Yii::$app
            ->mailer
            ->compose()
            ->setFrom('akmal.nodirov@gmail.com')
            ->setTo($trim_)
            ->setSubject('Account registration at ')
            ->send();
    }
}

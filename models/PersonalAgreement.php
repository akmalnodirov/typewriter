<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "personal_agreement".
 *
 * @property int $id
 * @property string $title
 * @property string $text
 * @property string $image
 * @property int $created_at
 * @property int $updated_at
 */
class PersonalAgreement extends \yii\db\ActiveRecord
{
    public $image_dynamic;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'personal_agreement';
    }

    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'image_dynamic',
                'pathAttribute' => 'image',
                'baseUrlAttribute' => false
            ],
            [
                'class' => \nsept\behaviors\CyrillicSlugBehavior::className(),
                'attribute' => 'title',
                //'slugAttribute' => 'slug',
            ],
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['image_dynamic', 'slug'], 'safe'],
            [['title', 'text'], 'required'],
            [['created_at', 'updated_at'], 'default', 'value' => null],
            [['created_at', 'updated_at'], 'integer'],
            [['title', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'text' => Yii::t('app', 'Text'),
            'image' => Yii::t('app', 'Image'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}

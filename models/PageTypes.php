<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "page_types".
 *
 * @property int $id
 * @property int $name
 * @property int $page_type
 */
class PageTypes extends \yii\db\ActiveRecord
{

    const SINGLE_PAGE = 0;
    const MULTIPAGE = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'page_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'page_type'], 'default', 'value' => null],
            [['name', 'page_type'], 'required'],
            [['page_type', 'status'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'page_type' => Yii::t('app', 'Page Type'),
        ];
    }

    public function pageTypes(){
        return [
            self::SINGLE_PAGE => 'Одностраничная страница',
            self::MULTIPAGE => 'Многостраничная страница',
        ];
    }
}

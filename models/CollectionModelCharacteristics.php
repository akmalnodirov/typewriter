<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "collection_model_characteristics".
 *
 * @property int $id
 * @property int $collection_model_id
 * @property string $key
 * @property string $value
 *
 * @property CollectionModels $collectionModel
 */
class CollectionModelCharacteristics extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'collection_model_characteristics';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['collection_model_id'], 'default', 'value' => null],
            [['collection_model_id'], 'integer'],
            [['key', 'value', 'hashtag'], 'string', 'max' => 255],
            [['collection_model_id'], 'exist', 'skipOnError' => true, 'targetClass' => CollectionModels::className(), 'targetAttribute' => ['collection_model_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'collection_model_id' => Yii::t('app', 'Collection Model ID'),
            'key' => Yii::t('app', 'Key'),
            'value' => Yii::t('app', 'Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCollectionModel()
    {
        return $this->hasOne(CollectionModels::className(), ['id' => 'collection_model_id']);
    }
}

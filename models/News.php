<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $text
 * @property string $image
 * @property string $posted_date
 * @property string $tegs
 * @property int $created_at
 * @property int $updated_at
 * @property int $order
 */
class News extends \yii\db\ActiveRecord
{

    public  $main_image;

    /**
     * {@inheritdoc}
     */

    public static function tableName()
    {
        return 'news';
    }
    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'main_image',
                'pathAttribute' => 'image',
                'baseUrlAttribute' => false
            ],
            [
                'class' => \nsept\behaviors\CyrillicSlugBehavior::className(),
                'attribute' => 'title',
                //'slugAttribute' => 'slug',
            ],
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['main_image'], 'safe'],
            [['created_at', 'updated_at', 'order'], 'default', 'value' => null],
            [['created_at', 'updated_at', 'order'], 'integer'],
            [['title', 'slug', 'image', 'posted_date', 'tegs', 'hashtag'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'slug' => Yii::t('app', 'Slug'),
            'text' => Yii::t('app', 'Text'),
            'image' => Yii::t('app', 'Image'),
            'posted_date' => Yii::t('app', 'Posted Date'),
            'tegs' => Yii::t('app', 'Tegs'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'order' => Yii::t('app', 'Order'),
        ];
    }
}

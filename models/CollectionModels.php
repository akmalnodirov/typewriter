<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "collection_models".
 *
 * @property int $id
 * @property int $collection_id
 * @property string $name
 * @property string $slug
 * @property string $old
 * @property string $image
 * @property string $description
 * @property string $update_at
 * @property string $create_at
 *
 * @property CollectionModelCharacteristics[] $collectionModelCharacteristics
 * @property CollectionModelImages[] $collectionModelImages
 * @property Collection $collection
 */
class CollectionModels extends \yii\db\ActiveRecord
{

    public $main_image;
    public $model_image;
    public $characters = [];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'collection_models';
    }

    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'main_image',
                'pathAttribute' => 'image',
                'baseUrlAttribute' => false
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'model_image',
                'multiple' => true,
                'uploadRelation' => 'collectionModelImages',
                'pathAttribute' => 'image',
            ],
            [
                'class' => \nsept\behaviors\CyrillicSlugBehavior::className(),
                'attribute' => 'name',
                //'slugAttribute' => 'slug',
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['collection_id'], 'default', 'value' => null],
            [['collection_id'], 'integer'],
            [['main_image', 'model_image'], 'safe'],
            [['description'], 'string'],
            [['update_at', 'create_at'], 'required'],
            [['update_at', 'create_at', 'characters'], 'safe'],
            [['name', 'slug', 'old', 'image', 'hashtag'], 'string', 'max' => 255],
            [['collection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Collection::className(), 'targetAttribute' => ['collection_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'collection_id' => Yii::t('app', 'Collection ID'),
            'name' => Yii::t('app', 'Name'),
            'slug' => Yii::t('app', 'Slug'),
            'old' => Yii::t('app', 'Old'),
            'image' => Yii::t('app', 'Image'),
            'description' => Yii::t('app', 'Description'),
            'update_at' => Yii::t('app', 'Update At'),
            'create_at' => Yii::t('app', 'Create At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCollectionModelCharacteristics()
    {
        return $this->hasMany(CollectionModelCharacteristics::className(), ['collection_model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCollectionModelImages()
    {
        return $this->hasMany(CollectionModelImages::className(), ['collection_model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCollection()
    {
        return $this->hasOne(Collection::className(), ['id' => 'collection_id']);
    }
}

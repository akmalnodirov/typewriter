<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "menu".
 *
 * @property int $id
 * @property string $name
 * @property int $order
 * @property array $pages
 * @property string $link
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu';
    }

    const DYNAMIC_PAGE = 2;
    const STANDARD_PAGE = 1;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order'], 'default', 'value' => null],
            [['order', 'status', 'page_id', 'page_type'], 'integer'],
            [['name', 'page_type'], 'required'],
            [['name', 'link'], 'string', 'max' => 255],
            [['link'], 'required',
                'when' => function ($model) {
                    return $model->page_type != 2;
                }, 'whenClient' => "function (attribute, value) {
                    return  $('#menu-page_type').val() != 2;
                }",
            ],
            [['page_id'], 'required',
                'when' => function ($model) {
                    return $model->page_type != 1;
                }, 'whenClient' => "function (attribute, value) {
                    return  $('#menu-page_type').val() != 1;
                }",
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название меню'),
            'order' => Yii::t('app', 'Order'),
            'link' => Yii::t('app', 'Выберите линк'),
            'page_type' => Yii::t('app', 'Тип страницы'),
            'page_id' => Yii::t('app', 'Выберите страницу'),
        ];
    }

    public function getPages()
    {

        return [
            'type/index' => 'Главная',
            'type/catalog' => 'Каталог',
            'type/news' => 'Новости',
        ];
    }

    public function dynamicPages()
    {

        return ArrayHelper::map(PageTypes::find()->where(['status' => 1])->all(), 'id', 'name');
    }

    public function getPage()
    {
        return $this->hasOne(PageTypes::className(), ['id' => 'page_id']);
    }
}

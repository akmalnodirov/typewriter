<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "languages".
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property int $is_main
 * @property int $status
 * @property string $image
 */
class Languages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'languages';
    }

    public $image_dynamic;

    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'image_dynamic',
                'pathAttribute' => 'image',
                'baseUrlAttribute' => false
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_main', 'status'], 'default', 'value' => null],
            [['is_main', 'status'], 'integer'],
            [['code'], 'string', 'max' => 3],
            [['name'], 'string', 'max' => 15],
            [['image'], 'string', 'max' => 255],
            [['image_dynamic'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Name'),
            'is_main' => Yii::t('app', 'Is Main'),
            'status' => Yii::t('app', 'Status'),
            'image' => Yii::t('app', 'Image'),
        ];
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%collection_forum}}`.
 */
class m191117_083735_create_collection_forum_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%collection_forum}}', [
            'id' => $this->primaryKey(),
            'collection_id' => $this->integer(),
            'user_id' => $this->integer(),
            'parent_id' => $this->integer()->null(),
            'level' => $this->smallInteger()->defaultValue(0),
            'comment' => $this->string(),
            'update_at' => $this->timestamp()->notNull(),
            'create_at' => $this->timestamp()->notNull(),
        ]);

        $this->createIndex('idx-forum-collection_id','collection_forum','collection_id');
        $this->addForeignKey('fk-forum-collection_id','collection_forum','collection_id','collection','id','CASCADE');

        $this->createIndex('idx-forum-collection_id','collection_forum','collection_id');
        $this->addForeignKey('fk-forum-collection_id','collection_forum','collection_id','collection','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-forum-collection_id','collection_forum');
        $this->dropIndex('idx-forum-collection_id','collection_forum');
        $this->dropTable('{{%collection_forum}}');
    }
}

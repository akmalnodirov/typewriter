<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Languages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-header">
        <div class="row justify-content-between" style="margin: auto">
            <div class="">
                <h3>Языки</h3>
                <span>Добавление нового языка</span>
            </div>
            <p>
                <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
        </div>
    </div>
    <div class="card-block">


        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'code',
                'name',
                'is_main',
                'status',
                [
                    'attribute' => 'main_image',
                    'value' => function ($model) {
                        return '<img src="/uploads' . $model->image . '" width="60">';
                    },
                    'format' => 'raw',
                ],

                ['class' => 'app\components\oweriddenClasses\CustomColumn', 'template' => '{update},{delete}'],
            ],
        ]); ?>
    </div>
</div>

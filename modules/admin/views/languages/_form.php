<?php

use app\components\oweriddenClasses\CustomUpload;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Languages */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="card">
    <div class="card-block">
        <h2 class="sub-title" style="font-size: 25px;">Редактировать</h2>
        <?php $form = ActiveForm::begin(); ?>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Название языка</label>
            <div class="col-sm-5">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label(false) ?>
            </div>
            <label class="col-sm-2 col-form-label">Код языка</label>
            <div class="col-sm-2">
                <?= $form->field($model, 'code')->textInput(['maxlength' => true])->label(false) ?>
            </div>
        </div>

        <div class="form-group row" style="vertical-align: middle">
            <label class="col-sm-2 col-form-label">Статус</label>
            <div class="col-sm-5">
                <?= $form->field($model, 'is_main')->dropDownList([1 => 'Выключен', 0 => 'Отключен'])->label(false) ?>
            </div>
            <label class="col-sm-2 col-form-label">Основной язык</label>
            <div class="col-sm-2">
                <?= $form->field($model, 'status')->dropDownList([1 => 'Да', 0 => 'Нет'])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Фото</label>
            <div class="col-sm-4">
                <?= $form->field($model, 'image_dynamic')->widget(CustomUpload::classname(), [
                    'url' => ['/file-storage/upload'],
                    'maxFileSize' => 2000000, // 5 MiB
                ])->label(false) ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>

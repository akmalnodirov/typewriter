<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CommentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Comments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-header">
        <div class="row justify-content-between" style="margin: auto">
            <div class="">
                <h3>Обратная связь</h3>
                <span></span>
            </div>
        </div>
    </div>
    <div class="card-block">

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'full_name',
                'phone',
                'email:email',
                [
                    'contentOptions' => ['style' => 'white-space: normal'],
                    'attribute' => 'body',
                    'label' => 'Текст',
                    'value' => function ($model) {
                        return $model->body;
                    }
                ],
                [
                    'contentOptions' => ['style' => 'white-space: normal'],
                    'attribute' => 'created_at',
                    'label' => 'Дата',
                    'value' => function ($model) {
                        return Yii::$app->formatter->asDate($model->created_at);
                    }
                ],

                ['class' => 'app\components\oweriddenClasses\CustomColumn', 'template' => '{delete}'],
            ],
        ]); ?>

    </div>
</div>

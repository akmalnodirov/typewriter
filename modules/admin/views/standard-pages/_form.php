<?php

use app\components\oweriddenClasses\CustomUpload;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
/* @var $this yii\web\View */
/* @var $model app\models\StandardPages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card">
    <div class="card-block">
        <h2 class="sub-title" style="font-size: 25px;">Редактировать</h2>
        <?php $form = ActiveForm::begin(); ?>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Выберите тип страницы</label>
            <div class="col-sm-8">
                <?= $form->field($model, 'page_type_id')
                    ->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\PageTypes::find()->all(), 'id', 'name'))
                    ->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Название</label>
            <div class="col-sm-8">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Теги</label>
            <div class="col-sm-8">
                <?= $form->field($model, 'hashtag')->textInput(['maxlength' => true])->label(false) ?>
            </div>
        </div>


        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Текст</label>
            <div class="col-sm-8">

                <?= $form->field($model, 'text')->widget(CKEditor::className(), [
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                        'preset' => 'full',
                        'height' => '500px'
                    ]),
                ])->label(false); ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label"></label>
            <div class="col-sm-4">
                <?= $form->field($model, 'order')->textInput(['placeholder' => 'Ордер'])->label(false) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'author')->textInput(['placeholder' => 'Автор'])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Фото</label>
            <div class="col-sm-4">
                <?= $form->field($model, 'image_dynamic')->widget(CustomUpload::classname(), [
                    'url' => ['/file-storage/upload'],
                    'maxFileSize' => 2000000, // 5 MiB
                ])->label(false) ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StandardPages */

$this->title = Yii::t('app', 'Update Standard Pages: {name}', [
    'name' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Standard Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="standard-pages-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

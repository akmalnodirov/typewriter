<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StandardPages */

$this->title = Yii::t('app', 'Create Standard Pages');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Standard Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="standard-pages-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card">
    <div class="card-block">
        <h2 class="sub-title" style="font-size: 25px;">Редактировать</h2>
        <?php $form = ActiveForm::begin(); ?>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Логин</label>
            <div class="col-sm-6">
                <?= $form->field($model, 'username')->textInput()->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Статус</label>
            <div class="col-sm-6">
                <?= $form->field($model, 'status')->dropDownList([10 => 'Актив', 0 => 'Отклчен'])->label(false) ?>
            </div>
        </div>


        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Тип</label>
            <div class="col-sm-6">
                <?= $form->field($model, 'type')->dropDownList([1 => 'Модератор', 0 => 'Ползователь'])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Пароль</label>
            <div class="col-sm-6">
                <?= $form->field($model, 'password')->textInput()->label(false) ?>
            </div>
        </div>


        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

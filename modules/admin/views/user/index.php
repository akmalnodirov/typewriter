<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-header">
        <div class="row justify-content-between" style="margin: auto">
            <div class="">
                <h3>Пользователи</h3>
                <span>Информации о ползователей</span>
            </div>
            <p>
                <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
        </div>
        <div class="card-block">

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'username',
                    [
                        'contentOptions' => ['style' => 'white-space: normal'],
                        'attribute' => 'type',
                        'label' => 'Тип',
                        'value' => function ($model) {
                            if($model->type == 1) { return 'Админ';} else return 'Ползователь';
                        }
                    ],
                    [
                        'contentOptions' => ['style' => 'white-space: normal'],
                        'attribute' => 'status',
                        'label' => 'Статус',
                        'value' => function ($model) {
                           if($model->status == 10) { return 'Актив';} else return 'Отключен';
                        }
                    ],
                    [
                        'contentOptions' => ['style' => 'white-space: normal'],
                        'attribute' => 'created_at',
                        'label' => 'Дата',
                        'value' => function ($model) {
                            return Yii::$app->formatter->asDate($model->created_at);
                        }
                    ],

                    ['class' => 'app\components\oweriddenClasses\CustomColumn', 'template' => '{update}{delete}'],
                ],
            ]); ?>


        </div>
    </div>

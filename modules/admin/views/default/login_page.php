<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

  <div class="splash-container">
    <div class="panel panel-default panel-border-color panel-border-color-primary">
      <div class="panel-heading"><img src="/admin_assets/img/logo-xx.png" alt="logo" width="102" height="27" class="logo-img"><span class="splash-description">Please enter your user information.</span></div>
      <div class="panel-body">
           <?php $form = ActiveForm::begin(); ?>

          <div class="login-form">
              <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
            <div class="form-group">
              <?= $form->field($model, 'password')->passwordInput() ?>
            </div>
            <div class="form-group row login-submit">
              <div class="">
                <button data-dismiss="modal" type="submit" class="btn btn-primary btn-xl">Sign in</button>
              </div>
            </div>
          </div>
        <?php ActiveForm::end(); ?>
      </div>
    </div>
  </div>
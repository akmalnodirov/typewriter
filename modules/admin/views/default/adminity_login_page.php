<?php

 use yii\bootstrap\ActiveForm;

?>

      <div class="row">
             <div class="col-sm-12">
                 <?php $form = ActiveForm::begin(['options' => ['class' => 'md-float-material form-material']]); ?>

                     <div class="text-center">
                         <img src="/adminty/images/type_logo.png" alt="logo.png">
                     </div>
                     <div class="auth-box card">
                         <div class="card-block">
                             <div class="row m-b-20">
                                 <div class="col-md-12">
                                     <h3 class="text-center">Войти</h3>
                                 </div>
                             </div>
                             <div class="form-group form-primary">
                                 <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label('Логин') ?>
                                 <span class="form-bar"></span>
                             </div>
                             <div class="form-group form-primary">
                                 <?= $form->field($model, 'password')->passwordInput()->label('Пароль') ?>
                                 <span class="form-bar"></span>
                             </div>
                             <div class="row m-t-30">
                                 <div class="col-md-12">
                                     <button type="submit"
                                             class="btn btn-primary btn-md btn-block waves-effect waves-light text-center m-b-20">
                                         В Х О Д
                                     </button>
                                 </div>
                             </div>
                             <hr/>
                         </div>
                     </div>
                  <?php ActiveForm::end(); ?>

             </div>

         </div>
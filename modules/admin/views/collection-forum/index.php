<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CollectionForumSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Collection Forums';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-header">
        <div class="row justify-content-between" style="margin: auto">
            <div class="">
                <h3>Переписки</h3>
                <span>Переписки форума</span>
            </div>
        </div>
    </div>
    <div class="card-block">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'contentOptions' => ['style' => 'white-space: normal'],
                'attribute' => 'user',
                'label' => 'Пользователь',
                'value' => function ($model) {
                    return $model->user ? $model->user->username : '';
                }
            ],
            [
                'contentOptions' => ['style' => 'white-space: normal'],
                'attribute' => 'body',
                'label' => 'Текст',
                'value' => function ($model) {
                    return $model->body;
                }
            ],
            [
                'contentOptions' => ['style' => 'white-space: normal'],
                'attribute' => 'forum_theme_id',
                'label' => 'Тема',
                'value' => function ($model) {
                    return $model->forumTheme ? $model->forumTheme->theme_name : '';
                }
            ],
            [
                'contentOptions' => ['style' => 'white-space: normal'],
                'attribute' => 'created_at',
                'label' => 'Дата',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDate($model->created_at);
                }
            ],

            ['class' => 'app\components\oweriddenClasses\CustomColumn', 'template' => '{delete}'],
        ],
    ]); ?>


</div>

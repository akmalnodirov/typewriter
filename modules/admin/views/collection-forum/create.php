<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CollectionForum */

$this->title = 'Create Collection Forum';
$this->params['breadcrumbs'][] = ['label' => 'Collection Forums', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="collection-forum-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

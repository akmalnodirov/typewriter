<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PageTypesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Page Types');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-header">
        <div class="row justify-content-between" style="margin: auto">
            <div class="">
                <h3>Типы страниц</h3>
                <span>Добавление типи страницы</span>
            </div>
            <p>
                <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
        </div>
    </div>
    <div class="card-block">

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],


                [
                    'contentOptions' => ['style' => 'white-space: normal'],
                    'attribute' => 'name',
                    'label' => 'Название',
                    'value' => function ($model) {
                        return $model->name;
                    }
                ],

                [
                    'contentOptions' => ['style' => 'white-space: normal'],
                    'attribute' => 'page_type',
                    'label' => 'Типы страницы',
                    'value' => function ($model) {
                        return $model->pageTypes()[$model->page_type];
                    }
                ],

                'status',

                ['class' => 'app\components\oweriddenClasses\CustomColumn', 'template' => '{update}{delete}'],
            ],
        ]); ?>


    </div>
</div>

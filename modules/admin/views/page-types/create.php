<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PageTypes */

$this->title = Yii::t('app', 'Create Page Types');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Page Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-types-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PageTypes */

$this->title = Yii::t('app', 'Update Page Types: {name}', [
    'name' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Page Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="page-types-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

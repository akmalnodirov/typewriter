<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SourceMessage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card">
    <div class="card-block">
        <h2 class="sub-title" style="font-size: 25px;">Редактировать</h2>
        <?php $form = ActiveForm::begin(); ?>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Категория</label>
            <div class="col-sm-9">
                <?= $form->field($model, 'category')->textInput(['maxlength' => true])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Источник</label>
            <div class="col-sm-9">
                <?= $form->field($model, 'message')->textarea(['rows' => 6])->label(false) ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

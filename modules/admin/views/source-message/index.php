<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SourceMessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Source Messages');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="card">
    <div class="card-header">
        <div class="row justify-content-between" style="margin: auto">
            <div class="">
                <h3>Транслейты</h3>
                <span>Добавление нового словаря</span>
            </div>
            <p>
                <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
        </div>
    </div>
    <div class="card-block">

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'category',
                'message:ntext',
                [
                    'headerOptions' => ['style'=>'vertical-align: middle; text-align: center;'],
                    'contentOptions' => ['style'=>'vertical-align: middle; text-align: center;'],
                    'label' => 'Переводить',
                    'value' => function($model){
                        return Html::a('<span class="fa fa-language" style="font-size: 20px"></span>', Url::to(['/admin/source-message/update-translation', 'id' => $model->id]), [
                            'title' => Yii::t('app', 'lead-delete'),
                        ]);
                    },
                    'format' => 'html'
                ],

                ['class' => 'app\components\oweriddenClasses\CustomColumn',
                    'template' => '{update},{delete}'],
            ],
        ]); ?>


    </div>

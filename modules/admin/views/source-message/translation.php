<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SourceMessage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card">
    <div class="card-block">
        <h2 class="sub-title" style="font-size: 25px;">Редактировать</h2>
        <?php $form = ActiveForm::begin(['action' => Url::to('/admin/source-message/save-translation'), 'method' => 'post']); ?>

        <div class="col-lg-12">
            <ul class="nav nav-tabs md-tabs tabs-left b-none" role="tablist">
                <?php foreach($messages as $oneKey=>$oneValue):?>
                <li class="nav-item">
                    <a class="nav-link <?php if($oneKey == 0) { echo 'active';}?>" data-toggle="tab" href="#<?=$oneKey . 'language'?>" role="tab" aria-expanded="false"><?=$oneValue->languages ? $oneValue->languages->name : ''?></a>
                    <div class="slide"></div>
                </li>
                <?php endforeach?>
                <input type="hidden" name="model_id" value="<?=$messages[0] ? $messages[0]->id : 0?>">
            </ul>

            <div class="tab-content tabs-left-content card-block">
                <?php foreach($messages as $secondKey=>$secondValue):?>
                <div class="tab-pane <?php if($secondKey == 0) { echo 'active';}?>" id="<?=$secondKey. 'language'?>" role="tabpanel" aria-expanded="false">
                    <p class="m-0"><textarea name="<?=$secondValue->language?>" id="" cols="80" rows="4"><?=$secondValue->translation?></textarea></p>
                </div>
                <?php endforeach?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Settings */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="card">
    <div class="card-header">
        <div class="row justify-content-between" style="margin: auto">
            <div class="">
                <h3>Настройки</h3>
                <span>Обновление парамеры настройки</span>
            </div>
            <p>
                <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            </p>
        </div>
    </div>
    <div class="card-block">


        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [

                [
                    'attribute' => 'forum_avatar',
                    'value' => function ($model) {
                        return '<img src="/uploads' . $model->forum_avatar . '" width="100">';
                    },
                    'format' => 'raw',
                ],

                [
                    'attribute' => 'brands_index_bg',
                    'value' => function ($model) {
                        return '<img src="/uploads' . $model->brands_index_bg . '" width="100">';
                    },
                    'format' => 'raw',
                ],

                [
                    'attribute' => 'catalog_museum_index_bg',
                    'value' => function ($model) {
                        return '<img src="/uploads' . $model->catalog_museum_index_bg . '" width="100">';
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'news_index_bg',
                    'value' => function ($model) {
                        return '<img src="/uploads' . $model->news_index_bg . '" width="100">';
                    },
                    'format' => 'raw',
                ],

                [
                    'attribute' => 'partners_index_bg',
                    'value' => function ($model) {
                        return '<img src="/uploads' . $model->partners_index_bg . '" width="100">';
                    },
                    'format' => 'raw',
                ],

                [
                    'attribute' => 'collection_bg',
                    'value' => function ($model) {
                        return '<img src="/uploads' . $model->collection_bg . '" width="100">';
                    },
                    'format' => 'raw',
                ],

                [
                    'attribute' => 'card_collection_bg',
                    'value' => function ($model) {
                        return '<img src="/uploads' . $model->card_collection_bg . '" width="100">';
                    },
                    'format' => 'raw',
                ],

                [
                    'attribute' => 'card_model_bg',
                    'value' => function ($model) {
                        return '<img src="/uploads' . $model->card_model_bg . '" width="100">';
                    },
                    'format' => 'raw',
                ],

                [
                    'attribute' => 'catalog_model_bg',
                    'value' => function ($model) {
                        return '<img src="/uploads' . $model->catalog_model_bg . '" width="100">';
                    },
                    'format' => 'raw',
                ],

                [
                    'attribute' => 'all_news_bg',
                    'value' => function ($model) {
                        return '<img src="/uploads' . $model->all_news_bg . '" width="100">';
                    },
                    'format' => 'raw',
                ],

                [
                    'attribute' => 'forum_bg',
                    'value' => function ($model) {
                        return '<img src="/uploads' . $model->forum_bg . '" width="100">';
                    },
                    'format' => 'raw',
                ],

                [
                    'attribute' => 'news_item_bg',
                    'value' => function ($model) {
                        return '<img src="/uploads' . $model->news_item_bg . '" width="100">';
                    },
                    'format' => 'raw',
                ],

                [
                    'attribute' => 'login_bg',
                    'value' => function ($model) {
                        return '<img src="/uploads' . $model->login_bg . '" width="100">';
                    },
                    'format' => 'raw',
                ],

                [
                    'attribute' => 'profile_bg',
                    'value' => function ($model) {
                        return '<img src="/uploads' . $model->profile_bg . '" width="100">';
                    },
                    'format' => 'raw',
                ],

                [
                    'attribute' => 'registration_bg',
                    'value' => function ($model) {
                        return '<img src="/uploads' . $model->registration_bg . '" width="100">';
                    },
                    'format' => 'raw',
                ],

                [
                    'attribute' => 'logo',
                    'value' => function ($model) {
                        return '<img src="/uploads' . $model->logo . '" width="100">';
                    },
                    'format' => 'raw',
                ],

                'email:email',
                'phone',
                'confidential_link',
                'footer_text',
            ],
        ]) ?>
    </div>
</div>

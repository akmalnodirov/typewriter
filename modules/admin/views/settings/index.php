<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-header">
        <div class="row justify-content-between" style="margin: auto">
            <div class="">
                <h3>Настройки</h3>
                <span>Добавление партнера</span>
            </div>
            <p>
                <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
        </div>
    </div>
    <div class="card-block">


        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'logo',
                    'value' => function ($model) {
                        return '<img src="/uploads' . $model->logo . '" width="100">';
                    },
                    'format' => 'raw',
                ],
                'email:email',
                'phone',
                'confidential_link',

                ['class' => 'app\components\oweriddenClasses\CustomColumn', 'template' => '{update}'],
            ],
        ]); ?>


    </div>
</div>

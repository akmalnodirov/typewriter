<?php

use app\components\oweriddenClasses\CustomUpload;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Settings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card">
    <div class="card-block">
        <h2 class="sub-title" style="font-size: 25px;">Редактировать</h2>
        <?php $form = ActiveForm::begin(); ?>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label"></label>
            <div class="col-sm-4">
                <?= $form->field($model, 'email')->textInput(['placeholder' => 'Email'])->label(false) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'phone')->textInput(['placeholder' => 'Телефон'])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label"></label>
            <div class="col-sm-4">
                <?= $form->field($model, 'partners_text')->textarea(['rows' => '10'])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label"></label>
            <div class="col-sm-4">
                <?= $form->field($model, 'confidential_link')->textInput(['placeholder' => 'Линк политика конфидециальности'])->label(false) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'footer_text')->textInput(['placeholder' => 'Произведено в :'])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label"></label>
            <div class="col-sm-4">
                <?= $form->field($model, 'facebook')->textInput(['placeholder' => 'Линк фейсбук'])->label(false) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'instagram')->textInput(['placeholder' => 'Линк инстаграм'])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Лого сайта</label>
            <div class="col-sm-4">
                <?= $form->field($model, 'logo_dynamic')->widget(CustomUpload::classname(), [
                    'url' => ['/file-storage/upload'],
                    'maxFileSize' => 2000000, // 5 MiB
                ])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Аватарка форума</label>
            <div class="col-sm-4">
                <?= $form->field($model, 'avatar_dynamic')->widget(CustomUpload::classname(), [
                    'url' => ['/file-storage/upload'],
                    'maxFileSize' => 2000000, // 5 MiB
                ])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Бэкгорунд бранды</label>
            <div class="col-sm-4">
                <?= $form->field($model, 'brands_index_bg_dynamic')->widget(CustomUpload::classname(), [
                    'url' => ['/file-storage/upload'],
                    'maxFileSize' => 2000000, // 5 MiB
                ])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Бэкгорунд каталог</label>
            <div class="col-sm-4">
                <?= $form->field($model, 'catalog_museum_index_bg_dynamic')->widget(CustomUpload::classname(), [
                    'url' => ['/file-storage/upload'],
                    'maxFileSize' => 2000000, // 5 MiB
                ])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Бэкгорунд новости</label>
            <div class="col-sm-4">
                <?= $form->field($model, 'news_index_bg_dynamic')->widget(CustomUpload::classname(), [
                    'url' => ['/file-storage/upload'],
                    'maxFileSize' => 2000000, // 5 MiB
                ])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Бэкгорунд партнеры</label>
            <div class="col-sm-4">
                <?= $form->field($model, 'partners_index_bg_dynamic')->widget(CustomUpload::classname(), [
                    'url' => ['/file-storage/upload'],
                    'maxFileSize' => 2000000, // 5 MiB
                ])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Бэкгорунд коллекци</label>
            <div class="col-sm-4">
                <?= $form->field($model, 'collection_bg_dynamic')->widget(CustomUpload::classname(), [
                    'url' => ['/file-storage/upload'],
                    'maxFileSize' => 2000000, // 5 MiB
                ])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Бэкгорунд коллекци внутр.</label>
            <div class="col-sm-4">
                <?= $form->field($model, 'card_collection_bg_dynamic')->widget(CustomUpload::classname(), [
                    'url' => ['/file-storage/upload'],
                    'maxFileSize' => 2000000, // 5 MiB
                ])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Бэкгорунд модель коллекци.</label>
            <div class="col-sm-4">
                <?= $form->field($model, 'card_model_bg_dynamic')->widget(CustomUpload::classname(), [
                    'url' => ['/file-storage/upload'],
                    'maxFileSize' => 2000000, // 5 MiB
                ])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Бэкгорунд каталог коллекци.</label>
            <div class="col-sm-4">
                <?= $form->field($model, 'catalog_model_bg_dynamic')->widget(CustomUpload::classname(), [
                    'url' => ['/file-storage/upload'],
                    'maxFileSize' => 2000000, // 5 MiB
                ])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Бэкгорунд все новости.</label>
            <div class="col-sm-4">
                <?= $form->field($model, 'all_news_bg_dynamic')->widget(CustomUpload::classname(), [
                    'url' => ['/file-storage/upload'],
                    'maxFileSize' => 2000000, // 5 MiB
                ])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Бэкгорунд форума.</label>
            <div class="col-sm-4">
                <?= $form->field($model, 'forum_bg_dynamic')->widget(CustomUpload::classname(), [
                    'url' => ['/file-storage/upload'],
                    'maxFileSize' => 2000000, // 5 MiB
                ])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Бэкгорунд логин пейдж.</label>
            <div class="col-sm-4">
                <?= $form->field($model, 'login_bg_dynamic')->widget(CustomUpload::classname(), [
                    'url' => ['/file-storage/upload'],
                    'maxFileSize' => 2000000, // 5 MiB
                ])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Бэкгорунд один новость.</label>
            <div class="col-sm-4">
                <?= $form->field($model, 'news_item_bg_dynamic')->widget(CustomUpload::classname(), [
                    'url' => ['/file-storage/upload'],
                    'maxFileSize' => 2000000, // 5 MiB
                ])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Бэкгорунд профиль пейдж.</label>
            <div class="col-sm-4">
                <?= $form->field($model, 'profile_bg_dynamic')->widget(CustomUpload::classname(), [
                    'url' => ['/file-storage/upload'],
                    'maxFileSize' => 2000000, // 5 MiB
                ])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Бэкгорунд регистрация пейдж.</label>
            <div class="col-sm-4">
                <?= $form->field($model, 'registration_bg_dynamic')->widget(CustomUpload::classname(), [
                    'url' => ['/file-storage/upload'],
                    'maxFileSize' => 2000000, // 5 MiB
                ])->label(false) ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

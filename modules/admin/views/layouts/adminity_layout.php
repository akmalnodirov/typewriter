<?php

use yii\helpers\Html;
use app\assets\AdmintyAsset;
use yii\helpers\Url;
use app\components\Helpers;

AdmintyAsset::register($this); // AdminAsset

?>


<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="description" content="#">
    <meta name="keywords"
          content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">

    <link rel="icon" href="https://colorlib.com//polygon/adminty/files/assets/images/favicon.ico" type="image/x-icon">
    <title>Beagle</title>

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>

<style>
    .activeMenu{
        color: #fe8a7d !important;
    }
</style>

<div class="theme-loader">
    <div class="ball-scale">
        <div class='contain'>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
        </div>
    </div>
</div>

<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">
        <nav class="navbar header-navbar pcoded-header">
            <div class="navbar-wrapper">
                <div class="navbar-logo">
                    <a class="mobile-menu" id="mobile-collapse" href="#!">
                        <i class="feather icon-menu"></i>
                    </a>
                    <a href="index.html">
                        <img class="img-fluid" src="/adminty/images/logo.png" alt="Theme-Logo"/>
                    </a>
                    <a class="mobile-options">
                        <i class="feather icon-more-horizontal"></i>
                    </a>
                </div>
                <div class="navbar-container container-fluid">
                    <ul class="nav-left">
                        <li class="header-search">
                            <div class="main-search morphsearch-search">
                                <div class="input-group">
                                    <span class="input-group-addon search-close"><i class="feather icon-x"></i></span>
                                    <input type="text" class="form-control" style="width: 200px;height: 30px;">
                                    <span class="input-group-addon search-btn"><i
                                                class="feather icon-search"></i></span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a href="#!" onclick="javascript:toggleFullScreen()">
                                <i class="feather icon-maximize full-screen"></i>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-right">

                        <li class="user-profile header-notification">
                            <div class="dropdown-primary dropdown show">
                                <div class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                    <img src="/adminty/images/avatar-4.jpg" class="img-radius" alt="User-Profile-Image">
                                    <span><?php echo !Yii::$app->user->isGuest ? Yii::$app->user->identity->username : "Someone"?></span>
                                    <i class="feather icon-chevron-down"></i>
                                </div>
                                <ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                    <li>
                                        <a href="<?=Url::to(['/admin/default/logout'])?>">
                                            <i class="feather icon-settings"></i> Logout
                                        </a>
                                    </li>
                                    <li>
                                        <a href="user-profile.html">
                                            <i class="feather icon-user"></i> Profile
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="pcoded-main-container" style="">
            <div class="pcoded-wrapper">
                <nav class="pcoded-navbar">
                    <div class="pcoded-inner-navbar main-menu">
                        <div class="pcoded-navigatio-lavel">Основные</div>
                        <ul class="pcoded-item pcoded-left-item">

                            <?php $main_page = ['index-page-slider', 'brand-slider', 'partners'];?>
                            <?php $collection_page = ['collection', 'collection-files', 'collection-model'];?>
                            <?php $forum_page = ['forum-themes', 'collection-forum'];?>
                            <?php $settings_page = ['menu', 'languages', 'source-message', 'settings'];?>
                            <?php $typic_pages = ['page-types', 'standard-pages', 'confidency-table', 'personal-agreement'];?>


                            <li  class="<?= Helpers::activeController('гыук') ?>">
                                <a href="<?=Url::to(['user/index'])?>">
                                    <span class="pcoded-micon"><i class="feather icon-users"></i></span>
                                    <span class="pcoded-mtext">Пользователи</span>
                                </a>
                            </li>

                            <li class="pcoded-hasmenu <?=Helpers::activeArrayMenu($main_page)?>">
                                <a href="javascript:void(0)">
                                    <span class="pcoded-micon"><i class="feather icon-layers"></i></span>
                                    <span class="pcoded-mtext">Главная страница</span>
                                </a>
                                <ul class="pcoded-submenu">
                                    <li class="<?= Helpers::activeController('brand-slider') ?>">
                                        <a href="<?= Url::to(['brand-slider/index']) ?>">
                                            <span class="pcoded-mtext">Бранд слайдер</span>
                                        </a>
                                    </li>
                                    <li class="<?= Helpers::activeController('partners') ?>">
                                        <a href="<?=Url::to(['partners/index'])?>">
                                            <span class="pcoded-micon"><i class="feather icon-users"></i></span>
                                            <span class="pcoded-mtext">Партнеры</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="pcoded-hasmenu <?=Helpers::activeArrayMenu($collection_page)?>">
                                <a href="javascript:void(0)">
                                    <span class="pcoded-micon"><i class="feather icon-check-circle"></i></span>
                                    <span class="pcoded-mtext">Коллекции</span>
                                </a>
                                <ul class="pcoded-submenu">

                                    <li class="<?= Helpers::activeController('collection') ?>">
                                        <a href="<?= Url::to(['collection/index']) ?>">
                                            <span class="pcoded-mtext">Коллекции</span>
                                        </a>
                                    </li>

                                    <li class="<?= Helpers::activeController('collection-files') ?>">
                                        <a href="<?= Url::to(['collection-files/index']) ?>">
                                            <span class="pcoded-mtext">Файли коллекции</span>
                                        </a>
                                    </li>

                                    <li class="<?= Helpers::activeController('collection-model') ?>">
                                        <a href="<?= Url::to(['collection-model/index']) ?>">
                                            <span class="pcoded-mtext">Модели коллекции</span>
                                        </a>
                                    </li>

                                </ul>
                            </li>

                            <li  class="<?= Helpers::activeController('news') ?>">
                                <a href="<?=Url::to(['news/index'])?>">
                                    <span class="pcoded-micon"><i class="feather icon-paperclip"></i></span>
                                    <span class="pcoded-mtext">Новости</span>
                                </a>
                            </li>

                            <li class="pcoded-hasmenu <?=Helpers::activeArrayMenu($forum_page)?>">
                                <a href="javascript:void(0)">
                                    <span class="pcoded-micon"><i class="feather icon-file-text"></i></span>
                                    <span class="pcoded-mtext">Форум</span>
                                </a>
                                <ul class="pcoded-submenu">
                                    <li class="<?= Helpers::activeController('forum-themes') ?>">
                                        <a href="<?= Url::to(['forum-themes/index']) ?>">
                                            <span class="pcoded-mtext">Тема форума</span>
                                        </a>
                                    </li>
                                    <li class="<?= Helpers::activeController('collection-forum') ?>">
                                        <a href="<?= Url::to(['collection-forum/index']) ?>">
                                            <span class="pcoded-mtext">Переписки форума</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="pcoded-hasmenu <?=Helpers::activeArrayMenu($typic_pages)?>">
                                <a href="javascript:void(0)">
                                    <span class="pcoded-micon"><i class="feather icon-file-text"></i></span>
                                    <span class="pcoded-mtext">Типовые страницы</span>
                                </a>
                                <ul class="pcoded-submenu">
                                    <li class="<?= Helpers::activeController('page-types') ?>">
                                        <a href="<?= Url::to(['page-types/index']) ?>">
                                            <span class="pcoded-mtext">Типы страницы</span>
                                        </a>
                                    </li>
                                    <li class="<?= Helpers::activeController('standard-pages') ?>">
                                        <a href="<?= Url::to(['standard-pages/index']) ?>">
                                            <span class="pcoded-mtext">Страницы</span>
                                        </a>
                                    </li>
                                    <li class="<?= Helpers::activeController('confidency-table') ?>">
                                        <a href="<?= Url::to(['confidency-table/index']) ?>">
                                            <span class="pcoded-mtext">Политика конфид.</span>
                                        </a>
                                    </li>
                                    <li class="<?= Helpers::activeController('personal-agreement') ?>">
                                        <a href="<?= Url::to(['personal-agreement/index']) ?>">
                                            <span class="pcoded-mtext">Согласие перс.</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="pcoded-hasmenu <?=Helpers::activeArrayMenu($settings_page)?>">
                                <a href="javascript:void(0)">
                                    <span class="pcoded-micon"><i class="feather icon-settings"></i></span>
                                    <span class="pcoded-mtext">Настройки</span>
                                </a>
                                <ul class="pcoded-submenu">
                                    <li class="<?= Helpers::activeController('menu') ?>">
                                        <a href="<?= Url::to(['menu/index']) ?>">
                                            <span class="pcoded-mtext">Меню</span>
                                        </a>
                                    </li>
                                    <li class="<?= Helpers::activeController('languages') ?>">
                                        <a href="<?= Url::to(['languages/index']) ?>">
                                            <span class="pcoded-mtext">Языки</span>
                                        </a>
                                    </li>
                                    <li class="<?= Helpers::activeController('source-message') ?>">
                                        <a href="<?= Url::to(['source-message/index']) ?>">
                                            <span class="pcoded-mtext">Переводы</span>
                                        </a>
                                    </li>
                                    <li class="<?= Helpers::activeController('settings') ?>">
                                        <a href="<?= Url::to(['settings/index']) ?>">
                                            <span class="pcoded-mtext">Системы</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li  class="<?= Helpers::activeController('comments') ?>">
                                <a href="<?=Url::to(['comments/index'])?>">
                                    <span class="pcoded-micon"><i class="feather icon-compass"></i></span>
                                    <span class="pcoded-mtext">ОБратная связь</span>
                                </a>
                            </li>


                        </ul>
                    </div>
                </nav>
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <div class="main-body">
                            <div class="page-wrapper">
                                <?= $content ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<?php $this->endBody() ?>


</body>
</html>
<?php $this->endPage() ?>

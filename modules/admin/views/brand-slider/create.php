<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BrandSlider */

$this->title = 'Create Brand Slider';
$this->params['breadcrumbs'][] = ['label' => 'Brand Sliders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brand-slider-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

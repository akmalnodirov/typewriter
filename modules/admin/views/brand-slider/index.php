<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Brand Sliders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-header">
        <div class="row justify-content-between" style="margin: auto">
            <div class="">
                <h3>Бренд слайдеры</h3>
                <span>Добавление нового языка</span>
            </div>
            <p>
                <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
        </div>
    </div>
    <div class="card-block">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'first_title',
                'second_title',
                [
                    'contentOptions' => ['style' => 'text-align: center; white-space: normal'],
                    'attribute' => 'description',
                    'label' => 'Описание',
                    'value' => function ($model) {
                        return $model->description;
                    }
                ],

                ['class' => 'app\components\oweriddenClasses\CustomColumn', 'template' => '{update}{delete}'],
            ],
        ]); ?>
    </div>
</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BrandSlider */

$this->title = 'Update Brand Slider: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Brand Sliders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="brand-slider-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Menu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card">
    <div class="card-block">
        <h2 class="sub-title" style="font-size: 25px;">Редактировать</h2>
        <?php $form = ActiveForm::begin(); ?>

        <div class="form-group row">
            <label class="col-sm-3 col-form-label">Название меню</label>
            <div class="col-sm-7">
                <?= $form->field($model, 'name')->textInput()->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-3 col-form-label">Тип страницы</label>
            <div class="col-sm-7">
                <?= $form->field($model, 'page_type')
                    ->dropDownList([1 => 'Стандартные страницы', 2 => 'Динамические страницы'],
                        [
                            'prompt' => 'Выберите тип страницы'
                        ])
                    ->label(false) ?>
            </div>
        </div>

        <div class="form-group row" style="display: none" id="standard_page_div">
            <label class="col-sm-3 col-form-label">Стандартные страницы</label>
            <div class="col-sm-7">
                <?= $form->field($model, 'link')->dropDownList($model->getPages(), [
                    'prompt' => 'Выберите стандартниые страницы',
                ])->label(false) ?>
            </div>
        </div>

        <div class="form-group row" style="display: none" id="dynamic_page_div">
            <label class="col-sm-3 col-form-label">Динамические страницы</label>
            <div class="col-sm-7">
                <?= $form->field($model, 'page_id')->dropDownList($model->dynamicPages(), [
                    'prompt' => 'Выберите стандартниые страницы',
                ])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-3 col-form-label">Ордер</label>
            <div class="col-sm-2">
                <?= $form->field($model, 'order')->textInput()->label(false) ?>
            </div>
            <label class="col-sm-1 col-form-label">Статус</label>
            <div class="col-sm-4">
                <?= $form->field($model, 'order')->dropDownList([1 => 'Актив', 0 => 'Отключен'])->label(false) ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

<?php $this->registerJs("

    $(function(){
    
        $('body').on('change', '#menu-page_type', function(){
            
            $('#standard_page_div').css('display', 'none');
            $('#dynamic_page_div').css('display', 'none');
           
            let value = $(this).val();
            
            if(value == 1) {
                $('#standard_page_div').css('display', 'flex');
            }
            
            if(value == 2) {
                $('#dynamic_page_div').css('display', 'flex');
            }
            
        })
    })

")?>

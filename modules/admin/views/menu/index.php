<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Menus');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-header">
        <div class="row justify-content-between" style="margin: auto">
            <div class="">
                <h3>Меню</h3>
                <span>Добавление меню</span>
            </div>
            <p>
                <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
        </div>
    </div>
    <div class="card-block">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'contentOptions' => ['style' => 'white-space: normal'],
                    'attribute' => 'name',
                    'label' => 'Название',
                    'value' => function ($model) {
                        return $model->name;
                    }
                ],

                [
                    'contentOptions' => ['style' => 'white-space: normal'],
                    'attribute' => 'link',
                    'label' => 'Линк',
                    'value' => function ($model) {
                        return $model->link;
                    }
                ],

                [
                    'contentOptions' => ['style' => 'white-space: normal'],
                    'attribute' => 'page_id',
                    'label' => 'Страница',
                    'value' => function ($model) {
                        return $model->page ? $model->page->name : '';
                    }
                ],
                [
                    'contentOptions' => ['style' => 'white-space: normal'],
                    'attribute' => 'order',
                    'label' => 'Ордер',
                    'value' => function ($model) {
                        return $model->order;
                    }
                ],

                ['class' => 'app\components\oweriddenClasses\CustomColumn', 'template' => '{update}{delete}'],
            ],
        ]); ?>

    </div>
</div>

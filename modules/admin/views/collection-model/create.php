<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CollectionModels */

$this->title = Yii::t('app', 'Create Collection Models');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Collection Models'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="collection-models-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

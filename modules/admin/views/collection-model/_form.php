<?php

use app\components\oweriddenClasses\CustomUpload;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use unclead\multipleinput\MultipleInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CollectionModels */
/* @var $form yii\widgets\ActiveForm */

?>


<div class="card">
    <div class="card-block">
        <h2 class="sub-title" style="font-size: 25px;">Редактировать</h2>
        <?php $form = ActiveForm::begin(); ?>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Коллекция</label>
            <div class="col-sm-8">
                <?= $form->field($model, 'collection_id')->dropDownList(ArrayHelper::map(\app\models\Collection::find()->all(), 'id', 'name'))->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label"></label>
            <div class="col-sm-4">
                <?= $form->field($model, 'name')->textInput(['placeholder' => 'Название'])->label(false) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'old')->textInput(['placeholder' => 'Индикатор'])->label(false) ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Фильтр символ</label>
            <div class="col-sm-2">
                <?= $form->field($model, 'start_symbol')->dropDownList([
                    'A' => 'A',
                    'B' => 'B',
                    'C' => 'C',
                    'D' => 'D',
                    'E' => 'E',
                    'F' => 'F',
                    'G' => 'G',
                    'H' => 'H',
                    'I' => 'I',
                    'J' => 'J',
                    'K' => 'K',
                    'L' => 'L',
                    'M' => 'M',
                    'N' => 'N',
                    'O' => 'O',
                    'P' => 'P',
                    'Q' => 'Q',
                    'R' => 'R',
                    'S' => 'S',
                    'T' => 'T',
                    'U' => 'U',
                    'V' => 'V',
                    'W' => 'W',
                    'X' => 'X',
                    'Y' => 'Y',
                    'Z' => 'Z'
                ])->label(false) ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'hashtag')->textInput(['placeholder' => 'Теги'])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Основное фото</label>
            <div class="col-sm-4">
                <?= $form->field($model, 'main_image')->widget(CustomUpload::classname(), [
                    'url' => ['/file-storage/upload'],
                    'maxFileSize' => 2000000, // 5 MiB
                ])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Описание</label>
            <div class="col-sm-8">

                <?= $form->field($model, 'description')->widget(CKEditor::className(), [
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                        'preset' => 'full',
                        'height' => '400px'
                    ]),

                ])->label(false); ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Фотографии</label>
            <div class="col-sm-4">
                <?php echo $form->field($model, 'model_image')->widget(
                    CustomUpload::className(),
                    [
                        'url' => ['/file-storage/upload'],
                        'sortable' => true,
                        'maxFileSize' => 10000000, // 10 MiB
                        'maxNumberOfFiles' => 10
                    ])->label(false);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Характеристики</label>
            <div class="col-sm-8">
                <?= $form->field($model, 'characters')->widget(MultipleInput::className(), [
                    'max' => 20,
                    'theme' => 'bs',
                    'prepend' => true,
                    'rendererClass' => \unclead\multipleinput\renderers\ListRenderer::className(),
                    'addButtonOptions' => [
                        'class' => 'btn btn-success',
                        'label' => '<i class="fa fa-plus"></i>' // also you can use html code
                    ],
                    'removeButtonOptions' => [
                        'class' => 'btn btn-danger',
                        'label' => '<i class="fa fa-minus"></i>'
                    ],
                    'layoutConfig' => [
                        'offsetClass' => 'col-md-offset-2',
                        'labelClass' => 'col-md-4',
                        'wrapperClass' => 'col-md-11',
                    ],
                    'columns' => [
                        [
                            'name' => 'key',
                            'title' => 'Название характеристики',
                        ],
                         [
                            'name' => 'hashtag',
                            'title' => 'Описание характеристики',
                        ],
                        [
                            'name' => 'value',
                            'title' => 'Описание характеристики',
                        ]
                    ]
                ])->label(false); ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

    </div>


    <?php ActiveForm::end(); ?>

</div>
</div>

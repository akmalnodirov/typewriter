<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PersonalAgreement */

$this->title = Yii::t('app', 'Create Personal Agreement');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Personal Agreements'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personal-agreement-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PoliticalConfidency */

$this->title = Yii::t('app', 'Update Political Confidency: {name}', [
    'name' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Political Confidencies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="political-confidency-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

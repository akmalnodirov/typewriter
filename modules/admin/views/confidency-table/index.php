<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Political Confidencies');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-header">
        <div class="row justify-content-between" style="margin: auto">
            <div class="">
                <h3>Политика конфиденциальности</h3>
                <span>Политика конфиденциальности</span>
            </div>
            <p>
                <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
        </div>
    </div>
    <div class="card-block">


        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'contentOptions' => ['style' => 'white-space: normal'],
                    'attribute' => 'title',
                    'label' => 'Название',
                    'value' => function ($model) {
                        return $model->title;
                    }
                ],
                [
                    'contentOptions' => ['style' => 'white-space: normal'],
                    'attribute' => 'text',
                    'label' => 'Описание',
                    'value' => function ($model) {
                        return mb_substr($model->text, 0, 255);
                    },
                    'format' => 'html'
                ],
                [
                    'attribute' => 'image',
                    'value' => function ($model) {
                        return '<img src="/uploads' . $model->image . '" width="100">';
                    },
                    'format' => 'raw',
                ],

                ['class' => 'app\components\oweriddenClasses\CustomColumn', 'template' => '{update}'],
            ],
        ]); ?>

    </div>
</div>

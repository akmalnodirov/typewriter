<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PoliticalConfidency */

$this->title = Yii::t('app', 'Create Political Confidency');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Political Confidencies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="political-confidency-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

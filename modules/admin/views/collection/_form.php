<?php

use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use app\components\oweriddenClasses\CustomUpload;
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model app\models\Collection */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card">
    <div class="card-block">
        <h2 class="sub-title" style="font-size: 25px;">Редактировать</h2>
        <?php $form = ActiveForm::begin(); ?>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Титуль</label>
            <div class="col-sm-8">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Название</label>
            <div class="col-sm-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label(false) ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'start_symbol')->dropDownList([
                    'A' => 'A',
                    'B' => 'B',
                    'C' => 'C',
                    'D' => 'D',
                    'E' => 'E',
                    'F' => 'F',
                    'G' => 'G',
                    'H' => 'H',
                    'I' => 'I',
                    'J' => 'J',
                    'K' => 'K',
                    'L' => 'L',
                    'M' => 'M',
                    'N' => 'N',
                    'O' => 'O',
                    'P' => 'P',
                    'Q' => 'Q',
                    'R' => 'R',
                    'S' => 'S',
                    'T' => 'T',
                    'U' => 'U',
                    'V' => 'V',
                    'W' => 'W',
                    'X' => 'X',
                    'Y' => 'Y',
                    'Z' => 'Z'
                ])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Тег</label>
            <div class="col-sm-8">
                <?= $form->field($model, 'hashtag')->textInput(['maxlength' => true])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">История</label>
            <div class="col-sm-8">

                <?= $form->field($model, 'history')->widget(CKEditor::className(), [
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                        'preset' => 'basic',
                        'height' => '200px'
                    ]),

                ])->label(false); ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Лого</label>
            <div class="col-sm-4">
                <?= $form->field($model, 'logo_dynamic')->widget(CustomUpload::classname(), [
                    'url' => ['/file-storage/upload'],
                    'maxFileSize' => 2000000, // 5 MiB
                ])->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">События</label>
            <div class="col-sm-8">
                <?= $form->field($model, 'events')->widget(MultipleInput::className(), [
                    'max' => 20,
                    'theme' => 'bs',
                    'prepend' => true,
                    'rendererClass' => \unclead\multipleinput\renderers\ListRenderer::className(),
                    'addButtonOptions' => [
                        'class' => 'btn btn-success',
                        'label' => '<i class="fa fa-plus"></i>' // also you can use html code
                    ],
                    'removeButtonOptions' => [
                        'class' => 'btn btn-danger',
                        'label' => '<i class="fa fa-minus"></i>'
                    ],
                    'layoutConfig' => [
                        'offsetClass' => 'col-md-offset-2',
                        'labelClass' => 'col-md-4',
                        'wrapperClass' => 'col-md-11',
                    ],
                    'columns' => [
                        [
                            'name' => 'time',
                            //'type' => DateTimePicker::className(),
                            'title' => 'Дата события',
                            'defaultValue' => date('d-m-Y h:i'),
                        ],
                        [
                            'name' => 'hashtag',
                            //'type' => DateTimePicker::className(),
                            'title' => 'Теги',
                        ],
                        [
                            'name' => 'text',
                            'title' => 'Описание',
                            'type' => 'textarea',
                            'enableError' => true,
                            'options' => [
                                'type' => 'textarea',
                                'class' => 'form-control',
                                'rows' => 6,
                            ]
                        ]
                    ]
                ])->label(false); ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Фотографии коллекция</label>
            <div class="col-sm-4">
                <?php echo $form->field($model, 'collection_images')->widget(
                    CustomUpload::className(),
                    [
                        'url' => ['/file-storage/upload'],
                        'sortable' => true,
                        'maxFileSize' => 10000000, // 10 MiB
                        'maxNumberOfFiles' => 10
                    ])->label(false);
                ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Дополнительные фотографии</label>
            <div class="col-sm-4">
                <?php echo $form->field($model, 'collection_details')->widget(
                    CustomUpload::className(),
                    [
                        'url' => ['/file-storage/upload'],
                        'sortable' => true,
                        'maxFileSize' => 10000000, // 10 MiB
                        'maxNumberOfFiles' => 10
                    ])->label(false);
                ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

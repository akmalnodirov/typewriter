<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CollectionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Collections';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-header">
        <div class="row justify-content-between" style="margin: auto">
            <div class="">
                <h3>Коллекции</h3>
                <span>Добавление нового коллекции</span>
            </div>
            <p>
                <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
        </div>
    </div>
    <div class="card-block">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'contentOptions' => ['style' => 'white-space: normal'],
                    'attribute' => 'title',
                    'label' => 'Описание',
                    'value' => function ($model) {
                        return $model->title;
                    }
                ],
                [
                    'contentOptions' => ['style' => 'white-space: normal'],
                    'attribute' => 'name',
                    'label' => 'Описание',
                    'value' => function ($model) {
                        return $model->name;
                    }
                ],
                [
                    'attribute' => 'logo',
                    'value' => function ($model) {
                        return '<img src="/uploads' . $model->logo . '" width="100">';
                    },
                    'format' => 'raw',
                ],

                ['class' => 'app\components\oweriddenClasses\CustomColumn', 'template' => '{update}{delete}'],
            ],
        ]); ?>
    </div>
</div>

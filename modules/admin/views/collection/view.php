<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Collection */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Collections', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="collection-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'contentOptions' => ['style' => 'white-space: normal'],
                'attribute' => 'title',
                'label' => 'Описание',
                'value' => function ($model) {
                    return $model->title;
                }
            ],
            [
                'contentOptions' => ['style' => 'white-space: normal'],
                'attribute' => 'name',
                'label' => 'Описание',
                'value' => function ($model) {
                    return $model->name;
                }
            ],
            [
                'attribute' => 'logo',
                'value' => function ($model) {
                    return '<img src="/uploads' . $model->logo . '" width="100">';
                },
                'format' => 'raw',
            ],
            [
                'contentOptions' => ['style' => 'white-space: normal'],
                'attribute' => 'history',
                'label' => 'Описание',
                'value' => function ($model) {
                    return $model->history;
                },
                'format' => 'raw'
            ],
            [
                'contentOptions' => ['style' => 'white-space: normal'],
                'attribute' => 'events',
                'label' => 'Описание',
                'value' => function ($model) {
                    $events = 'Akmal';
                    if($model->events){
                        foreach($model->events as $key=>$events){
                            $events = '<tr><td>Jill</td><td>Smith</td><td>50</td></tr>';
                        }
                    }
                    return $events;
                },
                'format' => 'html'
            ],
        ],
    ]) ?>

</div>

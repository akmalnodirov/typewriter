<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CollectionFilesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Collection Files';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-header">
        <div class="row justify-content-between" style="margin: auto">
            <div class="">
                <h3>Файлы коллекции</h3>
                <span>Добавление нового файла</span>
            </div>
            <p>
                <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
        </div>
    </div>
    <div class="card-block">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'contentOptions' => ['style' => 'white-space: normal'],
                'attribute' => 'collection_id',
                'label' => 'Коллекция',
                'value' => function ($model) {
                    return $model->collection->name;
                }
            ],
            'name',
            ['class' => 'app\components\oweriddenClasses\CustomColumn', 'template' => '{update}{delete}'],
        ],
    ]); ?>


</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CollectionFiles */

$this->title = 'Create Collection Files';
$this->params['breadcrumbs'][] = ['label' => 'Collection Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="collection-files-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CollectionFiles */

$this->title = 'Update Collection Files: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Collection Files', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="collection-files-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

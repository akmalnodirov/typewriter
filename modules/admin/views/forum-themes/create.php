<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ForumThemes */

$this->title = 'Create Forum Themes';
$this->params['breadcrumbs'][] = ['label' => 'Forum Themes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="forum-themes-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ForumThemes */

$this->title = 'Update Forum Themes: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Forum Themes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="forum-themes-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

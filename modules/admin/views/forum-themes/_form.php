<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ForumThemes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card">
    <div class="card-block">
        <h2 class="sub-title" style="font-size: 25px;">Редактировать</h2>
        <?php $form = ActiveForm::begin(); ?>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Коллекция</label>
            <div class="col-sm-7">
                <?= $form->field($model, 'collection_id')->dropDownList(ArrayHelper::map(\app\models\Collection::find()->all(), 'id', 'name'))->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Название темы</label>
            <div class="col-sm-7">
                <?= $form->field($model, 'theme_name')->textInput()->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Теги</label>
            <div class="col-sm-7">
                <?= $form->field($model, 'hashtag')->textInput()->label(false) ?>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Статус</label>
            <div class="col-sm-7">
                <?= $form->field($model, 'status')->dropDownList([ 1=> 'Актив',  2=> 'Отключен'])->label(false) ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

<?php

namespace app\modules\admin\controllers;

use app\models\CollectionModelCharacteristics;
use Yii;
use app\models\CollectionModels;
use app\models\CollectionModelSearch;
use yii\db\Expression;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CollectionModelController implements the CRUD actions for CollectionModels model.
 */
class CollectionModelController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CollectionModels models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CollectionModelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CollectionModels model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CollectionModels model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CollectionModels();
        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $model->update_at = new Expression('NOW()');
                $model->create_at = new Expression('NOW()');
                $model->save();
                if ($model->characters) {
                    foreach ($model->characters as $events) {
                        $collectionEvents = new CollectionModelCharacteristics();
                        $collectionEvents->collection_model_id = $model->id;
                        $collectionEvents->key = $events['key'];
                        $collectionEvents->value = $events['value'];
                        $collectionEvents->save();
                    }
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Collection model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelEvents = CollectionModelCharacteristics::findAll(['collection_model_id' => $id]);

        foreach ($modelEvents as $key => $modelEvent) {
            $model->characters[$key]['key'] = $modelEvent->key;
            $model->characters[$key]['value'] = $modelEvent->value;
        }

        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                CollectionModelCharacteristics::deleteAll(['collection_model_id' => $id]);
                $model->update_at = new Expression('NOW()');
                $model->save();
                if ($model->characters) {
                    foreach ($model->characters as $events) {
                        $collectionEvents = new CollectionModelCharacteristics();
                        $collectionEvents->collection_model_id = $model->id;
                        $collectionEvents->key = $events['key'];
                        $collectionEvents->value = $events['value'];
                        $collectionEvents->save();
                    }
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    /**
     * Deletes an existing CollectionModels model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CollectionModels model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CollectionModels the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CollectionModels::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

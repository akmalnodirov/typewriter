<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

use app\models\Authorization\LoginForm;
use yii\data\ActiveDataProvider;


class DefaultController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // 'only' => ['logout'],
                'only' => ['login', 'index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                    [

                        'allow' => true,
                        'actions' => ['index', 'logout'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->type === 1;
                        }
                    ],
                ],
            ],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex(){

        return $this->render('welcome_admin');
    }

    public function actionLogin(){

        $this->layout = 'adminity_login_layout.php';

        if (!Yii::$app->user->isGuest) {
            return $this->redirect('welcome');
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->loginAdmin()) {
           return $this->goBack();
        }

        $model->password = '';
        return $this->render('adminity_login_page', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {

        Yii::$app->user->logout();

        return $this->redirect('index');
    }


}

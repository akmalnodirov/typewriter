<?php

namespace app\modules\admin;

/**
 * admin module definition class
 */
class Admin extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {

        $this->layout = 'adminity_layout';
        \Yii::$app->user->loginUrl = ['/admin/default/login'];
        parent::init();
        // custom initialization code goes here
    }
}
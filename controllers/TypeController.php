<?php

namespace app\controllers;

use app\models\BrandSlider;
use app\models\Collection;
use app\models\CollectionEvents;
use app\models\CollectionForum;
use app\models\CollectionModelCharacteristics;
use app\models\CollectionModels;
use app\models\Comments;
use app\models\ForumThemes;
use app\models\News;
use app\models\PageTypes;
use app\models\Partners;
use app\models\PersonalAgreement;
use app\models\PoliticalConfidency;
use app\models\StandardPages;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

/**
 * Class TypeController
 * @package app\controllers
 */
class TypeController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $brandSlider = BrandSlider::find()->all();
        $collectionModels = CollectionModels::find()->all();
        $news = News::find()->limit(6)->all();
        $partners = Partners::find()->limit(3)->all();
        return $this->render('index', [
            'brandSlider' => $brandSlider,
            'collectionModels' => $collectionModels,
            'news' => $news,
            'partners' => $partners
        ]);
    }

    /**
     * @return string
     */
    public function actionCatalog()
    {
        return $this->render('catalog');
    }

    /**
     * @return string
     */
    public function actionCatalogPartial()
    {
        $search = Yii::$app->request->get('search');
        $limit = Yii::$app->request->get('limit');
        $catalogs = Collection::find();

        if ($search) {
            $limit = null;
            $catalogs = $catalogs->where(['like', 'LOWER(name)', $search]);
        }

        if ($limit != null && $limit != 0) {
            $catalogs = $catalogs->limit($limit)->all();
        } else {
            $catalogs = $catalogs->all();
        }

        $content = $this->renderPartial('catalog_partial', [
            'catalogs' => $catalogs,
        ]);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $content;
    }

    /**
     * @param $slug
     * @return string
     */
    public function actionOneCollection($slug)
    {
        $collection = Collection::find()->where(['slug' => $slug])->one();
        return $this->render('one-catalog', [
            'collection' => $collection
        ]);
    }

    /**
     * @param $slug
     * @return string
     */
    public function actionCollectionModels($slug)
    {
        $collection = Collection::find()->where(['slug' => $slug])->one();
        return $this->render('collection_models', [
            'collection' => $collection,
        ]);
    }

    /**
     * @return string
     */
    public function actionCollectionModelsPartial()
    {
        $symbol = Yii::$app->request->get('symbol');
        $search = Yii::$app->request->get('search');
        $limit = Yii::$app->request->get('limit');
        $collectionId = Yii::$app->request->get('collection_id');
        $models = CollectionModels::find()->where(['collection_id' => $collectionId]);
        $collection = Collection::findOne(['id' => $collectionId]);

        if ($symbol) {
            $models = $models->andWhere(['start_symbol' => $symbol]);
            $limit = null;
        }

        if ($search) {
            $limit = null;
            $models = $models->andWhere(['like', 'name', $search]);
        }

        if ($limit != null && $limit != 0) {
            $models = $models->limit($limit)->all();
        } else {
            $models = $models->all();
        }

        $content = $this->renderPartial('collection_models_partial', [
            'models' => $models,
        ]);

        $result = [
            'collection' => $collection,
            'content' => $content,
        ];

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $result;
    }

    /**
     * @param $slug
     * @return string
     */
    public function actionCollectionForum($slug = null)
    {
        $collection = Collection::findOne(['slug' => $slug]);
        $forumTheme = ForumThemes::find()->where(['collection_id' => $collection->id])->limit(1)->one();
        $forumComments = CollectionForum::find()
            ->where(['forum_theme_id' => $forumTheme->id])
            ->orderBy(['id' => SORT_DESC]);
        $new_comment = new CollectionForum();

        if ($new_comment->load(Yii::$app->request->post())) {
            $new_comment->save(false);
            $this->redirect(Yii::$app->request->referrer);
        }

        $provider = new ActiveDataProvider([
            'query' => $forumComments,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $comments = $provider->getModels();

        return $this->render('collection_forum', [
            'forumTheme' => $forumTheme,
            'forumComments' => $comments,
            'new_comment' => $new_comment,
            'childValue' => null,
            'provider' => $provider,
        ]);
    }


    /**
     * @return array
     */
    public function actionForumPartial()
    {

        $theme_id = Yii::$app->request->get('theme_id');
        $offset = Yii::$app->request->get('offset');
        $forumComments = CollectionForum::find()
            ->where(['forum_theme_id' => $theme_id])
            ->orderBy(['id' => SORT_DESC])
            ->where(['parent_id' => null])
            ->limit(2)->offset($offset)
            ->all();
        $new_comment = new CollectionForum();

        $content = $this->renderPartial('forum_partial', [
            'theme_id' => $theme_id,
            'forumComments' => $forumComments,
            'new_comment' => $new_comment,
        ]);

        $result = [
            'content' => $content,
            'offset' => $offset + 2,
        ];

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $result;
    }

    /**
     * @param $slug
     * @return string
     */
    public function actionOneModel($slug)
    {
        $model = CollectionModels::find()->where(['slug' => $slug])->one();
        return $this->render('one_model', [
            'model' => $model,
        ]);
    }

    /**
     * @return string
     */
    public function actionNews()
    {
        return $this->render('news');
    }

    /**
     * @return string
     */
    public function actionNewsPartial()
    {

        $all = Yii::$app->request->get('all');
        if ($all) {
            $news = News::find()->orderBy(['id' => SORT_DESC])->all();
        } else {
            $news = News::find()->orderBy(['id' => SORT_DESC])->limit(10)->all();
        }
        $content = $this->renderPartial('news_partial', [
            'news' => $news,
        ]);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $content;
    }

    /**
     * @param $slug
     * @return string
     */
    public function actionOneNews($slug)
    {
        $model = News::findOne(['slug' => $slug]);
        $news = News::find()->all();
        return $this->render('one_news', [
            'model' => $model,
            'news' => $news,
        ]);
    }


    public function actionCommentSave()
    {
        $comment = new Comments();
        if ($comment->load(Yii::$app->request->post()) && $comment->validate()) {
            if ($comment->save()) {
                Yii::$app->session->setFlash('success', "Your message has been sent successfully!");
            }
        }
        $this->redirect(Yii::$app->request->referrer);
    }

    public function actionPoliticalConfident()
    {
        $model = PoliticalConfidency::find()->limit(1)->one();
        return $this->render('political-confident', [
            'model' => $model,
        ]);
    }

    public function actionPersonalAgreement()
    {
        $model = PersonalAgreement::find()->limit(1)->one();
        return $this->render('personal-agreement', [
            'model' => $model,
        ]);
    }


    public function actionStandardPages($page_id)
    {
        $pageType = PageTypes::findOne(['id' => $page_id]);
        $page = StandardPages::find()->where(['page_type_id' => $page_id])->orderBy(['id' => SORT_DESC]);

        if ($pageType->page_type == PageTypes::MULTIPAGE) {
            $pages = $page->all();
            Yii::$app->controller->action->id = 'multipage';
            return $this->render('multi_pages', [
                'pages' => $pages,
                'pageType' => $pageType
            ]);
        } else if ($pageType->page_type == PageTypes::SINGLE_PAGE) {
            $page = $page->one();
            Yii::$app->controller->action->id = 'single_page';
            return $this->render('single_pages', [
                'page' => $page,
                'pageType' => $pageType
            ]);
        }

        return null;
    }

    public function actionSinglePage($slug)
    {
        $page = StandardPages::findOne(['slug' => $slug]);
        return $this->render('single_pages', [
            'page' => $page,
        ]);
    }


    public function actionHashtagSearch()
    {
        $query = null;

        if(Yii::$app->request->post()){
            $query = Yii::$app->request->post('search_value');
        }

        $collectionEvents = CollectionEvents::find()->where(['like', 'hashtag', $query])->all();
        $collections = Collection::find()->where(['like', 'hashtag', $query])->all();
        $collectionModelCharacteristics = CollectionModelCharacteristics::find()->where(['like', 'hashtag', $query])->all();
        $colectionModels = CollectionModels::find()->where(['like', 'hashtag', $query])->all();
        $forumThemes = ForumThemes::find()->where(['like', 'hashtag', $query])->all();
        $news = News::find()->where(['like', 'hashtag', $query])->all();
        $standardPages = StandardPages::find()->where(['like', 'hashtag', $query])->all();
        return $this->render('search_content', [
            'collectionEvents' => $collectionEvents,
            'collections' => $collections,
            'collectionModelCharacteristics' => $collectionModelCharacteristics,
            'colectionModels' => $colectionModels,
            'forumThemes' => $forumThemes,
            'news' => $news,
            'standardPages' => $standardPages,
        ]);
    }
}

<?php

namespace app\controllers;

use app\models\ImageCompression;
use bigbrush\tinypng\TinyPng;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;

/**
 * FileStorageController implements the CRUD actions for FileStorageItem model.
 */
class FileStorageController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'upload-delete' => ['delete']
                ]
            ]
        ];
    }


    public function actions()
    {
        return [
            'upload' => [
                'class' => 'trntv\filekit\actions\UploadAction',
                'deleteRoute' => 'upload-delete',
//                'on afterSave' => function ($event) {
//                    $compression = $this->compressionObject();
//                    \Tinify\setProxy("http://172.17.9.96:3128");
//
//                    if($compression->compression_left_count < 500 && $compression['compression_status'] == 1){
//                        try{
//                            $this->tinyfy(Yii::getAlias('@app') . '/web/uploads' . $event->path);
//                            $this->tinyfyCount();
//                        } catch (\Exception $e){
//                            return $e->getMessage();
//                        }
//                    }
//
//                }
            ],

            'upload-delete' => [
                'class' => 'trntv\filekit\actions\DeleteAction'
            ],

            'upload-imperavi' => [
                'class' => 'trntv\filekit\actions\UploadAction',
                'fileparam' => 'file',
                'responseUrlParam' => 'filelink',
                'multiple' => false,
                'disableCsrf' => true
            ],

            'view' => [
                'class' => 'trntv\filekit\actions\ViewAction',
            ],

        ];
    }

    public function tinyfy($file)
    {

        $tiny = new TinyPng(['apiKey' => 'wx9f7sq572Vqg3LMnfY8Ls8QjNlPwjYg']);
        $tiny->compress($file);

    }

    public function tinyfyCount()
    {
        $tiny = new TinyPng(['apiKey' => 'wx9f7sq572Vqg3LMnfY8Ls8QjNlPwjYg']);
        return $this->compressionCountSave($tiny->usage());
    }


    public function compressionObject(){

        return ImageCompression::find()->limit(1)->one();
    }

    public function compressionCountSave($left_count){

        $object = $this->compressionObject();
        $object->compression_left_count = 500 - (int)$left_count;
        $object->all_compressed_image_count = $object->all_compressed_image_count + 1;
        return $object->save(false);

    }
}

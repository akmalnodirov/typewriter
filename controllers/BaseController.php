<?php

namespace app\controllers;

use app\models\Comments;
use app\models\Languages;
use app\models\Menu;
use app\models\Settings;
use Yii;
use yii\web\Controller;


class BaseController extends Controller
{

    public function init()
    {
        $this->view->params['menu'] = Menu::find()->where(['status' => 1])->orderBy(['order' => 'SORT_DESC'])->all();
        $this->view->params['settings'] = Settings::find()->limit(1)->one();
        $this->view->params['comment'] = new Comments();
        $this->view->params['languages'] = Languages::find()->all();

        if (!empty(Yii::$app->request->cookies['language'])) {
            Yii::$app->language = Yii::$app->request->cookies['language']->value;
        } else {
            Yii::$app->language = 'ru';
        }
        parent::init();
    }

    public function actionSetLanguage($language)
    {
        Yii::$app->language = $language;
        $cookies = Yii::$app->response->cookies;
        $cookies->add(new \yii\web\Cookie([
            'name' => 'language',
            'value' => $language,
        ]));

        return $this->redirect(Yii::$app->request->referrer);
    }

}
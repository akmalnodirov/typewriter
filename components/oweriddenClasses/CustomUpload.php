<?php

namespace app\components\oweriddenClasses;

use app\assets\CustomUploadAsset;
use yii\helpers\Json;
use yii\jui\JuiAsset;

/**
 * Class Upload
 * @package trntv\filekit\widget
 */
class CustomUpload extends \trntv\filekit\widget\Upload
{

    public function registerClientScript()
    {
        CustomUploadAsset::register($this->getView());
        $options = Json::encode($this->clientOptions);
        if ($this->sortable) {
            JuiAsset::register($this->getView());
        }
        $this->getView()->registerJs("jQuery('#{$this->getId()}').yiiUploadKit({$options});");
    }
}

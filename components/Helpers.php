<?php

namespace app\components;

use Yii;
use yii\helpers\Url;

class Helpers {

    public static function activeController($controller)
    {


        if(Yii::$app->controller->id == $controller){

            return 'active';
        }

        return false;
    }

    public static function activeAction($action)
    {
        if(Yii::$app->controller->action->id == $action){
            return 'active';
        }

        return false;
    }

    public static function convertPathToUrl($path){
        return str_replace('\\', '/', $path);
    }

    public static function url($link){
        $url = '#'.$link;

        if(Yii::$app->controller->action->id != 'index'){
            $url = Url::to(['/muhtasham/index', '#' => $link]);
        }

        if(Yii::$app->controller->action->id != 'index' && $link == 'section_news'){
            $url = Url::to(['/muhtasham/all-news']);
        }

        if(Yii::$app->controller->action->id != 'index' && $link == 'section_about'){
            $url = Url::to(['/muhtasham/about']);
        }

        return $url;

    }

    public static function activeMenu($action, $second=null){
        $class = '';
        if(Yii::$app->controller->action->id == $action || Yii::$app->controller->action->id == $second){
            $class = 'activeMenu';
        }

        return $class;
    }

    public static function activeArrayMenu($array){
        $class = '';

        if(in_array(Yii::$app->controller->id, $array)){
            $class = 'pcoded-trigger';
        }

        return $class;
    }

    public static function activeAdminMenu($controller){
        $class = '';
        if(Yii::$app->controller->id == $controller){
            $class = 'activeMenu';
        }
        return $class;
    }

}
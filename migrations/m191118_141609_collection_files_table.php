<?php

use yii\db\Migration;

/**
 * Class m191118_141609_collection_files_table
 */
class m191118_141609_collection_files_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%collection_files}}', [
            'id' => $this->primaryKey(),
            'collection_id' => $this->integer(),
            'file' => $this->string()->null(),
            'name' => $this->string(),
        ]);

        $this->createIndex('idx-file-collection_id','collection_files','collection_id');
        $this->addForeignKey('fk-file-collection_id','collection_files','collection_id','collection','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-file-collection_id','collection_files');
        $this->dropIndex('idx-file-collection_id','collection_files');
        $this->dropTable('{{%collection_files}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191118_141609_collection_files_table cannot be reverted.\n";

        return false;
    }
    */
}

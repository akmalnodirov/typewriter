<?php

use yii\db\Migration;

/**
 * Class m191123_151823_alter_table_menu
 */
class m191123_151823_alter_table_menu extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('menu', 'page_type', 'integer');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('menu', 'page_type');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191123_151823_alter_table_menu cannot be reverted.\n";

        return false;
    }
    */
}

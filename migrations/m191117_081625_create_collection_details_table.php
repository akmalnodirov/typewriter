<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%collection_details}}`.
 */
class m191117_081625_create_collection_details_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%collection_details}}', [
            'id' => $this->primaryKey(),
            'collection_id' => $this->integer(),
            'image' => $this->string()->null(),
        ]);

        $this->createIndex('idx-detail-collection_id','collection_details','collection_id');
        $this->addForeignKey('fk-detail-collection_id','collection_details','collection_id','collection','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-detail-collection_id','collection_details');
        $this->dropIndex('idx-detail-collection_id','collection_details');
        $this->dropTable('{{%collection_details}}');
    }
}

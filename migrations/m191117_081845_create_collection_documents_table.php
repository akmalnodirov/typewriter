<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%collection_documents}}`.
 */
class m191117_081845_create_collection_documents_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%collection_documents}}', [
            'id' => $this->primaryKey(),
            'collection_id' => $this->integer(),
            'file' => $this->string()->null(),
        ]);

        $this->createIndex('idx-document-collection_id','collection_documents','collection_id');
        $this->addForeignKey('fk-document-collection_id','collection_documents','collection_id','collection','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-document-collection_id','collection_documents');
        $this->dropIndex('idx-document-collection_id','collection_documents');
        $this->dropTable('{{%collection_documents}}');
    }
}

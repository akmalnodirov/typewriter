<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%settings}}`.
 */
class m191121_051858_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%settings}}', [
            'id' => $this->primaryKey(),
            'brands_index_bg' => $this->string()->null(),
            'catalog_museum_index_bg' => $this->string()->null(),
            'news_index_bg' => $this->string()->null(),
            'partners_index_bg' => $this->string()->null(),
            'collection_bg' => $this->string()->null(),
            'card_collection_bg' => $this->string()->null(),
            'card_model_bg' => $this->string()->null(),
            'catalog_model_bg' => $this->string()->null(),
            'all_news_bg' => $this->string()->null(),
            'forum_bg' => $this->string()->null(),
            'news_item_bg' => $this->string()->null(),
            'login_bg' => $this->string()->null(),
            'profile_bg' => $this->string()->null(),
            'registration_bg' => $this->string()->null(),
            'logo' => $this->string()->null(),
            'email' => $this->string()->notNull(),
            'phone' => $this->string()->notNull(),
            'confidential_link' => $this->string(),
            'footer_text' => $this->string(),
            'partners_text' => $this->string(),
            'facebook' => $this->string(),
            'instagram' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%settings}}');
    }
}

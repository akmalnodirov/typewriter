<?php

use yii\db\Migration;

/**
 * Class m191118_173832_alter_collection_table
 */
class m191118_173832_alter_collection_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('collection', 'start_symbol', 'string');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('collection', 'start_symbol');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191118_173832_alter_collection_table cannot be reverted.\n";

        return false;
    }
    */
}

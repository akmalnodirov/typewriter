<?php

use yii\db\Migration;

/**
 * Class m191123_175803_alter_table_user
 */
class m191123_175803_alter_table_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'agreement', 'integer');
        $this->addColumn('user', 'phone', 'string');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'agreement');
        $this->dropColumn('user', 'phone');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191123_175803_alter_table_user cannot be reverted.\n";

        return false;
    }
    */
}

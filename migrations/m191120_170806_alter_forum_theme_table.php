<?php

use yii\db\Migration;

/**
 * Class m191120_170806_alter_forum_theme_table
 */
class m191120_170806_alter_forum_theme_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('forum_themes', 'created_at', 'integer');
        $this->addColumn('forum_themes', 'updated_at', 'integer');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('forum_themes', 'created_at');
        $this->dropColumn('forum_themes', 'updated_at');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191120_170806_alter_forum_theme_table cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%political_confidency}}`.
 */
class m191123_160739_create_political_confidency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%political_confidency}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'text' => $this->text(),
            'image' => $this->string(),
            'slug' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%political_confidency}}');
    }
}

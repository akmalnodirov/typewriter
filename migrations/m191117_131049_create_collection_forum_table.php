<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%collection_forum}}`.
 */
class m191117_131049_create_collection_forum_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%collection_forum}}', [
            'id' => $this->primaryKey(),
            'collection_id' => $this->integer(),
            'user_id' => $this->integer(),
            'parent_id' => $this->integer()->null(),
            'level' => $this->smallInteger(0),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'body' => $this->text()->notNull(),
        ]);

        $this->createIndex('idx-collection_forum_id','collection_forum','collection_id');
        $this->addForeignKey('fk-collection_forum_id','collection_forum','collection_id','collection','id','CASCADE');
        $this->createIndex('idx-collection_user_id','collection_forum','user_id');
        $this->addForeignKey('fk-collection_user_id','collection_forum','user_id','user','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-collection_forum_id','collection_forum');
        $this->dropIndex('idx-collection_forum_id','collection_forum');
        $this->dropForeignKey('fk-collection_user_id','collection_forum');
        $this->dropIndex('idx-collection_user_id','collection_forum');
        $this->dropTable('{{%collection_forum}}');
    }
}

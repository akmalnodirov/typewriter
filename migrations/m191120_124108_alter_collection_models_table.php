<?php

use yii\db\Migration;

/**
 * Class m191120_124108_alter_collection_models_table
 */
class m191120_124108_alter_collection_models_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('collection_models', 'start_symbol', 'string');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('collection_models', 'start_symbol', 'string');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191120_124108_alter_collection_models_table cannot be reverted.\n";

        return false;
    }
    */
}

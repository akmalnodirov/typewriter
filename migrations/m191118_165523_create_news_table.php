<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%news}}`.
 */
class m191118_165523_create_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%news}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'slug' => $this->string()->null(),
            'text' => $this->text(),
            'image' => $this->string()->null(),
            'posted_date' => $this->string()->null(),
            'tegs' => $this->string()->null(),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'order' => $this->smallInteger()->defaultValue(0)->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%news}}');
    }
}

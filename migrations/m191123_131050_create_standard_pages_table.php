<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%standard_pages}}`.
 */
class m191123_131050_create_standard_pages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%standard_pages}}', [
            'id' => $this->primaryKey(),
            'page_type_id' => $this->integer(),
            'title' => $this->string(),
            'text' => $this->text(),
            'image' => $this->string(),
            'slug' => $this->string(),
            'order' => $this->integer(),
            'author' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%standard_pages}}');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%typic_pages}}`.
 */
class m191123_111616_create_typic_pages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%page_types}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'page_type' => $this->smallInteger()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%page_types}}');
    }
}

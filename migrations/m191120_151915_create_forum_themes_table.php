<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%forum_themes}}`.
 */
class m191120_151915_create_forum_themes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%forum_themes}}', [
            'id' => $this->primaryKey(),
            'collection_id' => $this->integer()->null(),
            'theme_name' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1),
        ]);

        $this->createIndex('idx-forum-collection_id','forum_themes','collection_id');
        $this->addForeignKey('fk-forum--collection_id','forum_themes','collection_id','collection','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-forum--collection_id','forum_themes');
        $this->dropIndex('idx-forum--collection_id','forum_themes');
        $this->dropTable('{{%forum_themes}}');
    }
}

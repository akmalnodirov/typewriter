<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%collection_model_images}}`.
 */
class m191117_082759_create_collection_model_images_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%collection_model_images}}', [
            'id' => $this->primaryKey(),
            'collection_model_id' => $this->integer(),
            'image' => $this->string()->null(),
        ]);

        $this->createIndex('idx-collection_model_id','collection_model_images','collection_model_id');
        $this->addForeignKey('fk-collection_model_id','collection_model_images','collection_model_id','collection_models','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-collection_model_id','collection_model_images');
        $this->dropIndex('idx-collection_model_id','collection_model_images');
        $this->dropTable('{{%collection_model_images}}');
    }
}

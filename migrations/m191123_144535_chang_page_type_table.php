<?php

use yii\db\Migration;

/**
 * Class m191123_144535_chang_page_type_table
 */
class m191123_144535_chang_page_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('page_types', 'name');
        $this->addColumn('page_types', 'name', 'string');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('page_types', 'name');
        $this->addColumn('page_types', 'name', 'integer');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191123_144535_chang_page_type_table cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m191120_165817_alter_collection_forum_table
 */
class m191120_165817_alter_collection_forum_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('collection_forum', 'forum_theme_id', 'integer');
        $this->dropColumn('collection_forum', 'collection_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('collection_forum', 'collection_id', 'integer');
        $this->dropColumn('collection_forum', 'forum_theme_id');
    }
}

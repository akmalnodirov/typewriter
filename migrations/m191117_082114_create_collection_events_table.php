<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%collection_events}}`.
 */
class m191117_082114_create_collection_events_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%collection_events}}', [
            'id' => $this->primaryKey(),
            'collection_id' => $this->integer(),
            'event_date' => $this->string(),
            'event_description' => $this->text(),
            'update_at' => $this->timestamp()->notNull(),
            'create_at' => $this->timestamp()->notNull(),
        ]);

        $this->createIndex('idx-event-collection_id','collection_events','collection_id');
        $this->addForeignKey('fk-event-collection_id','collection_events','collection_id','collection','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-event-collection_id','collection_events');
        $this->dropIndex('idx-event-collection_id','collection_events');
        $this->dropTable('{{%collection_events}}');
    }
}

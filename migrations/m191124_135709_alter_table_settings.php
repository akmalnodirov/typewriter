<?php

use yii\db\Migration;

/**
 * Class m191124_135709_alter_table_settings
 */
class m191124_135709_alter_table_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('settings', 'forum_avatar', 'string');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('settings', 'forum_avatar');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191124_135709_alter_table_settings cannot be reverted.\n";

        return false;
    }
    */
}

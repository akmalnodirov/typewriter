<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%collection_images}}`.
 */
class m191117_081108_create_collection_images_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%collection_images}}', [
            'id' => $this->primaryKey(),
            'collection_id' => $this->integer(),
            'image' => $this->string()->null(),
        ]);

        $this->createIndex('idx-collection_id','collection_images','collection_id');
        $this->addForeignKey('fk-collection_id','collection_images','collection_id','collection','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-collection_id','collection_images');
        $this->dropIndex('idx-collection_id','collection_images');
        $this->dropTable('{{%collection_images}}');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%collection_model_characteristics}}`.
 */
class m191117_083420_create_collection_model_characteristics_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%collection_model_characteristics}}', [
            'id' => $this->primaryKey(),
            'collection_model_id' => $this->integer(),
            'key' => $this->string()->null(),
            'value' => $this->string()->null(),
        ]);

        $this->createIndex('idx-collection_model_ch_id','collection_model_characteristics','collection_model_id');
        $this->addForeignKey('fk-collection_model_ch_id','collection_model_characteristics','collection_model_id','collection_models','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-collection_model_ch_id','collection_model_characteristics');
        $this->dropIndex('idx-collection_model_ch_id','collection_model_characteristics');
        $this->dropTable('{{%collection_model_characteristics}}');
    }
}

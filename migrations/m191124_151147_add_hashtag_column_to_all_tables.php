<?php

use yii\db\Migration;

/**
 * Class m191124_151147_add_hashtag_column_to_all_tables
 */
class m191124_151147_add_hashtag_column_to_all_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('collection_events', 'hashtag', 'string');
        $this->addColumn('collection_files', 'hashtag', 'string');
        $this->addColumn('collection_model_characteristics', 'hashtag', 'string');
        $this->addColumn('collection_models', 'hashtag', 'string');
        $this->addColumn('forum_themes', 'hashtag', 'string');
        $this->addColumn('news', 'hashtag', 'string');
        $this->addColumn('standard_pages', 'hashtag', 'string');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('collection_events', 'hashtag');
        $this->dropColumn('collection_files', 'hashtag');
        $this->dropColumn('collection_model_characteristics', 'hashtag');
        $this->dropColumn('collection_models', 'hashtag');
        $this->dropColumn('forum_themes', 'hashtag');
        $this->dropColumn('news', 'hashtag');
        $this->dropColumn('standard_pages', 'hashtag');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191124_151147_add_hashtag_column_to_all_tables cannot be reverted.\n";

        return false;
    }
    */
}

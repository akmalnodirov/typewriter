<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%collection_models}}`.
 */
class m191117_082457_create_collection_models_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%collection_models}}', [
            'id' => $this->primaryKey(),
            'collection_id' => $this->integer(),
            'name' => $this->string(),
            'slug' => $this->string()->null(),
            'old' => $this->string(),
            'image' => $this->string()->null(),
            'description' => $this->text(),
            'update_at' => $this->timestamp()->notNull(),
            'create_at' => $this->timestamp()->notNull(),
        ]);

        $this->createIndex('idx-model-collection_id','collection_models','collection_id');
        $this->addForeignKey('fk-model-collection_id','collection_models','collection_id','collection','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-model-collection_id','collection_models');
        $this->dropIndex('idx-model-collection_id','collection_models');
        $this->dropTable('{{%collection_models}}');
    }
}

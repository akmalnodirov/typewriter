<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user}}`.
 */
class m191117_084617_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->unique(),
            'password_hash' => $this->string(),
            'password_reset_token' => $this->string()->null(),
            'auth_key' =>  $this->string()->null(),
            'email' => $this->string(),
            'address' => $this->string(),
            'type' => $this->smallInteger()->null()->defaultValue(0),
            'status' => $this->smallInteger()->defaultValue(0),
            'updated_at' => $this->integer(11)->null(),
            'created_at' => $this->integer(11)->null(),
            'verification_token' => $this->string()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}

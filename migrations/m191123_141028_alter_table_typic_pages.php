<?php

use yii\db\Migration;

/**
 * Class m191123_141028_alter_table_typic_pages
 */
class m191123_141028_alter_table_typic_pages extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('page_types', 'status', 'integer');
        $this->createIndex('idx-page_type_standard_page_id','standard_pages','page_type_id');
        $this->addForeignKey('fk-page_type_standard_page_id','standard_pages','page_type_id','page_types','id','CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-page_type_standard_page_id','standard_pages');
        $this->dropIndex('idx-page_type_standard_page_id','standard_pages');
        $this->dropColumn('page_types', 'status');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191123_141028_alter_table_typic_pages cannot be reverted.\n";

        return false;
    }
    */
}

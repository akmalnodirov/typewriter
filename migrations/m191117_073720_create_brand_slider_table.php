<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%brand_slider}}`.
 */
class m191117_073720_create_brand_slider_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%brand_slider}}', [
            'id' => $this->primaryKey(),
            'first_title' => $this->string(),
            'second_title' => $this->string(),
            'description' => $this->text(),
            'link' => $this->string()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%brand_slider}}');
    }
}

<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class TypeAsset
 * @package app\assets
 */
class TypeAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $basePath = '@webroot';

    /**
     * @var string
     */
    public $baseUrl = '@web';

    /**
     * @var array
     */
    public $css = [
        'typewriter/css/app.css',
        'typewriter/my_css/custom.css',

    ];

    /**
     * @var array
     */
    public $js = [
        'typewriter/js/vendor.js',
        'typewriter/js/app.js',
        'my_custom_assets/catalog.js',
        'my_custom_assets/collection_models.js',
        'my_custom_assets/news.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap4\BootstrapAsset',
    ];
}

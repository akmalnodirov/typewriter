<?php
namespace app\assets;

use trntv\filekit\widget\UploadAsset;
use yii\web\AssetBundle;

class CustomUploadAsset extends UploadAsset
{

    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap4\BootstrapAsset',
        'trntv\filekit\widget\BlueimpFileuploadAsset'
    ];

    public $js = [
        '/my_custom_assets/upload.kit.owerride.js',
    ];

}

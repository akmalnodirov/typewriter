<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdmintyAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/css?family=Open+Sans:400,600',
        'adminty/css/bootstrap.min.css',
        'adminty/css/themify-icons.css',
        'adminty/css/icofont.css',
        'adminty/css/feather.css',
        'adminty/css/switchery.min.css',
        'adminty/css/style.css',
        'adminty/css/font-awesome.min.css',

    ];
    public $js = [
        'adminty/js/jquery-ui.min.js',
        'adminty/js/popper.min.js',
        'adminty/js/bootstrap.min.js',
        'adminty/js/jquery.slimscroll.js',
        'adminty/js/jquery.mCustomScrollbar.concat.min.js',
        'adminty/js/pcoded.min.js',
        'adminty/js/switchery.min.js',
        'adminty/js/vartical-layout.min.js',
        'adminty/js/script.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap4\BootstrapAsset',
    ];
}
